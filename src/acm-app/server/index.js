var acm_detection = require("acm-detection");
var acm_repo = require("acm-repository-gateway");
var acm_model_merger = require("acm-model-merger");
var acm_model_converter = require("./acm-model-converter-proxy");
var acm_deployment = require("acm-deployment");
var acm_injection = require("acm-injection");
var acm_metamodel = require("acm-metamodel");
var acm_custom = require("acm-custom-design")


const crypto = require('crypto');
const http = require("http");
const URL = require('url').URL;
const util = require('util');
const fs = require('fs');
const { spawn } = require('child_process'); 

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');

var Monitor = acm_metamodel.Monitor;
var ACMComponent = acm_metamodel.ACMComponent;
var Action = acm_metamodel.Action;
var SoftwareComponent = acm_metamodel.SoftwareComponent;
var Link = acm_metamodel.Link;
var PhysicalProcess = acm_metamodel.PhysicalProcess;

var express = require("express");
const cors = require('cors'); 
var app = express();
app.use(cors())
app.set("port", 8008);
// enable json body
app.use(express.json({ limit: '10mb' }));
// enable swagger doc
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// start server
var server = app.listen(app.get('port'), '0.0.0.0', function () {
    var port = server.address().port;
    console.log('ACM Server started on ' + port);
});

// Route to get the model, parameters are a JSON object with path and type
app.post("/acm-renderer/model", generateModel);
// Load from file
app.post("/acm-model-editor/loadFile", loadFile);
// Solve conflicts with default ACM
app.post("/acm-model-editor/solveConflictsDefault", solveConflictsDefault);
// Route to find conflicts, parameters are a metamodel json serialization
app.post("/acm-renderer/findConflicts", findConflicts);
// Route to select a conflict to solve
app.post("/acm-renderer/select", selectConflict);
// Route to get the list of supported application types
app.get("/acm-renderer/supported-app-types", getSupportedAppTypes);
// Route to instantiate an ACM
app.post("/acm-renderer/instantiateACM", instantiateACM);
// Route to instantiate a Monitor
app.post("/acm-renderer/instantiateMonitor", instantiateMonitor);
// Route to deploy a model, online, to the target
app.post("/acm-renderer/deployOnline", deployOnline);
// Route to deploy a model, as a file to be downloaded
app.post("/acm-renderer/deployDownload", deployDownload);
// Render model for view refresh without full reploy
app.post("/acm-renderer/modelRender", onDemandRender);
// Refresh env model without refreshing the rest of the model
app.post("/acm-renderer/updateEnvModel", updateEnvModel);

// Route to add a new strategy to the database
app.post("/acm-database/addStrategy", addStrategyToDatabase);
// Route to list all strats in database (debug)
app.get("/acm-database/strategyDatabaseListing", strategyDatabaseListing);
// Route to query strategy database using sparql for jena or metadata for both
app.post("/acm-database/queryStrategyDatabase", queryStrategyDatabase);
// Check if database is online and started properly
app.get("/acm-database/queryDatabaseStatus", queryDatabaseStatus);

// Starts AGG with ACM rules for edition
app.post("/acm-model-editor/editAGGRules", editAGGRules);
// verify the rules specified by the user. 
app.post("/acm-custom/verifyECA", verifyECA)

// CI route that runs conflict detection and returns a true/false result
app.post("/acm-model-editor/conflictCheck", conflictCheck);
// Load from the CI model buffer
app.get("/acm-model-editor/loadFromACMBuffer", loadFromACMBuffer);

// takes a genesis deployment model, runs a full analysis, and returns whether or not new conflicts are identified
const DOCKER_CHECKING_ENVMODEL_DIR = "/data/acm/";
const ENVMODEL_FILENAME = "acm_env_model.json"; 
const defaultAggStrategy = {
    "request": {
        "sequence": [
            { "custom_default": 1 }
        ]
    },
    "custom_rules": [
        { "spreadTheTag": { "sequence": [{ "DirectConflictInitialize": 20 }, { "DirectConflictTagSpreading": 20 }] } },
        { "custom_addACMForSoft": { "sequence": [{ "DirectConflictAddACM": 1 }, { "DirectConflictAddACM2": 20 }] } },
        { "custom_addACMForAction": { "sequence": [{ "AddACMToAction": 1 }, { "AddACMToAction2": 20 }] } },
        { "custom_physicalConflict": { "sequence": [{ "physicalConflict2": 20 }, { "physicalConflict": 1 }, { "physicalConflict2": 20 }] } },
        { "custom_default": { "sequence": [{ "custom_physicalConflict": 20 }, { "custom_addACMForAction": 20 }] } },
        { "custom_default_global": { "sequence": [{ "spreadTheTag": 20 }, { "custom_addACMForAction": 20 }, { "custom_physicalConflict": 20 }, { "custom_addACMForSoft": 20 }] } }

    ]
};

let bufferedModel = {};
let bufferedEnvModel = {};

function conflictCheck(req, res) {
    console.log("Running a full conflict checking");
    let dm = req.body;

    bufferedModel = req.body;

    let envModel = null;
    if (fs.existsSync(DOCKER_CHECKING_ENVMODEL_DIR + ENVMODEL_FILENAME)){
        console.log("Loading env model from docker path " + DOCKER_CHECKING_ENVMODEL_DIR + ENVMODEL_FILENAME);
        envModel = JSON.parse(fs.readFileSync(DOCKER_CHECKING_ENVMODEL_DIR + ENVMODEL_FILENAME));
    } else {
        console.log("Loading env model from current path");
        envModel = JSON.parse(fs.readFileSync(ENVMODEL_FILENAME));
    }
    if (!envModel) {
        console.error("Couldnt load environment model, using an empty one");
        envModel = { "physical_processes": [], "links": [] };
    }
    bufferedEnvModel = envModel;

    let lfres = {
        end: (resultjson) => {
            let result = JSON.parse(resultjson);
            findConflictsNoSolve(result, envModel, { default: defaultAggStrategy }, (compare) => {
				res.set('Access-Control-Allow-Origin', '*');
                res.end(JSON.stringify({ newconflicts: compare }));
            });
        },
        type: () => { }
    };
    loadFile({ body: { json: JSON.stringify(dm), envModel: JSON.stringify(envModel), type: "genesis" } }, lfres);
}


function loadFromACMBuffer(req, res) {
    acm_model_converter.genesisConverter.fromJSON(
        bufferedModel,
        (model) => generateModelCallback(model, bufferedEnvModel, res)
    );
}

// verifies eca properties
function verifyECA(req, res){
    console.log("Starting verification ECA ")

    var error, fsm, smv, ecaJson
    //parsing ECA through Pegjs.
    ecaJson = acm_custom.verifySyntax(req.body)
    if(ecaJson.error){
        res.type("application/json")
        res.end(JSON.stringify({"error":ecaJson.error}))
        return   
    }
 
    //get a mapping between id_parent and flow name
    var idParentToParentName = {}

    //if model is from GeneSIS
    if(req.body.model.model.models.acm_model_type == "genesis"){
        var components = req.body.model.model.models.dm.components
        ///searching for component named _type: '/internal/node_red_flow'
        for (let i = 0; i < components.length; i++) {
            if(components[i]._type == '/internal/node_red_flow'){
                for (let j = 0; j < components[i].nr_flow.length; j++) {
                    var node = components[i].nr_flow[j];
                    if(node.type == "tab"){
                        idParentToParentName[node.id] = node.label 
                    }
                }
            }   
        }
    }
    //if model comes from node-red
    else{
        var nodes = req.body.model.model.models.nodes
        for (let i = 0; i < nodes.length; i++) {
            if(nodes[i].type == "tab"){
                idParentToParentName[nodes[i].id] = nodes[i].label 
            }
        }
    }

    //finding current ACM to get the id_conflict
    var conflictedACM;
    for (let i = 0; i < req.body.model.model.components.length; i++) {
        if(req.body.model.model.components[i].id == req.body.conflict.id){
            conflictedACM = req.body.model.model.components[i]
        }
    }

    //map id_parent to inputport and outputport
    var FlowNameToInputID = {}
    var FlowNameToOutputID = {}
    var inputIndex = 0
    var outputIndex = 0
    var links = req.body.model.model.links
    for (let i = 0; i < links.length; i++) {
        var link = links[i]

        if(link.from.id_conflict == conflictedACM.id_conflict){
            FlowNameToOutputID[link.to.id_parent] = outputIndex++ 
        }
        else if(link.to.id_conflict == conflictedACM.id_conflict){
            if(link.from.type == "mqtt in"){
                for (let j = 0; j < links.length; j++) {
                    if(links[j].from.type == "mqtt out" && links[j].to.type == "mqtt in" && links[j].from.configuration.topic == link.from.configuration.topic){
                        FlowNameToInputID[links[j].from.id_parent] = inputIndex++ 
                    }        
                }
            }
        }
    }

    //create the complete binding between flow name and port index
    var inputDictionnary = {}
    var outputDictionnary = {}
    for (const id in FlowNameToInputID) {
        inputDictionnary[idParentToParentName[id]] = FlowNameToInputID[id]
    }
    for (const id in FlowNameToOutputID) {
        //ECA doesn't like name with space. That's why spaces in flow names are removed.
        outputDictionnary[idParentToParentName[id].replace(/\s/g, '')] = FlowNameToOutputID[id]
    }

    //modify input definition
    for (let i = 0; i < ecaJson.environmentConfiguration.length; i++) {
        if(inputDictionnary[ecaJson.environmentConfiguration[i].inputPort] != undefined)
            ecaJson.environmentConfiguration[i].inputPort = inputDictionnary[ecaJson.environmentConfiguration[i].inputPort]
        else
            console.log("ERROR : flow "+ ecaJson.environmentConfiguration[i].inputPort + " has not been found ! Please verify that you have correctly written the flow name")    
    }
    
    //modify predicate definition
    for (let i = 0; i < ecaJson.predicateConfiguration.length; i++) {
        modifyECACondition(ecaJson.predicateConfiguration[i].condition,inputDictionnary) 
    }
    
    //modify action definition
    for (let i = 0; i < ecaJson.actionDefinition.length; i++) {
        for (let j = 0; j < ecaJson.actionDefinition[i].assignement.length; j++) {
            if(outputDictionnary[ecaJson.actionDefinition[i].assignement[j].output.outputPort] != undefined)
                ecaJson.actionDefinition[i].assignement[j].output.outputPort = outputDictionnary[ecaJson.actionDefinition[i].assignement[j].output.outputPort]
            else
            console.log("ERROR : flow "+ ecaJson.actionDefinition[i].assignement[j].output.outputPort + " has not been found ! Please verify that you have correctly written the flow name")    
        }
    }

    //modify sync strategy
    modifyECACondition(ecaJson.synchro.strategy.condition, inputDictionnary)

    //modify properties
    for (let i = 0; i < ecaJson.properties.length; i++) {
        modifyECACondition(ecaJson.properties[i].property,inputDictionnary,outputDictionnary)
    }

    error = acm_custom.verifyECA(ecaJson)
    if(error){
        res.type("application/json")
        res.end(JSON.stringify({"error":error.error}))
        return   
    }
    
    try{
        smv = acm_custom.verifyProperties(ecaJson)
    }catch(error){
        console.log("internal error in modelChecking verification")
        console.log(error)
        res.type("application/json")
        res.end(JSON.stringify({"error":{"message":"internal error in modelChecking verification","description":"internal error in modelChecking verification : check console to get the complete error message.","type":"logic"}}))
        return
    }

    smv=""
    if(smv.includes("FALSE")){
        console.log("model checking : properties are not compliant with current ECA rules")
        res.type("application/json")
        res.end(JSON.stringify({"error":{"message":"error during model checking","description":smv,"type":"logic"}}))
    }
    //console.log("everything good")
    
    ecaJson = acm_custom.verifySyntax(req.body)
    //modify action definition
    for (let i = 0; i < ecaJson.actionDefinition.length; i++) {
        for (let j = 0; j < ecaJson.actionDefinition[i].assignement.length; j++) {
            if(outputDictionnary[ecaJson.actionDefinition[i].assignement[j].output.outputPort] != undefined)
                ecaJson.actionDefinition[i].assignement[j].output.outputPort = outputDictionnary[ecaJson.actionDefinition[i].assignement[j].output.outputPort]
            else
            console.log("ERROR : flow "+ ecaJson.actionDefinition[i].assignement[j].output.outputPort + " has not been found ! Please verify that you have correctly written the flow name")    
        }
    }
    fsm = acm_custom.createComponent(ecaJson)
    fsm.idParentToParentName = idParentToParentName
    fsm.outputDictionnary = outputDictionnary
    res.type("application/json")
    res.end(JSON.stringify({"fsm":fsm,"smv":smv}))
}

function modifyECACondition(condition, inputDict, outputDict){

    if(!condition){
        return;
    }
    if(condition.input){
        condition.input.inputPort = inputDict[condition.input.inputPort]
    }
    else if(condition.inputPort){
        condition.inputPort = inputDict[condition.inputPort]
    }
    else if(condition.outputPort){
        condition.outputPort = outputDict[condition.outputPort]
    }
    else{
        modifyECACondition(condition.left,inputDict,outputDict)
        modifyECACondition(condition.right,inputDict,outputDict)
    }
}

function loadFile(req, res) {
    try {
        switch (req.body.type) {
            case "node-red":
                acm_model_converter.nodeRedConverter.fromJSON(
                    JSON.parse(req.body.json),
                    (model) => generateModelCallback(model, req.body.envModel, res)
                );
                break;
            case "genesis":
                // not a file, pretend it's a url and try it
                acm_model_converter.genesisConverter.fromJSON(
                    JSON.parse(req.body.json),
                    (model) => generateModelCallback(model, req.body.envModel, res)
                );
                break;
            default:
                res.type("application/json");
                res.end(JSON.stringify({ error: "Invalid model type", c:"Load from file" }));
                return;
        }
    } catch (e) {
        console.error(e);
        res.type("application/json");
        res.end(JSON.stringify({ error: e }));
    }
}

function getSupportedAppTypes(req, res) {
    res.end(JSON.stringify([{ text: "Node-RED", value: "node-red", defaultPath: "http://localhost:1880/" }, { text: "GeneSIS", value: "genesis", defaultPath: "http://localhost:8880/" }]));
}

// we have to be server side node to use child_process
// i definitely hate the agg editor
function editAGGRules(req, res) {
    let command = process.platform === "win32" ? "agg.bat" : "agg.sh";
    let spawnret = spawn(command, [".\\ACMRuleTemplate.ggx"]);
    if (spawnret.pid) {
        res.end(JSON.stringify({
            type: 'info',
            message: 'AGG Editor starting...',
            description:
                'The AGG editor will show up shortly, edit rules then overwrite the file to update ACM rules',
            duration: 10
        }));
    } else {
        res.end(JSON.stringify({
            type: 'error',
            message: 'AGG Editor unavailable!',
            description:
                "ACM couldn't start AGG Editor, make sure " + command + " is in the PATH",
            duration: 0
        }));
    }
}

function updateEnvModel(req, res) {
    let models = rebuildMetamodel(req.body.model);
    let envModel = req.body.envModel;
    acm_model_merger.unmergeModels(models, (unmergedModel) => {
        acm_model_merger.mergeModels(unmergedModel, envModel,
            (mergedModel) => {
                res.type("application/json");
                res.end(JSON.stringify(render(mergedModel)));
            }
        )
    });
}

function generateModel(req, res) {
    switch (req.body.type) {
    case "node-red":
        acm_model_converter.nodeRedConverter.fromURL(
            req.body.path,
            (model) => generateModelCallback(model, req.body.envModel, res)
        );
        break;
    case "genesis":
        if (fs.existsSync("genesis_models/" + req.body.path)) {
            // file exists, load it
            let genesismodel = fs.readFileSync("genesis_models/" + req.body.path);
            acm_model_converter.genesisConverter.fromJSON(
                JSON.parse(genesismodel),
                (model) => generateModelCallback(model, req.body.envModel, res)
            );
        } else {
            // not a file, pretend it's a url and try it
            acm_model_converter.genesisConverter.fromURL(
                req.body.path,
                (model) => generateModelCallback(model, req.body.envModel, res)
            );
        }
        break;
    default:
            res.type("application/json");
            res.end(JSON.stringify({ error: "Invalid model type", operation: "Load from server" }));
        return;
    }
}

function generateModelCallback(models, envModel, res) {
    acm_model_merger.mergeModels(rebuildMetamodel(models), envModel,
        (mergedModel) => {
            res.type("application/json");
            res.end(JSON.stringify(render(mergedModel, true)));
        }
    );
}

function findConflicts(req, res) {
    try {
        acm_model_merger.mergeModels(rebuildMetamodel(req.body.model.model), req.body.model.envModel,
            (mergedModel) => {
                //fs.writeFileSync("test.json", JSON.stringify(mergedModel));
                acm_detection.detection.findActuationConflictsInModel(
                    mergedModel,
                    req.body.aggStrategy,
                    (modelConflicts) => {
                        //modelConflicts.url = req.body.path;
                        //finalLog(modelConflicts);
                        solveConflictsDefaultImpl(rebuildMetamodel(modelConflicts), (ret) => {
                            res.type("application/json");
                            res.end(JSON.stringify(ret));
                        }, "Monitor");

                    }
                );
            }
        );
    } catch (e) {
        res.type("application/json");
        res.end(JSON.stringify({ error: e }));
    }
}

function findConflictsNoSolve(model, envModel, aggStrategy, callback) {
    try {
        acm_model_merger.mergeModels(rebuildMetamodel(model.model), envModel,
            (mergedModel) => {
                //fs.writeFileSync("test.json", JSON.stringify(mergedModel));
                acm_detection.detection.findActuationConflictsInModel(
                    mergedModel,
                    aggStrategy,
                    (modelConflicts) => {
                        let model = rebuildMetamodel(modelConflicts);
                        let cmps = model.components.filter(cmp => cmp instanceof ACMComponent && !cmp.configured);
                        callback(cmps.length !== 0);
                    }
                );
            }
        );
    } catch (e) {
        callback(false);
    }
}

function selectConflict(req, res) {
    //console.log(req.body.id);
    //console.log(JSON.stringify(req.body.model));
    res.type("application/json");
    acm_repo.getPolicies(ret => res.end(JSON.stringify(ret)));
}

function deployOnline(req, res) {
    console.log("deployOnline");
    try {
        acm_injection.injectNodes(rebuildMetamodel(req.body.model), (ret) => {
            //fs.writeFileSync("model_dump.json", JSON.stringify(ret));
            //finalLog(ret);
            acm_deployment.deployOnline(ret, (retdep) => {
                //finalLog(retdep);
                delete retdep.acm_model_type;
                res.type("application/json");
                res.end(JSON.stringify(retdep));
            });
        });
    } catch (e) {
        console.error(e);
        res.end(JSON.stringify({
            error: "Error deploying model",
            message: e.message
        }));
    }
}


function deployDownload(req, res) {
    console.log("deployDownload");
    acm_injection.injectNodes(rebuildMetamodel(req.body.model), (ret) => {
        acm_deployment.deployDownload(ret, (retdep) => {
            console.log("deployDownload ret");
            //finalLog(retdep);
            delete retdep.acm_model_type;
            res.type("application/json");
            res.end(JSON.stringify(retdep));
        });
    });
}


/**
 *  console.log("Deploy command received for type " + req.body.conflict.model.model.type);
    var strat_id = req.body.strat_id;
    var conflict_id = req.body.conflict.id;
    acm_repo.getPolicies(strats => {
        // get strat
        strats = JSON.parse(strats);
        var selectedStrat = { id: "invalid" };
        for (var i = 0; i < strats.length; i++) {
            if (strats[i].id === strat_id) {
                selectedStrat = strats[i];
                break;
            }
        }
        if (selectedStrat.id === "invalid") {
            res.end("{\"error\":\"invalid strat id can't deploy\""); 
            return;
        }
    });
 * */

function solveConflictsDefault(req, res) {
    let model = rebuildMetamodel(req.body);
    solveConflictsDefaultImpl(model, (ret) => res.end(JSON.stringify(ret)), "PassThrough", true);
}

function solveConflictsDefaultImpl(model, callback, stratID = "PassThrough", solveMonitors = false) {
    let defaultConfig = [
        /*{
            "name": "mqttserver",
            "description": "MQTT server to connect to",
            "value": "mqtt://localhost:1883"
        },
        {
            "name": "mqtttopic",
            "description": "MQTT topic to publish to",
            "value": "acmMonitor"
        }*/
    ];
    let cmps = model.components.filter(cmp => cmp instanceof ACMComponent && (!cmp.configured || solveMonitors && cmp.strategy.name === "Monitor" ));
    let toprocess = cmps.length;

    // nothing to process just
    if (toprocess === 0) {
        console.log("no conflicts to solve");
        callback(model);
    } else {
        console.log("solveConflictsDefault toprocess: " + toprocess)
        cmps.forEach(cmp => {
            instantiateACMImpl(cmp.id, stratID, model, defaultConfig, (modelRet) => {
                toprocess--;
                //model = modelRet;

                if (toprocess === 0) {
                    //res.end(JSON.stringify(model.components.filter(cmp => cmp instanceof ACMComponent)));
                    callback(model);
                }
            });
        });
    }
}

function instantiateACMImpl(targetComponentID, stratID, model, scxml, callback) {
    acm_repo.getPolicies(strats => {
        strats = JSON.parse(strats);
        var selectedStrat = { id: "invalid" };
        for (var i = 0; i < strats.length; i++) {
            if (strats[i].id === stratID || strats[i].name === stratID) {
                selectedStrat = strats[i];
                break;
            }
        }

        //added to get configuration if it needs one.
        if(scxml){
            selectedStrat.configuration = scxml
        }

        // yeah fuck it
        //console.log(stratID);
        if (selectedStrat.id === "invalid" && stratID !== "monitor") {
            callback({ error: "invalid strat id can't deploy" });
            return;
        }

        var id_conflict = undefined;
        for (var cmpidx in model.components) {
            // because yes of course generated IDs are fucking ints not node-red ids like the rest of the fucking model because fuck consistency
            if ("" + model.components[cmpidx].id === targetComponentID) {
                model.components[cmpidx].strategy = selectedStrat;
                model.components[cmpidx].configured = true;
                id_conflict = model.components[cmpidx].id_conflict;
                break;
            }
        }

        // send help
        if (id_conflict) {
            for (var cmpidx2 in model.components) {
                //console.log(model.components[cmpidx2].id_conflict);
                if (model.components[cmpidx2].id_conflict === id_conflict) {
                    model.components[cmpidx2].strategy = selectedStrat;
                    model.components[cmpidx2].configured = true;
                }
            }
        }

        callback(model);
    });
}

function instantiateACM(req, res) {
    let targetComponentID = req.body.conflict.id;
    let stratID = req.body.strat_id;
    let model = rebuildMetamodel(req.body.conflict.model);

    //finalLog(req.body.conflict.model);
    let scxml;
    if (req.body.configuration) {
        if (req.body.configuration.scxml) {
            scxml = req.body.configuration.scxml
        } 
        if (req.body.configuration.configuration) {
            scxml = req.body.configuration.configuration;
        }
    }
    //console.log("scxml : " + scxml)

    instantiateACMImpl(targetComponentID, stratID, model, scxml, (modelRet) => {
        if (modelRet.error) {
            res.end(JSON.stringify(modelRet));
        } else {
            res.end(JSON.stringify({ success: modelRet }));
        }
    });
}

// monitors
function instantiateMonitor(req, res) {
    var targetComponentID = req.body.conflict.id;
    var model = rebuildMetamodel(req.body.model);
    var monitorAction = req.body.monitorConfig;

    //finalLog(model);

    for (var cmpidx in model.components) {
        // because yes of course generated IDs are fucking ints not node-red ids like the rest of the fucking model because fuck consistency
        if ("" + model.components[cmpidx].id === targetComponentID) {
            model.components[cmpidx].action = monitorAction;
            model.components[cmpidx].configured = true;
            break;
        }
    }

    res.end(JSON.stringify({ success: model }));
}


// model is sent from client using json
// json loses type information
// needs to rebuild the model from json to metamodel type
function rebuildMetamodel(model) {
    return acm_metamodel.rebuildMetamodel(model);
}


function onDemandRender(req, res) {
    res.type("application/json");
    res.end(JSON.stringify(render(rebuildMetamodel(req.body))));
}

function render(model, initial = false) {
    // init rendered model
    var renderedModel = {};
    renderedModel.model = model;
    /*
    console.log("!! entering render, printing model");
    finalLog(model);
    console.log("----");
    */
    //fs.writeFileSync("model.json", JSON.stringify(model));
    
    // init graph section 
    renderedModel.graph = {
        zoomingEnabled: true,
        userZoomingEnabled: true,
        panningEnabled: true,
        userPanningEnabled: true,
        boxSelectionEnable: false,
        renderer: { name: "canvas" }
    };
    renderedModel.graph.elements = {
        nodes: [],
        edges: []
    };

    // insert parents
    let parentsToCreate = [];
    for (let cmpidx in model.components) {
        let componentz = model.components[cmpidx];
        // skipping comps with no parents
        if (componentz.id_parent && componentz.id_conflict !== "") {
            if (parentsToCreate.findIndex(cmp => cmp.id === componentz.id_parent) === -1) {
                // we need to create a new parent flow, go fetch its parent genesis container name and the flow name if available
                let parent_name = componentz.id_parent;
                let disabled = false;
                if (model.models.dm) {
                    for (let gencmpidx in model.models.dm.components) {
                        let cmp = model.models.dm.components[gencmpidx];
                        // node red component, it has an nr_flow
                        if (cmp.nr_flow) {
                            for (let nodeidx in cmp.nr_flow) {
                                let node = cmp.nr_flow[nodeidx];
                                // ez case: we have a node with id === id_parent
                                if (node.id === componentz.id_parent) {
                                    // use name
                                    parent_name = cmp.name + "/" + node.label;
                                    disabled = node.disabled;
                                    break;
                                }
                                // bad case: no tab node
                                if (node.id === componentz.id) {
                                    parent_name = cmp.name + "/" + parent_name;
                                }
                            }
                        }
                    }
                } else if (model.models.nodes) {
                    // if there is no genesis model data try to find nodered model data
                    for (let nrcmpidx in model.models.nodes) {
                        let nrcmp = model.models.nodes[nrcmpidx];
                        if (nrcmp.id === componentz.id_parent) {
                            parent_name = nrcmp.label;
                            disabled = nrcmp.disabled;
                        }
                    }
                }


                parentsToCreate.push({ id: componentz.id_parent, name: parent_name, isConflictParent: false, disabled: disabled });
            }
            if (componentz.id_conflict && parentsToCreate.findIndex(cmp => cmp.id === "conflict_" + componentz.id_conflict) === -1) {
                parentsToCreate.push({ id: "conflict_" + componentz.id_conflict, name: "conflict_" + componentz.id_conflict, isConflictParent: true });
            }
        }
    }
    for (let paridx in parentsToCreate) {
        if (parentsToCreate[paridx].disabled) { console.log("skipping disabled flow " + parentsToCreate[paridx].id); continue; }
        let nodez = {
            data: { ...parentsToCreate[paridx], isParent: true },
            position: {
                x: 0, y: 0
            },
            group: "nodes",
            removed: false,
            selected: false,
            selectable: true,
            locked: false,
            grabbable: true,
            classes: parentsToCreate[paridx].id.startsWith("conflict_") ? "conflict-parent-node" : "parent-node"
        };
        renderedModel.graph.elements.nodes.push(nodez);
    }

    // run an ebin layout engine
    // we run the layout here to take advantage from having computed parent count
    if (initial) {
        console.log("Running auto layout");
        runLayout(model, parentsToCreate.filter(p => !p.disabled));
    }

    // insert nodes
    let disabledComponents = [];
    for (let i = 0; i < model.components.length; i++) {
        let component = model.components[i];
        // skip components defined as no render
        if (component.configuration && component.configuration.noRender) continue;
        // skip disabled components, or components in disabled flows
        if (component.disabled || parentsToCreate.find(p => p.id === component.id_parent && p.disabled)) { disabledComponents.push(component); continue; }
        if (component.id_parent !== "") {
            let node = {
                data: {
                    id: component.id,
                    name: component.name,
                    parent: component.id_conflict ? "conflict_" + component.id_conflict : component.id_parent,
                    actualParent: component.id_parent,
                    toConfigure: component instanceof ACMComponent && !component.configured,
                    isACM: component instanceof ACMComponent,
                    isMonitor: component instanceof Monitor || (component.strategy && component.strategy.name === "Monitor")
                },
                position: {
                    x: component.x, y: component.y
                },
                group: "nodes",
                removed: false,
                selected: false,
                selectable: true,
                locked: false,
                grabbable: true,
                classes: getClassForComponent(component)
            };
            renderedModel.graph.elements.nodes.push(node);

            // insert links to physical processes
            for (let phyproidx in component.physicalProcess) {
                let edge = {
                    data: {
                        id: component.id + "_" + component.physicalProcess[phyproidx].id,
                        source: component.id,
                        target: component.physicalProcess[phyproidx].id
                    },
                    position: {},
                    group: "edges",
                    removed: false,
                    selected: false,
                    selectable: false,
                    locked: false,
                    grabbable: true,
                    classes: "phy"
                };
                renderedModel.graph.elements.edges.push(edge);
            }
        }
    }

    // insert edges/links
    for (i = 0; i < model.links.length; i++) {
        let link = model.links[i];
        // skip malformed links
        if (link.from && link.to && link.from.id && link.to.id && disabledComponents.findIndex(c => c === link.from || c === link.to) === -1) {
            let edge = {
                data: {
                    id: link.from.id + "_" + link.to.id,
                    source: link.from.id,
                    target: link.to.id
                },
                position: {},
                group: "edges",
                removed: false,
                selected: false,
                selectable: false,
                locked: false,
                grabbable: true,
                classes: link.virtual?"virtual":""
            };
            renderedModel.graph.elements.edges.push(edge);
        } else {
            //console.log("malformed link");
            //finalLog(link);
        }
    }

    // insert physical processes
    for (let phyprocidx in model.physicalProcess) {
        let phyProc = model.physicalProcess[phyprocidx];
        let node = {
            data: {
                id: phyProc.id,
                name: phyProc.name,
                parent: ""
            },
            position: {
                x: phyProc.x, y: phyProc.y
            },
            group: "nodes",
            removed: false,
            selected: false,
            selectable: true,
            locked: false,
            grabbable: true,
            classes: "physicalProcess"
        };
        renderedModel.graph.elements.nodes.push(node);
    }

    //finalLog(renderedModel.graph.elements.nodes);
    return renderedModel;
}

function getClassForComponent(component) {
    var compclass = component.constructor.name;
    if (compclass.startsWith("MQTT")) compclass = "mqtt";
    if (component.configured) {
        compclass += "-configured";
    }
    if (component.strategy && component.strategy.name === "Monitor") {
        compclass = component.constructor.name + "-default";
    }
    return compclass;
}


function runLayout(model, parents) {
    // sqrt(mbflow) wide, go up one y axis step every time you reach it
    let gridcols = Math.ceil(Math.sqrt(parents.length));
    //console.log("ACM layout " + gridcols);
    let xbase = 0, ybase = 0, xmax = 0, ymax = 0;
    for (let paridx in parents) {
        // find children
        for (let cmpidx in model.components) {
            let cmp = model.components[cmpidx];
            if (cmp.id_parent === parents[paridx].id) {
                cmp.x += xbase;
                cmp.y += ybase;

                if (cmp.x > xmax) { xmax = cmp.x; }
                if (cmp.y > ymax) { ymax = cmp.y; }
            }
        }

        if ((parseInt(paridx) + 1) % gridcols === 0) {
            //console.log("going to next row");
            // next row
            xbase = 0;
            xmax = 0;
            ybase = ymax + 30;
        } else {
            // next col
            xbase = xmax + 30;
        }
    }
}

function finalLog(o) {
    console.log(util.inspect(o, false, null, true));
}

/******
 * Rethink data explorer
 *  r.db('acm_repository').table('policies').forEach((entry)=>{
        return r.db('acm_repository').table('policies').get(entry('id')).delete();
    });
*/

function addStrategyToDatabase(req, res) {
    acm_repo.addPolicy(req.body, (dbres) => { console.log("addPolicy : " + dbres); res.end(dbres); });
}

function strategyDatabaseListing(req, res) {
    acm_repo.getPolicies((gpres) => { /*console.log(gpres);*/ res.type("application/json"); res.end(gpres); });
}

function queryStrategyDatabase(req, res) {
    acm_repo.queryPolicies(req.body, qpret => {
        res.type("application/json");
        res.end(qpret);
    });
}

function queryDatabaseStatus(req, res) {
    res.type('application/json');
    res.end(JSON.stringify(acm_repo.repoStatus));
}
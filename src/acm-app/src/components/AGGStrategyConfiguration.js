import React, { Component } from 'react';
import { Modal, Button, Input, Form, Alert, Checkbox } from 'antd';

export default class AGGStrategyConfiguration extends Component {
	constructor(props) {
		super(props);

		this.state = {
			parent: this.props.parent,
			showJsonInvalidAlert: false
		};

		this.onStratContentChange = this.onStratContentChange.bind(this);
		this.saveAggStrat = this.saveAggStrat.bind(this);
		this.closeAggStratModal = this.closeAggStratModal.bind(this);
		this.onCheckboxChange = this.onCheckboxChange.bind(this);
	}

	closeAggStratModal() {
		this.setState({ showJsonInvalidAlert: false });
		this.state.parent.closeAggStratModal();
    }

	onStratContentChange(e) {
		this.state.parent.setState({ aggStratConfigurationBuff: e.target.value });
	}

	saveAggStrat() {
		if (!this.state.parent.state.aggStratConfigurationDefault && !this.isJSON(this.state.parent.state.aggStratConfigurationBuff)) {
			this.setState({ showJsonInvalidAlert: true });
		} else {
			this.setState({ showJsonInvalidAlert: false });
			this.state.parent.saveAggStrat();
        }
	}

	onCheckboxChange(e) {
		this.state.parent.setState({ aggStratConfigurationDefault: e.target.checked });
    }


	isJSON(str) {
		try {
			JSON.parse(str);
			return true;
		} catch (e) {
			return false
        }
	}

	render() {
		return (
			<Modal
				title="AGG Strategy Configuration"
				footer={[
					<Button key="aggstratcancel" onClick={this.closeAggStratModal}>Cancel</Button>,
					<Button key="aggstratselect" onClick={this.saveAggStrat}>Go!</Button>
				]}
				visible={this.state.parent.state.aggStratModalOpen}
				onCancel={this.state.parent.closeAggStratModal}
				width={"60vw"}
			>
				<Form>
					<Checkbox checked={this.state.parent.state.aggStratConfigurationDefault} onChange={this.onCheckboxChange}>Default</Checkbox>
					<Form.Item label="Custom">
						<Input.TextArea disabled={this.state.parent.state.aggStratConfigurationDefault} onChange={this.onStratContentChange} value={this.state.parent.state.aggStratConfigurationBuff} autoFocus autosize={{minRows: 6, maxRows: 30}} />
					</Form.Item>
					{this.state.showJsonInvalidAlert ? <Alert message="Invalid JSON" type="error" showIcon/> : null}
				</Form>
			</Modal>
		);
	}
} 
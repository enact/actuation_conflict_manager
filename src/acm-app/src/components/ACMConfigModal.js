import React, { Component } from 'react';
import { Modal, Button, Form, Input } from 'antd';

export default class ACMConfigModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			parent: this.props.parent,
			configurations: []
		};

		this.updateConfigurationValue = this.updateConfigurationValue.bind(this);
	}

	updateConfigurationValue(e, name) {
		let ss = this.state.parent.state.selectedStrat;
		let confs = ss.configuration;
		let conf = confs.find(c => c.name === name);
		conf.value = e.target.value;
		this.state.parent.setState({ selectedStrat:ss });
	}

	render() {
		let formItems = [];
		if (this.state.parent.state.selectedStrat && this.state.parent.state.selectedStrat.configuration) {
			this.state.parent.state.selectedStrat.configuration.forEach(config => {
				formItems.push(
					<Form.Item label={config.description} key={config.name}>
						<Input onChange={(e) => this.updateConfigurationValue(e, config.name)} value={config.value} autoFocus />
					</Form.Item>
				);
			});
		} else {
			formItems.push(<div key="configplaceholder">nothing</div>);
        }
		return (
			<Modal
				title="ACM Configuration Data"
				footer={[
					<Button key="ACMConfigCancel" onClick={this.state.parent.closeConfigModal}>Cancel</Button>,
					<Button key="ACMConfigSave" onClick={this.state.parent.deployWithConfig}>Save</Button>
				]}
				visible={this.state.parent.state.configModalOpen}
				onCancel={this.state.parent.closeConfigModal}
			>
				<Form>
					{formItems}
				</Form>
			</Modal>
		);
	}
} 
import React, { Component } from 'react';
import { Modal, Button, Form, Input,Alert, Checkbox, Steps } from 'antd';
const { Step } = Steps;


export default class ACMCustom extends Component {

	constructor(props) {
		super(props);

		this.state = {
			parent: this.props.parent,
			showError : false
		};
		this.onNameChange = this.onNameChange.bind(this);
		this.onCheckboxChange = this.onCheckboxChange.bind(this);

		this.stepSuccess  = {
      		backgroundColor: "lightgreen",
      		padding: "10px",
			fontFamily: "Arial",
			fontWeight: 'bold'
		}
		this.stepFail = {
			backgroundColor: "#E9573F",
			padding: "10px",
			fontFamily: "Arial",
			fontWeight: 'bold'
		}
	}

	onNameChange(e) {
		this.state.parent.setState({ ACMCustomEcaRule: e.target.value });
	}

	onCheckboxChange(e) {
		this.state.parent.setState({ isECAToAddToDB: e.target.checked });
	}

	render() {

		return (
			<Modal
				title="Create new Custom ACM from ECA Rules"
				footer={[
					<Button key="ACMCustomCancel"  onClick={this.state.parent.closeACMCustomModal}>Cancel</Button>,
					<Button key="ACMCustomSelect" onClick={this.state.parent.createACMCustom}>Go!</Button>,
					<Button key="ACMCustomVerify" onClick={this.state.parent.verifyECA} >Verify your ECA</Button>
				]}
				visible={this.state.parent.state.ACMCustomModalOpen	}
				onCancel={this.state.parent.closeACMCustomModal}
				width={"60vw"}
				>
				<Steps>
  					<Step status={this.state.parent.state.validationStep.syntax} title="syntax verification" />
					<Step status={this.state.parent.state.validationStep.semantic} title="semantic verification" />
					<Step status={this.state.parent.state.validationStep.logic} title="model checking" />
				</Steps>
				<Form style={{ padding: '10px' }}>
					<Form.Item >
						<Input.TextArea onChange={this.onNameChange} value={this.state.parent.state.ACMCustomEcaRule} autoFocus autosize={{minRows: 6, maxRows: 30}} /> 
					</Form.Item>
				</Form>

				<Checkbox checked={this.state.parent.state.isECAToAddToDB} onChange={this.onCheckboxChange}>Remember?</Checkbox>
				{this.state.parent.state.validationStep.error!=null ? <Alert message={this.state.parent.state.validationStep.error.description} type="error" showIcon/> : null}
			</Modal>
		);
	}
} 
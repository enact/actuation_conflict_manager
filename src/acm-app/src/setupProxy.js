const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    '/acm-renderer/*',
    createProxyMiddleware({
      target: 'http://localhost:8008',
      changeOrigin: true,
    })
  );
  app.use(
    '/api-docs*',
    createProxyMiddleware({
      target: 'http://localhost:8008',
      changeOrigin: true,
    })
  );
  app.use(
    '/acm-database/*',
    createProxyMiddleware({
      target: 'http://localhost:8008',
      changeOrigin: true,
    })
  );
  app.use(
    '/acm-model-editor/*',
    createProxyMiddleware({
      target: 'http://localhost:8008',
      changeOrigin: true,
    })
  );
  app.use(
    '/acm-custom/*',
    createProxyMiddleware({
      target: 'http://localhost:8008',
      changeOrigin: true,
    })
  );
};
/**
  "proxy": {
    "/acm-renderer/*": {
      "target": "http://localhost:8008/",
      "secure": "false"
    },
    "/api-docs*": {
      "target": "http://localhost:8008/",
      "secure": "false"
    },
    "/acm-database/*": {
      "target": "http://localhost:8008/",
      "secure": "false"
    },
    "/acm-model-editor/*": {
      "target": "http://localhost:8008/",
      "secure": "false"
    },
    "/acm-custom/*": {
      "target": "http://localhost:8008/",
      "secure": "false"
    }
  },
**/
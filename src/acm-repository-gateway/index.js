﻿let rethinkdb = require("./rethinkdb");
let jena = require("./jena-fuseki");

// select repo to query
//let repo = jena;
// 3030 jena 28015 rethinkdb
//let port = 3030;

let repo = rethinkdb;
let port = 28015;

let host = "localhost";

// list of default policies to load in db
let defaultpolicies = JSON.parse(require("fs").readFileSync("./database/default.json"));

// init it
let initattempts = 1;
if (repo) {
    repo.init(host, port, defaultpolicies, init_callback);

    let exports = module.exports = {};

    function init_callback(err) {
        exports.init = repo.init;
        exports.getPolicies = repo.getPolicies;
        exports.addPolicy = repo.addPolicy;
        exports.updatePolicy = repo.updatePolicy;
        exports.deletePolicy = repo.deletePolicy;
        exports.queryPolicies = repo.queryPolicies;

        if (err) {
            console.log("init error (attempt " +  initattempts + ")\n" + err);
            exports.repoStatus = { online: false, error: err };
            if (initattempts < 10) {
                initattempts++;
                setTimeout(() => repo.init(host, port, defaultpolicies, init_callback), 5000);
            }
        } else {
            console.log("init success " + initattempts);
            exports.repoStatus = { online: true };
        }
    }
}
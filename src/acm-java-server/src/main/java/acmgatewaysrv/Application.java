package acmgatewaysrv;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Collections;

@SpringBootApplication(scanBasePackages = {"acmgatewaysrv", "acmthingmlsrv"})
public class Application {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", "8007"));
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
}
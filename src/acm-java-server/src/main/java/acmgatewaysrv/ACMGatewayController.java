package acmgatewaysrv;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.Arrays;

@RestController
public class ACMGatewayController {
    @PostMapping("/findConflicts")
    public String findConflicts(@RequestBody String data) {
        File tmp;
        try {
            JSONObject request = new JSONObject(data);
            String xml = request.getString("ggx");
            tmp = File.createTempFile("acm-agg-", ".ggx");
            PrintWriter pw = new PrintWriter(tmp);
            pw.print(xml);
            pw.flush();

            System.out.println("Processing input model " + tmp.getAbsolutePath());
            try {
                new AGGRunner(tmp.getAbsolutePath(), request.getJSONObject("AGGRunnerStrategy"), (request.has("debugOverride") &&  request.getBoolean("debugOverride")));
            } catch(Exception e){
                System.out.println("Exception processing model " + tmp.getAbsolutePath() + ":\n");
                e.printStackTrace();
            }
            System.out.println("Processed model into " + tmp.getAbsolutePath().replace(".ggx", "") + "-output.ggx");

            InputStream is = new FileInputStream(tmp.getAbsolutePath().replace(".ggx", "") + "-output.ggx");
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
            String fileAsString = sb.toString();

            return fileAsString;
        } catch (Exception e) {
            e.printStackTrace();
            return "error \n" + Arrays.toString(e.getStackTrace());
        }
    }
}
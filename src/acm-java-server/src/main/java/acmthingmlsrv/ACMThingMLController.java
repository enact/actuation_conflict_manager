package acmthingmlsrv;

import org.eclipse.emf.common.util.EList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.thingml.compilers.ThingMLCompiler;
import org.thingml.xtext.constraints.ThingMLHelpers;
import org.thingml.xtext.thingML.*;
import org.thingml.xtext.thingML.impl.ThingImpl;

import javax.swing.plaf.basic.BasicViewportUI;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
public class ACMThingMLController {
    @PostMapping("/fromThingML")
    public String fromThingML(@RequestBody String data){
        try {
            JSONObject rq = new JSONObject(data);
            String thingmlInputPath = rq.getString("thingmlInputPath");

            ThingMLFactory factory = ThingMLFactory.eINSTANCE;
            File input = new File(thingmlInputPath);
            ThingMLModel input_model = ThingMLCompiler.loadModel(input);
            Configuration config = input_model.getConfigs().get(0);

            JSONObject res = new JSONObject();
            JSONArray instances = new JSONArray();
            JSONArray connectors = new JSONArray();

            for(Instance i : ThingMLHelpers.allInstances(config)){
                JSONObject jinstance = new JSONObject();
                jinstance.put("name", i.getName());
                jinstance.put("type", i.getType().getName());
                instances.put(jinstance);
            }

            for(AbstractConnector ac : config.getConnectors()){
                if(ac instanceof Connector) {
                    Connector conn = (Connector) ac;
                    JSONObject jconn = new JSONObject();
                    jconn.put("from", conn.getSrv().getName());
                    jconn.put("to", conn.getCli().getName());
                    jconn.put("fromPort", conn.getProvided().getName());
                    jconn.put("toPort", conn.getRequired().getName());
                    connectors.put(jconn);
                }
            }

            res.put("instances", instances);
            res.put("connectors", connectors);

            return res.toString();
        } catch (JSONException e) {
            return "{\"error\":\"failed to parse json\"}";
        }
    }

    @PostMapping("/toThingML")
    public String toThingML(@RequestBody String data) {
        try {
            JSONObject rq = new JSONObject(data);
            String thingmlInputPath = rq.getString("thingmlInputPath"),
                    thingmlOutputPath = rq.getString("thingmlOutputPath"),
                    targetLanguage = rq.getString("targetLanguage");
            JSONArray instances = rq.getJSONArray("instances"),
                    imports = rq.getJSONArray("imports");
            JSONObject model = rq.getJSONObject("model");
            ThingMLFactory factory = ThingMLFactory.eINSTANCE;

            File input0 = new File(thingmlInputPath);
            String input2str = thingmlInputPath + "_acmtemp.thingml";

            // copy input file to input2 file
            BufferedWriter bw = new BufferedWriter(new FileWriter(input2str));
            {
                BufferedReader inr = new BufferedReader(new FileReader(thingmlInputPath));
                String str;
                while ((str = inr.readLine()) != null) {
                    bw.write(str);
                    bw.write("\n");
                }
                inr.close();
            }

            // copy acm stuff to input2 file
            String absolutePath = input0.getAbsolutePath();
            String basepath = absolutePath.substring(0,absolutePath.lastIndexOf(File.separator));
            // insert datatypes import
            {
                /*Import acmdatatypes = factory.createImport();
                acmdatatypes.setImportURI("acm-datatypes.thingml");
                input_model.getImports().add(acmdatatypes);*/
                BufferedReader br = new BufferedReader(new FileReader(basepath + "/acm-datatypes.thingml"));
                String str;
                while ((str = br.readLine()) != null) {
                    bw.write(str);
                    bw.write("\n");
                }
                br.close();
            }

            // insert generated imports
            for(int i = 0; i < imports.length(); i++){
                JSONObject importdata = imports.getJSONObject(i);
                /*Import thingimport = factory.createImport();
                thingimport.setImportURI(importdata.getString("path"));
                input_model.getImports().add(thingimport);*/
                BufferedReader br = new BufferedReader(new FileReader(basepath + "/" + importdata.getString("path")));
                String str;
                while ((str = br.readLine()) != null) {
                    bw.write(str);
                    bw.write("\n");
                }
                br.close();
            }
            bw.flush();
            bw.close();

            // load model in ThingML
            File input = new File(input2str);
            ThingMLModel input_model = ThingMLCompiler.loadModel(input);
            Configuration config = input_model.getConfigs().get(0);

            // fix primitive imports
            /*{
                Thing ACMThing = ThingMLHelpers.findThing(input_model, "ACMThing", false).get(0);
                ACMThing.getProperties().forEach(p -> {
                    TypeRef tr = p.getTypeRef();
                    tr.setType(ThingMLHelpers.findSimpleType(input_model, tr.getType().getName(), false).get(0));
                });
                Thing ACMTagThing = ThingMLHelpers.findThing(input_model, "ACMTagThing", false).get(0);
                ACMTagThing.getProperties().forEach(p -> {
                    TypeRef tr = p.getTypeRef();
                    tr.setType(ThingMLHelpers.findSimpleType(input_model, tr.getType().getName(), false).get(0));
                });
                Thing ACMSyncThing = ThingMLHelpers.findThing(input_model, "ACMSyncThing", false).get(0);
                ACMSyncThing.getProperties().forEach(p -> {
                    TypeRef tr = p.getTypeRef();
                    tr.setType(ThingMLHelpers.findSimpleType(input_model, tr.getType().getName(), false).get(0));
                });
                ACMSyncThing.getIncludes().clear();
                ACMSyncThing.getIncludes().add(ThingMLHelpers.findThing(input_model, "ACMDatatypes", false).get(0));
                ACMSyncThing.getIncludes().add(ThingMLHelpers.findThing(input_model, "TimerMsgs", false).get(0));
            }*/

            // split links into existing links vs new ACM links
            List<JSONObject> w[] = splitLinks(model);
            List<JSONObject> acmConnectors = w[0],
                    thingConnectors = w[1];

            // generate list of connectors that don't exist in the model
            List<AbstractConnector> deadConnectors = config.getConnectors().stream().filter(conn -> {
                try{
                    return !findConnectorInList(thingConnectors, (Connector) conn, input_model, config);
                } catch(ClassCastException | JSONException ex){
                    System.err.println("Casting error AbstractConnector not a Connector?");
                    return false;
                }
            }).collect(Collectors.toList());

            // delete these connectors from the config
            config.getConnectors().removeAll(deadConnectors);

            // insert instances
            int tagIndex = 0;
            for(int i = 0; i < instances.length(); i++){
                JSONObject instancejson = instances.getJSONObject(i);
                // create the instance itself
                Instance instance = factory.createInstance();
                Thing thing = ThingMLHelpers.findThing(input_model, instancejson.getString("type"), false).get(0);
                instance.setType(thing);
                instance.setName(instancejson.getString("name"));
                config.getInstances().add(instance);

                // add properties and extra instances based on acmtype
                //noinspection DuplicateBranchesInSwitch
                switch(instancejson.getString("acmtype")){
                    case "acm-tag":
                        // configure acmtag.tag
                        ConfigPropertyAssign setAcmTagTag = factory.createConfigPropertyAssign();
                        setAcmTagTag.setInstance(instance);
                        Property acmTagTag = getPropInThing(thing, "tag");
                        setAcmTagTag.setProperty(acmTagTag);
                        StringLiteral acmTagTagVal = factory.createStringLiteral();
                        acmTagTagVal.setStringValue(instancejson.getString("name"));
                        setAcmTagTag.setInit(acmTagTagVal);
                        config.getPropassigns().add(setAcmTagTag);

                        // configure acmtag.dx
                        ConfigPropertyAssign setAcmTagIdx = factory.createConfigPropertyAssign();
                        setAcmTagIdx.setInstance(instance);
                        Property acmTagIdx = getPropInThing(thing, "idx");
                        setAcmTagIdx.setProperty(acmTagIdx);
                        IntegerLiteral acmTagIdxVal = factory.createIntegerLiteral();
                        acmTagIdxVal.setIntValue(tagIndex);
                        setAcmTagIdx.setInit(acmTagIdxVal);
                        config.getPropassigns().add(setAcmTagIdx);

                        // increment index value
                        // the way this works to set the ACM buffer length is that the instance array is sorted
                        // (...tags, ..syncs, ...acms)
                        // so it essentially tracks how many tags were added
                        tagIndex++;
                        break;
                    case "acm-sync":
                        // configure acmsync.bufferlen
                        ConfigPropertyAssign setAcmSyncBufferLen = factory.createConfigPropertyAssign();
                        setAcmSyncBufferLen.setInstance(instance);
                        Property acmSyncbufferLen = getPropInThing(thing, "bufferlen");
                        setAcmSyncBufferLen.setProperty(acmSyncbufferLen);
                        IntegerLiteral acmSyncBufferLenVal = factory.createIntegerLiteral();
                        acmSyncBufferLenVal.setIntValue(tagIndex);
                        setAcmSyncBufferLen.setInit(acmSyncBufferLenVal);
                        config.getPropassigns().add(setAcmSyncBufferLen);
                        break;
                    case "acm":
                        // configure acm.bufferlen
                        ConfigPropertyAssign setAcmBufferLen = factory.createConfigPropertyAssign();
                        setAcmBufferLen.setInstance(instance);
                        Property acmBufferLen = getPropInThing(thing, "bufferlen");
                        setAcmBufferLen.setProperty(acmBufferLen);
                        IntegerLiteral acmBufferLenVal = factory.createIntegerLiteral();
                        acmBufferLenVal.setIntValue(tagIndex);
                        setAcmBufferLen.setInit(acmBufferLenVal);
                        config.getPropassigns().add(setAcmBufferLen);
                        break;
                    default: break;
                }
            }

            // first sort ACM connectors so the ports pass through naturally in the next loop and it makes finding the dead acm conn easier
            final Function<String, Integer> val = (s) -> {switch(s){ case "acm": return 1; case "acm-sync": return 2; case "acm-tag": return 3; default: return 0;}};
            acmConnectors.sort((o1, o2) -> {
                int fval=0, sval=0;
                try{
                    fval = val.apply(o1.getJSONObject("to").getString("type"));
                    sval = val.apply(o2.getJSONObject("to").getString("type"));
                }catch(Exception ignored){
                    //ignored.printStackTrace();
                }
                return sval - fval;
            });

            // find the connector(s) deleted by the ACM
            // get all possible inputs and outputs
            List<Instance> acminputinstances = acmConnectors.stream().map(acmConn ->{
                try {
                    Thing acminputthing = findThingFromWIMACName(input_model, acmConn.getJSONObject("from").getString("name"));
                    return getInstanceOfThingNamed(config, acminputthing, acmConn.getJSONObject("from").getString("name"));
                } catch (JSONException ignored) {
                    return null;
                }
            }).filter(Objects::nonNull).collect(Collectors.toList());
            List<Instance> acmoutputinstances = acmConnectors.stream().map(acmConn -> {
                try {
                    Thing acmoutputthing = findThingFromWIMACName(input_model, acmConn.getJSONObject("to").getString("name"));
                    return getInstanceOfThingNamed(config, acmoutputthing, acmConn.getJSONObject("to").getString("name"));
                } catch (JSONException ignored) {
                    return null;
                }
            }).filter(Objects::nonNull).collect(Collectors.toList());
            //System.out.println(acminputinstances);
            //System.out.println(acmoutputinstances);
            // and a list of possible connectors from this destination
            List<AbstractConnector> acmToConnectors = deadConnectors.stream().filter(
                abstractConnector -> {
                    try {
                        Connector conn = (Connector) abstractConnector;
                        return acminputinstances.contains(conn.getSrv()) && acmoutputinstances.contains(conn.getCli()); /*conn.getSrv().equals(acmoutputinstance);*/
                    } catch(Exception ignored){
                        System.out.println("excepted");
                        return false;
                    }
                }
            ).collect(Collectors.toList());
            Connector deadACMConnector = (Connector) acmToConnectors.get(0);

            // get port information from dead ACM connector
            ProvidedPort fromPortDead = deadACMConnector.getProvided();
            RequiredPort toPortDead = deadACMConnector.getRequired();
            String fromPortName = fromPortDead.getName();
            String toPortName = toPortDead.getName();

            // add the ACM datatypes
            HashMap<String, Message> acmMessages = addACMDatatypes(input_model, deadACMConnector, factory);

            // get the name of the message to intercept and put through the tagging
            List<PlatformAnnotation> acmannots = deadACMConnector.getRequired().getAnnotations().stream().filter(f -> f.getName().equals("acm")).collect(Collectors.toList());
            String messageToIntercept = null;
            if(acmannots.size() >  0) {
                messageToIntercept  = acmannots.get(0).getValue();
            }
            // get the name of the cancel message
            List<PlatformAnnotation> acmannots3 = deadACMConnector.getRequired().getAnnotations().stream().filter(f -> f.getName().equals("acmcancel")).collect(Collectors.toList());
            String messageToCancel = null;
            if(acmannots3.size() >  0) {
                messageToCancel  = acmannots3.get(0).getValue();
            }
            // get the cancel message params
            List<PlatformAnnotation> acmannots9 = deadACMConnector.getRequired().getAnnotations().stream().filter(f -> f.getName().equals("acmcancelparams")).collect(Collectors.toList());
            String cancelParams = null;
            if(acmannots9.size() >  0) {
                cancelParams  = acmannots9.get(0).getValue();
            }

            // fill the ACMTagThing behaviour
            Thing ACMTagThing = fillACMTagThing(input_model, deadACMConnector, factory, acmMessages.get("tag"), messageToIntercept);
            Thing ACMSyncThing = fillACMSyncThing(input_model, deadACMConnector, factory, acmMessages.get("tag"), acmMessages.get("acm"), messageToIntercept, targetLanguage);
            Thing ACMThing = fillACMThing(input_model, deadACMConnector, factory, acmMessages.get("tag"), acmMessages.get("acm"), messageToIntercept, messageToCancel, cancelParams, targetLanguage);

            // add all the ACM connectors
            for(JSONObject acmConnectorJSON : acmConnectors){
                try {
                    // get instances
                    Thing fromCmpThing = findThingFromWIMACName(input_model, acmConnectorJSON.getJSONObject("from").getString("name"));
                    Thing toCmpThing = findThingFromWIMACName(input_model, acmConnectorJSON.getJSONObject("to").getString("name"));
                    Instance from = getInstanceOfThingNamed(config, fromCmpThing, acmConnectorJSON.getJSONObject("from").getString("name"));
                    Instance to = getInstanceOfThingNamed(config, toCmpThing, acmConnectorJSON.getJSONObject("to").getString("name"));

                    //System.out.println("Link: " + fromPortName + "-" + toPortName);
                    //System.out.println("from:" + fromCmpThing.getName() + ":");
                    //ThingMLHelpers.allPorts(fromCmpThing).forEach(p -> System.out.println("-" + p.getName()));
                    //System.out.println("to:" + toCmpThing.getName() + ":");
                    //ThingMLHelpers.allPorts(toCmpThing).forEach(p -> System.out.println("-" + p.getName()));

                    // get ports inside instance
                    //ProvidedPort fromPort = (ProvidedPort) ThingMLHelpers.findPort(fromCmpThing, fromPortName, false).get(0);
                    ProvidedPort  fromPort =  null;
                    List<Port> fromPortList = null;
                    for(AbstractConnector c : acmToConnectors) {
                        fromPortDead = ((Connector)c).getProvided();
                        fromPortName = fromPortDead.getName();
                        //System.out.println("--testing connector " + ((Connector) c).getCli().getName() + "=>" + ((Connector) c).getSrv().getName());
                        //System.out.println("--testing from port " + fromPortName);
                        fromPortList = ThingMLHelpers.findPort(fromCmpThing, fromPortName, false);
                        if (fromPortList.size() > 0) {
                            fromPort = (ProvidedPort) fromPortList.get(0);
                        }
                    }
                    if(fromPort == null) {
                        // alright so we couldn't find the port using ACM data, fallback to finding an ACM port on the destination thing
                        //System.out.println("--failed to find the port in acmConnectors, trying messageToIntercept (" + messageToIntercept + ")");
                        String finalMessageToIntercept = messageToIntercept;
                        fromPort = (ProvidedPort) fromCmpThing.getPorts().stream()
                                .filter(p -> p.getSends().stream()
                                        .filter(r -> r.getName().equals(finalMessageToIntercept)).count() > 0)
                                .collect(Collectors.toList())
                                .get(0);

                        // still wrong, throw
                        if(fromPort == null) {
                            throw new Exception("---no from port found");
                        }
                    }

                    // test all possible ports
                    RequiredPort toPort = null;
                    List<Port> toPortList = null;
                    for(AbstractConnector c : acmToConnectors) {
                        toPortDead = ((Connector)c).getRequired();
                        toPortName = toPortDead.getName();
                        //System.out.println("--testing port " + toPortName);
                        toPortList = ThingMLHelpers.findPort(toCmpThing, toPortName, false);
                        if (toPortList.size() > 0) {
                            toPort = (RequiredPort) toPortList.get(0);
                        }
                    }
                    if(toPort == null) {
                        // alright so we couldn't find the port using ACM data, fallback to finding an ACM port on the destination thing
                        //System.out.println("--failed to find the port in acmConnectors, trying messageToIntercept (" + messageToIntercept + ")");
                        String finalMessageToIntercept = messageToIntercept;
                        toPort = (RequiredPort) toCmpThing.getPorts().stream()
                                .filter(p -> p.getReceives().stream()
                                .filter(r -> r.getName().equals(finalMessageToIntercept)).count() > 0)
                                .collect(Collectors.toList())
                                .get(0);

                        // still wrong, throw
                        if(toPort == null) {
                            throw new Exception("---no to port found");
                        }
                    }
                    Connector acmConnector = factory.createConnector();
                    acmConnector.setCli(to);
                    acmConnector.setRequired(toPort);
                    acmConnector.setSrv(from);
                    acmConnector.setProvided(fromPort);
                    config.getConnectors().add(acmConnector);
                } catch(Exception ex){
                    System.err.println("Failed to create ACM connector: " + ex.getMessage());
                    //ex.printStackTrace();
                }
            }

            // save model
            ThingMLModel output = ThingMLHelpers.flattenModel(input_model);
            debugPrints(output);
            ThingMLCompiler.saveAsThingML(output, thingmlOutputPath);
            return "good";
        } catch (Exception e) {
            e.printStackTrace();
            return "error \n" + Arrays.toString(e.getStackTrace());
        }
    }

    private Thing fillACMThing(ThingMLModel model, Connector replacedConnector, ThingMLFactory factory, Message tagMessage, Message acmMessage, String messageToIntercept, String messageToCancel, String cancelParams, String targetLanguage) {
        Thing ACMThing = ThingMLHelpers.findThing(model, "ACMThing", false).get(0);

        // include the Things holding the messages
        EList<Message> allmsgsw = replacedConnector.getProvided().getReceives();
        allmsgsw.addAll(replacedConnector.getProvided().getSends());
        allmsgsw.addAll(replacedConnector.getRequired().getReceives());
        allmsgsw.addAll(replacedConnector.getRequired().getSends());
        List<Message> allmsgs = allmsgsw.stream().distinct().collect(Collectors.toList());
        for(Message rcv : allmsgs) {
            ACMThing.getIncludes().add(ThingMLHelpers.findContainingThing(rcv));
        }

        ProvidedPort outputPort = factory.createProvidedPort();
        outputPort.setName(replacedConnector.getProvided().getName());
        outputPort.getAnnotations().add(getSyncSendAnnotation(factory));
        outputPort.getSends().addAll(replacedConnector.getProvided().getSends());
        outputPort.getReceives().addAll(replacedConnector.getProvided().getReceives());
        ACMThing.getPorts().add(outputPort);

        RequiredPort inputPort = factory.createRequiredPort();
        inputPort.setName(replacedConnector.getRequired().getName());
        inputPort.getAnnotations().add(getSyncSendAnnotation(factory));
        inputPort.getSends().addAll(replacedConnector.getRequired().getSends());
        inputPort.getReceives().add(acmMessage);
        if(replacedConnector.getRequired().getReceives().size() > 1) {
            inputPort.getReceives().addAll(replacedConnector.getRequired().getReceives().subList(1, replacedConnector.getRequired().getReceives().size()));
        }
        ACMThing.getPorts().add(inputPort);

        // create buffers
        Property bufferlen = ACMThing.getProperties().stream().filter(p -> p.getName().equals("bufferlen")).collect(Collectors.toList()).get(0);
        List<Property> buffers = createBuffers(factory, tagMessage, ACMThing, bufferlen, "");

        // add events
        State acmsyncstate = ACMThing.getBehaviour().getInitial();
        for(Message provMess : replacedConnector.getRequired().getReceives()){
            // internal
            InternalTransition newInternal = factory.createInternalTransition();

            // event
            ReceiveMessage event = factory.createReceiveMessage();
            event.setName("msg_" + provMess.getName());
            event.setMessage(provMess);
            event.setPort(inputPort);
            newInternal.setEvent(event);

            // action block
            ActionBlock action = factory.createActionBlock();

            // store data if it needs to be sync'd
            if(provMess.getName().equals(messageToIntercept)) {
                // the syncd message uses acm message
                event.setMessage(acmMessage);

                // copy data
                for(int i = 0; i < acmMessage.getParameters().size(); i++){
                    Parameter param = acmMessage.getParameters().get(i);
                    Property buffer = buffers.get(i);

                    EventReference paramRef = factory.createEventReference();
                    paramRef.setParameter(param);
                    paramRef.setReceiveMsg(event);

                    VariableAssignment assignment = factory.createVariableAssignment();
                    assignment.setProperty(buffer);
                    assignment.setExpression(paramRef);

                    action.getActions().add(assignment);
                }

                // local sent variable
                LocalVariable sent = factory.createLocalVariable();
                sent.setName("sent");
                TypeRef senttr = factory.createTypeRef();
                senttr.setType(ThingMLHelpers.findSimpleType(model, "Integer", false).get(0));
                sent.setTypeRef(senttr);
                IntegerLiteral sentinit = factory.createIntegerLiteral();
                sentinit.setIntValue(0);
                sent.setInit(sentinit);
                action.getActions().add(sent);

                // for on first buffer
                if(buffers.size() > 0){
                    Property bufferLoop = buffers.get(0);
                    //for(s: String, index: Integer in buffer_s2) do end
                    ForAction forAction = factory.createForAction();
                    action.getActions().add(forAction);

                    // variable not really used but needs to be of a correct type
                    Parameter forVariable = factory.createParameter();
                    TypeRef forVarTR = factory.createTypeRef();
                    forVarTR.setType(bufferLoop.getTypeRef().getType());
                    forVariable.setTypeRef(forVarTR);
                    forVariable.setName("value");
                    forAction.setVariable(forVariable);

                    // index, used
                    Parameter forIndex = factory.createParameter();
                    TypeRef forIndexTR = factory.createTypeRef();
                    forIndexTR.setType(ThingMLHelpers.findSimpleType(model, "Integer", false).get(0));
                    forIndex.setTypeRef(forIndexTR);
                    forIndex.setName("index");
                    forAction.setIndex(forIndex);

                    // select buffer
                    PropertyReference buffPropRef = factory.createPropertyReference();
                    buffPropRef.setProperty(bufferLoop);
                    forAction.setArray(buffPropRef);

                    // for body
                    ActionBlock forBody = factory.createActionBlock();
                    forAction.setAction(forBody);

                    // if not sent
                    ConditionalAction ifNotSent = factory.createConditionalAction();
                    EqualsExpression notsent = factory.createEqualsExpression();
                    PropertyReference sentref = factory.createPropertyReference();
                    sentref.setProperty(sent);
                    notsent.setLhs(sentref);
                    IntegerLiteral sentzerolit = factory.createIntegerLiteral();
                    sentzerolit.setIntValue(0);
                    notsent.setRhs(sentzerolit);
                    ifNotSent.setCondition(notsent);
                    forBody.getActions().add(ifNotSent);

                    ActionBlock sendchk = factory.createActionBlock();
                    ifNotSent.setAction(sendchk);

                    // if value isn't null
                    ConditionalAction ifvaluenotnull = factory.createConditionalAction();
                    NotEqualsExpression notnull = factory.createNotEqualsExpression();
                    PropertyReference valueref = factory.createPropertyReference();
                    valueref.setProperty(forVariable);
                    notnull.setLhs(valueref);
                    Expression externnull = getNullExpressionForProperty(factory, forVariable.getTypeRef(), targetLanguage);
                    notnull.setRhs(externnull);
                    ifvaluenotnull.setCondition(notnull);

                    ActionBlock send = factory.createActionBlock();
                    ifvaluenotnull.setAction(send);

                    sendchk.getActions().add(ifvaluenotnull);

                    // sent ;)
                    VariableAssignment sentset = factory.createVariableAssignment();
                    sentset.setProperty(sent);
                    IntegerLiteral sentsetone = factory.createIntegerLiteral();
                    sentsetone.setIntValue(1);
                    sentset.setExpression(sentsetone);
                    send.getActions().add(sentset);

                    // send the message
                    SendAction sendAction = factory.createSendAction();
                    sendAction.setMessage(replacedConnector.getRequired().getReceives().stream().filter(m -> m.getName().equals(messageToIntercept)).collect(Collectors.toList()).get(0));
                    sendAction.setPort(outputPort);
                    for(Property buff : buffers){
                        PropertyReference ref = factory.createPropertyReference();
                        ref.setProperty(buff);
                        ArrayIndex i = factory.createArrayIndex();
                        i.setArray(ref);
                        PropertyReference iref = factory.createPropertyReference();
                        iref.setProperty(forIndex);
                        i.setIndex(iref);
                        sendAction.getParameters().add(i);
                    }
                    send.getActions().add(sendAction);

                    // if we sent already send out the cancel message
                    ActionBlock cancelled = factory.createActionBlock();
                    ifNotSent.setElseAction(cancelled);

                    SendAction sendActionCancel = factory.createSendAction();
                    sendActionCancel.setMessage(replacedConnector.getRequired().getReceives().stream().filter(m -> m.getName().equals(messageToCancel)).collect(Collectors.toList()).get(0));
                    sendActionCancel.setPort(outputPort);
                    String[] paramsvalls = cancelParams.substring(1, cancelParams.length()-1).split(",");
                    List<Parameter> paramsmsgs = sendActionCancel.getMessage().getParameters();
                    for(int i = 0; i < paramsmsgs.size(); i++){
                        // init null
                        Expression expr = getNullExpressionForProperty(factory, paramsmsgs.get(i).getTypeRef(), targetLanguage);
                        switch(paramsmsgs.get(i).getTypeRef().getType().getName()){
                            // per type switch for primitive
                            case "Integer": ((IntegerLiteral)expr).setIntValue(Integer.parseInt(paramsvalls[i])); break;
                            case "String": expr = factory.createStringLiteral(); ((StringLiteral)expr).setStringValue(paramsvalls[i]); break;
                            default: break;
                        }
                        sendActionCancel.getParameters().add(expr);
                    }
                    cancelled.getActions().add(sendActionCancel);
                }
            } else {
                // just forward the message
                SendAction out = factory.createSendAction();
                out.setPort(outputPort);
                out.setMessage(provMess);
                EList<Expression> params = out.getParameters();

                // add the parameters from the original message
                EList<Parameter> provMessParams = provMess.getParameters();
                for (Parameter provMessParam : provMessParams) {
                    EventReference eventReference = factory.createEventReference();
                    eventReference.setReceiveMsg(event);
                    eventReference.setParameter(provMessParam);
                    params.add(eventReference);
                }

                action.getActions().add(out);
            }
            newInternal.setAction(action);

            // add to state
            acmsyncstate.getInternal().add(newInternal);
        }

        return ACMThing;
    }

    private Thing fillACMTagThing(ThingMLModel model, Connector replacedConnector, ThingMLFactory factory, Message tagMessage, String messageToIntercept) {
        // get the thing
        Thing ACMTagThing = ThingMLHelpers.findThing(model, "ACMTagThing", false).get(0);

        // include the Things holding the messages
        EList<Message> allmsgsw = replacedConnector.getProvided().getReceives();
        allmsgsw.addAll(replacedConnector.getProvided().getSends());
        allmsgsw.addAll(replacedConnector.getRequired().getReceives());
        allmsgsw.addAll(replacedConnector.getRequired().getSends());
        List<Message> allmsgs = allmsgsw.stream().distinct().collect(Collectors.toList());
        for(Message rcv : allmsgs) {
            ACMTagThing.getIncludes().add(ThingMLHelpers.findContainingThing(rcv));
        }

        // create the provided port from the replaced connector's port
        ProvidedPort outputPort = factory.createProvidedPort();
        outputPort.setName(replacedConnector.getProvided().getName());
        // this is needed to pass strings as mentioned in some ThingML doc/sources
        // add it everywhere because we're manipulating strings
        outputPort.getAnnotations().add(getSyncSendAnnotation(factory));
        // add the new tag message
        outputPort.getSends().add(tagMessage);
        // copy the other messages
        if(replacedConnector.getProvided().getSends().size() > 1) {
            // this sounds like it would go out of bounds of the list but subList's to parameter is exclusive
            // ie you need to give it n to get the item at index n-1 in the sublist
            outputPort.getSends().addAll(replacedConnector.getProvided().getSends().subList(1, replacedConnector.getProvided().getSends().size()));
        }
        // copy all the receives that shouldn't be there i think but idk
        outputPort.getReceives().addAll(replacedConnector.getProvided().getReceives());
        ACMTagThing.getPorts().add(outputPort);

        // create the provided port which is very close to the original port
        RequiredPort inputPort = factory.createRequiredPort();
        inputPort.setName(replacedConnector.getRequired().getName());
        inputPort.getAnnotations().add(getSyncSendAnnotation(factory));
        // copy all other messages
        inputPort.getSends().addAll(replacedConnector.getRequired().getSends());
        inputPort.getReceives().addAll(replacedConnector.getRequired().getReceives());
        ACMTagThing.getPorts().add(inputPort);

        // add events
        State acmtagstate = ACMTagThing.getBehaviour().getInitial();
        for(Message provMess : replacedConnector.getRequired().getReceives()){
            // internal
            InternalTransition newInternal = factory.createInternalTransition();

            // event
            ReceiveMessage event = factory.createReceiveMessage();
            event.setName("msg_" + provMess.getName());
            event.setMessage(provMess);
            event.setPort(inputPort);
            newInternal.setEvent(event);

            // action block
            ActionBlock action = factory.createActionBlock();
            // output action
            SendAction out = factory.createSendAction();
            out.setPort(outputPort);
            out.setMessage(provMess);
            EList<Expression> params = out.getParameters();

            // add acm parameters if it's an acm'd message
            if(provMess.getName().equals(messageToIntercept)) {
                // emit the tag message
                out.setMessage(tagMessage);

                // internal parameters (tag, idx)
                PropertyReference tagRef = factory.createPropertyReference();
                tagRef.setProperty(ThingMLHelpers.findProperty(ACMTagThing, "tag", false).get(0));
                params.add(tagRef);
                PropertyReference idxRef = factory.createPropertyReference();
                idxRef.setProperty(ThingMLHelpers.findProperty(ACMTagThing, "idx", false).get(0));
                params.add(idxRef);
            }

            // add the parameters from the original message
            EList<Parameter> provMessParams = provMess.getParameters();
            for (Parameter provMessParam : provMessParams) {
                EventReference eventReference = factory.createEventReference();
                eventReference.setReceiveMsg(event);
                eventReference.setParameter(provMessParam);
                params.add(eventReference);
            }

            action.getActions().add(out);
            newInternal.setAction(action);

            // add to state
            acmtagstate.getInternal().add(newInternal);
        }

        return ACMTagThing;
    }

    private Thing fillACMSyncThing(ThingMLModel model, Connector replacedConnector, ThingMLFactory factory, Message tagMessage, Message syncMessage, String messageToIntercept, String targetLanguage) {
        // -----
        // note: most of this code is similar to fillACMTagThing; check that method for more detailed comments
        // -----
        // get the thing
        Thing ACMSyncThing = ThingMLHelpers.findThing(model, "ACMSyncThing", false).get(0);

        // include all references
        EList<Message> allmsgsw = replacedConnector.getProvided().getReceives();
        allmsgsw.addAll(replacedConnector.getProvided().getSends());
        allmsgsw.addAll(replacedConnector.getRequired().getReceives());
        allmsgsw.addAll(replacedConnector.getRequired().getSends());
        List<Message> allmsgs = allmsgsw.stream().distinct().collect(Collectors.toList());
        for (Message msg : allmsgs) {
            Thing toInclude = ThingMLHelpers.findContainingThing(msg);
            ACMSyncThing.getIncludes().add(toInclude);
        }

        // create required port
        ProvidedPort outputPort = factory.createProvidedPort();
        outputPort.setName(replacedConnector.getProvided().getName());
        outputPort.getAnnotations().add(getSyncSendAnnotation(factory));
        outputPort.getSends().add(syncMessage);
        if(replacedConnector.getProvided().getSends().size() > 1) {
            outputPort.getSends().addAll(replacedConnector.getProvided().getSends().subList(1, replacedConnector.getProvided().getSends().size()));
        }
        outputPort.getReceives().addAll(replacedConnector.getProvided().getReceives());
        ACMSyncThing.getPorts().add(outputPort);

        // create provided port
        RequiredPort inputPort = factory.createRequiredPort();
        inputPort.setName(replacedConnector.getRequired().getName());
        inputPort.getAnnotations().add(getSyncSendAnnotation(factory));
        inputPort.getReceives().add(tagMessage);
        if(replacedConnector.getProvided().getReceives().size() > 1) {
            inputPort.getReceives().addAll(replacedConnector.getRequired().getReceives().subList(1, replacedConnector.getRequired().getReceives().size()));
        }
        inputPort.getSends().addAll(replacedConnector.getRequired().getSends());
        ACMSyncThing.getPorts().add(inputPort);

        // create buffers
        Property bufferlen = ACMSyncThing.getProperties().stream().filter(p -> p.getName().equals("bufferlen")).collect(Collectors.toList()).get(0);
        List<Property> bufferGroup1 = createBuffers(factory, tagMessage, ACMSyncThing, bufferlen, "");
        List<Property> bufferGroup2 = createBuffers(factory, tagMessage, ACMSyncThing, bufferlen, "2");

        ActionBlock clearBlockBufferGroup1 = null, clearBlockBufferGroup2 = null;
        try {
            // locate the blocks to generate sends in
            // internal timer action
            InternalTransition emitterTransitionEvent = ACMSyncThing.getBehaviour().getInitial().getInternal().get(0);
            // action do ... end block
            ActionBlock emitterTransitionActionBlock = (ActionBlock) emitterTransitionEvent.getAction();
            // if data available
            ConditionalAction ifDataAvailable = (ConditionalAction) emitterTransitionActionBlock.getActions().get(0);
            // if buffer used 0
            ConditionalAction ifBufferUsed = (ConditionalAction) ((ActionBlock)ifDataAvailable.getAction()).getActions().get(1);

            // send buffer 0 block (else block)
            clearBlockBufferGroup1 = (ActionBlock) ifBufferUsed.getElseAction();
            // send buffer 1 block (if block)
            clearBlockBufferGroup2 = (ActionBlock) ifBufferUsed.getAction();
        } catch(Exception ex){
            System.err.println("Exception locating appropriate action blocks:");
            ex.printStackTrace(System.err);
        }
        if(clearBlockBufferGroup2 != null && clearBlockBufferGroup1 != null){
            // create the output calls
            createOutputCalls(model, factory, clearBlockBufferGroup1, outputPort, syncMessage, bufferGroup2);
            createOutputCalls(model, factory, clearBlockBufferGroup2, outputPort, syncMessage, bufferGroup1);
            // create the buffer clear calls
            createBufferClear(model, factory, clearBlockBufferGroup1, bufferGroup1, targetLanguage);
            createBufferClear(model, factory, clearBlockBufferGroup2, bufferGroup2, targetLanguage);
        } else {
            System.err.println("Couldn't locate the appropriate action blocks, check code structure.");
        }

        // add events
        State acmsyncstate = ACMSyncThing.getBehaviour().getInitial();
        for(Message provMess : replacedConnector.getRequired().getReceives()){
            // internal
            InternalTransition newInternal = factory.createInternalTransition();

            // event
            ReceiveMessage event = factory.createReceiveMessage();
            event.setName("msg_" + provMess.getName());
            event.setMessage(provMess);
            event.setPort(inputPort);
            newInternal.setEvent(event);

            // action block
            ActionBlock action = factory.createActionBlock();

            // store data if it needs to be sync'd
            if(provMess.getName().equals(messageToIntercept)) {
                // the syncd message uses tagmsg
                event.setMessage(tagMessage);

                // switch buffer
                ConditionalAction ifBufferUsed = factory.createConditionalAction();

                PropertyReference bufferUsedRef = factory.createPropertyReference();
                Property bufferUsedProp = ThingMLHelpers.findProperty(ACMSyncThing, "bufferUsed", false).get(0);
                bufferUsedRef.setProperty(bufferUsedProp);

                // bufferUsed == 0
                EqualsExpression bufferUsed0 = factory.createEqualsExpression();
                bufferUsed0.setLhs(bufferUsedRef);
                IntegerLiteral zero = factory.createIntegerLiteral();
                zero.setIntValue(0);
                bufferUsed0.setRhs(zero);
                ifBufferUsed.setCondition(bufferUsed0);

                // action uses bufferGroup1
                ActionBlock bufferStore1 = createBufferStore(model, factory, event, tagMessage, ACMSyncThing, bufferGroup1);
                ifBufferUsed.setAction(bufferStore1);

                // else uses bufferGroup2
                ActionBlock bufferStore2 = createBufferStore(model, factory, event, tagMessage, ACMSyncThing, bufferGroup2);
                ifBufferUsed.setElseAction(bufferStore2);

                action.getActions().add(ifBufferUsed);
            } else {
                // just forward the message
                SendAction out = factory.createSendAction();
                out.setPort(outputPort);
                out.setMessage(outputPort.getSends().stream().filter(sendmsg -> sendmsg.getName().equals(provMess.getName())).findFirst().get());
                EList<Expression> params = out.getParameters();

               // add the parameters from the original message
                EList<Parameter> provMessParams = provMess.getParameters();
                for (Parameter provMessParam : provMessParams) {
                    EventReference eventReference = factory.createEventReference();
                    eventReference.setReceiveMsg(event);
                    eventReference.setParameter(provMessParam);
                    params.add(eventReference);
                }

                action.getActions().add(out);
            }
            newInternal.setAction(action);

            // add to state
            acmsyncstate.getInternal().add(newInternal);
        }

        return ACMSyncThing;
    }

    private void createOutputCalls(ThingMLModel model, ThingMLFactory factory, ActionBlock block, Port outPort, Message acmMessage, List<Property> buffers) {
        SendAction out = factory.createSendAction();
        out.setPort(outPort);
        out.setMessage(acmMessage);
        for(Property buff : buffers){
            PropertyReference buffref = factory.createPropertyReference();
            buffref.setProperty(buff);
            out.getParameters().add(buffref);
        }
        block.getActions().add(out);
    }

    private ActionBlock createBufferStore(ThingMLModel model, ThingMLFactory factory, ReceiveMessage message, Message tagMessage, Thing ACMSyncThing, List<Property> buffers) {
        ActionBlock w = factory.createActionBlock();

        // set the data available flag
        VariableAssignment dataAvailable = factory.createVariableAssignment();
        dataAvailable.setProperty(ThingMLHelpers.findProperty(ACMSyncThing, "dataavailable", false).get(0));
        IntegerLiteral one = factory.createIntegerLiteral();
        one.setIntValue(1);
        dataAvailable.setExpression(one);
        w.getActions().add(dataAvailable);

        // store all data
        for(int i = 0; i < buffers.size(); i++){
            Property buff = buffers.get(i);
            VariableAssignment assignment = factory.createVariableAssignment();
            assignment.setProperty(buff);

            // create event reference idx
            EventReference idxRef = factory.createEventReference();
            idxRef.setReceiveMsg(message);
            idxRef.setParameter(tagMessage.getParameters().stream().filter(p -> p.getName().equals("idx")).collect(Collectors.toList()).get(0));
            assignment.setIndex(idxRef);

            EventReference eventReference = factory.createEventReference();
            eventReference.setReceiveMsg(message);
            // skip idx & tag
            eventReference.setParameter(tagMessage.getParameters().get(2+i));
            assignment.setExpression(eventReference);

            w.getActions().add(assignment);
        }
        return w;
    }

    private void createBufferClear(ThingMLModel model, ThingMLFactory factory, ActionBlock block, List<Property> buffers, String targetLanguage) {
        if(buffers.size() > 0){
            Property bufferLoop = buffers.get(0);
            //for(s: String, index: Integer in buffer_s2) do end
            ForAction forAction = factory.createForAction();

            // variable not really used but needs to be of a correct type
            Parameter forVariable = factory.createParameter();
            TypeRef forVarTR = factory.createTypeRef();
            forVarTR.setType(bufferLoop.getTypeRef().getType());
            forVariable.setTypeRef(forVarTR);
            forVariable.setName("ignored");
            forAction.setVariable(forVariable);

            // index, used
            Parameter forIndex = factory.createParameter();
            TypeRef forIndexTR = factory.createTypeRef();
            forIndexTR.setType(ThingMLHelpers.findSimpleType(model, "Integer", false).get(0));
            forIndex.setTypeRef(forIndexTR);
            forIndex.setName("index");
            forAction.setIndex(forIndex);

            // select buffer
            PropertyReference buffPropRef = factory.createPropertyReference();
            buffPropRef.setProperty(bufferLoop);
            forAction.setArray(buffPropRef);

            // sends
            ActionBlock sendBlock = factory.createActionBlock();
            for(Property buffer:  buffers){
                VariableAssignment clear = factory.createVariableAssignment();
                clear.setProperty(buffer);
                PropertyReference clearIndexRef = factory.createPropertyReference();
                clearIndexRef.setProperty(forIndex);
                clear.setIndex(clearIndexRef);
                clear.setExpression(getNullExpressionForProperty(factory, buffer.getTypeRef(), targetLanguage));
                sendBlock.getActions().add(clear);
            }
            forAction.setAction(sendBlock);

            block.getActions().add(forAction);
        }
    }

    private Expression getNullExpressionForProperty(ThingMLFactory factory, TypeRef type, String targetLanguage) {
        Expression ret = null;
        switch(type.getType().getName()){
            case "String":
                // this adds the java style null
                // will probably need to be tinkered with for Arduino and others
                ret = factory.createExternExpression();
                ((ExternExpression)ret).setExpression("null");
                break;
            case "Integer":
                ret = factory.createIntegerLiteral();
                // yolo i guess
                if("java".equals(targetLanguage)) {
                    ((IntegerLiteral) ret).setIntValue(Integer.MAX_VALUE);
                } else if("arduino".equals(targetLanguage)){
                    ((IntegerLiteral) ret).setIntValue(32767);
                }
                break;
            default:
                ret = factory.createIntegerLiteral();
                ((IntegerLiteral)ret).setIntValue(0);
                break;
        }
        return ret;
    }

    private List<Property> createBuffers(ThingMLFactory factory, Message tagMessage, Thing ACMSyncThing, Property bufferlen, String suffix) {
        List<Property> buffers = new LinkedList<>();
        // create a buffer for a tagMessage
        // start at 2 to skip idx and tag
        for(int i = 2; i < tagMessage.getParameters().size(); i++){
            Parameter p = tagMessage.getParameters().get(i);
            Property buffprop = factory.createProperty();
            buffprop.setName("buffer_" + p.getName() + suffix);
            TypeRef bptr = factory.createTypeRef();
            bptr.setType(p.getTypeRef().getType());
            bptr.setIsArray(true);
            PropertyReference bptrexpr = factory.createPropertyReference();
            bptrexpr.setProperty(bufferlen);
            bptr.setCardinality(bptrexpr);
            buffprop.setTypeRef(bptr);
            ACMSyncThing.getProperties().add(buffprop);
            buffers.add(buffprop);
        }
        return buffers;
    }


    private PlatformAnnotation getSyncSendAnnotation(ThingMLFactory factory){
        PlatformAnnotation syncsend = factory.createPlatformAnnotation();
        syncsend.setName("sync_send");
        syncsend.setValue("true");
        return syncsend;
    }

    private HashMap<String, Message> addACMDatatypes(ThingMLModel model, Connector replacedConnector, ThingMLFactory factory) {
        HashMap<String, Message> ret = new HashMap<>();
        // get the message type used by the replaced connected
        // take the first message in the list?
        Message replacedMessage = replacedConnector.getRequired().getReceives().get(0);

        // get the thing that holds the messages
        Thing ACMDatatypesThing = ThingMLHelpers.findThing(model, "ACMDatatypes", false).get(0);

        // create the tag message
        Message TagMessage = factory.createMessage();
        TagMessage.setName("tag_" + replacedMessage.getName());
        // add the new parameters
        Parameter tag = factory.createParameter();
        tag.setName("tag");
        TypeRef tagString = factory.createTypeRef();
        tagString.setType(ThingMLHelpers.findSimpleType(model, "String", false).get(0));
        tag.setTypeRef(tagString);
        TagMessage.getParameters().add(tag);
        Parameter idx = factory.createParameter();
        idx.setName("idx");
        TypeRef idxInt = factory.createTypeRef();
        idxInt.setType(ThingMLHelpers.findSimpleType(model, "Integer", false).get(0));
        idx.setTypeRef(idxInt);
        TagMessage.getParameters().add(idx);
        // copy the old
        // parameters are derived from the replaced message parameters
        for(Parameter p : replacedMessage.getParameters()){
            Parameter tagParam = factory.createParameter();
            tagParam.setName(p.getName());
            TypeRef trold = p.getTypeRef();
            TypeRef trnew = factory.createTypeRef();
            trnew.setType(trold.getType());
            trnew.setIsArray(trold.isIsArray());
            trnew.setCardinality(trold.getCardinality());
            tagParam.setTypeRef(trnew);

            TagMessage.getParameters().add(tagParam);
        }
        ACMDatatypesThing.getMessages().add(TagMessage);
        ret.put("tag", TagMessage);

        // create the acm message
        Message ACMMessage = factory.createMessage();
        ACMMessage.setName("acm_" + replacedMessage.getName());
        // parameters are derived from the replaced message parameters
        for(Parameter p : replacedMessage.getParameters()){
            Parameter acmParam = factory.createParameter();
            acmParam.setName(p.getName());
            TypeRef trold = p.getTypeRef();
            TypeRef trnew = factory.createTypeRef();
            trnew.setType(trold.getType());
            trnew.setIsArray(true);
            trnew.setCardinality(null);
            acmParam.setTypeRef(trnew);

            ACMMessage.getParameters().add(acmParam);
        }
        ACMDatatypesThing.getMessages().add(ACMMessage);
        ret.put("acm", ACMMessage);

        // add a copy of the og message, not sure if necessary
        //ACMDatatypesThing.getMessages().add(replacedMessage);
        return ret;
    }

    private String getNameForWIMACObject(JSONObject instancejson) throws JSONException {
        //System.out.println(instancejson);
        return
                "[" + ((instancejson.getString("name") + "_" + instancejson.getString("id"))
                        .replace(" ","")
                        .replace(".", "_")
                        .replace("-", "_")) + ":" + getThingForACMType(instancejson.getString("type")) + "]";
    }

    private String getThingForACMType(String type) {
        switch(type){
            case "acm-tag": return "ACMTagThing";
            case "acm-sync": return "ACMSyncThing";
            case "acm": return "ACMThing";
            default: return null;
        }
    }

    private Thing findThingFromWIMACName(ThingMLModel model, String name){
        String[] split = splitNameType(name);
        return ThingMLHelpers.findThing(model, split[1], false).get(0);
    }

    private Instance getInstanceOfThingNamed(Configuration configuration, Thing thing, String name){
        String[] split = splitNameType(name);
        return getInstancesForThing(configuration, thing).stream().filter(i -> i.getName().equals(split[0])).collect(Collectors.toList()).get(0);
    }

    private List<Instance> getInstancesForThing(Configuration configuration, Thing thing){
        return ThingMLHelpers.allInstances(configuration).stream().filter(i -> i.getType().equals(thing)).collect(Collectors.toList());
    }

    private boolean findConnectorInList(List<JSONObject> thingConnectors, Connector conn, ThingMLModel model, Configuration config) throws JSONException {
        for(JSONObject thingConn : thingConnectors){
            try {
                // get the to and from instances
                Thing fromCmpThing = findThingFromWIMACName(model, thingConn.getJSONObject("from").getString("name"));
                Thing toCmpThing = findThingFromWIMACName(model, thingConn.getJSONObject("to").getString("name"));
                Instance from = getInstanceOfThingNamed(config, fromCmpThing, thingConn.getJSONObject("from").getString("name"));
                Instance to = getInstanceOfThingNamed(config, toCmpThing, thingConn.getJSONObject("to").getString("name"));

                /*System.out.println("////////////");
                System.out.println(from.getName());
                System.out.println(conn.getCli().getName());
                System.out.println(to.getName());
                System.out.println(conn.getSrv().getName());*/

                // so this probably checks for to and from
                if(conn.getSrv().equals(from) && conn.getCli().equals(to)){
                    // now we gotta search for ports
                    String fromPortStr = thingConn.getJSONObject("port").getString("source");
                    String toPortStr = thingConn.getJSONObject("port").getString("target");

                    Port fromPort = ThingMLHelpers.findPort(fromCmpThing, fromPortStr, false).get(0);
                    Port toPort = ThingMLHelpers.findPort(toCmpThing, toPortStr, false).get(0);

                    if(conn.getProvided().equals(fromPort) && conn.getRequired().equals(toPort)){
                        //System.out.println("port match");
                        return true;
                    } else {
                        //System.out.println("port mismatch");
                    }
                }
            } catch(Exception ex){
                System.out.println("findConnectorInList blew up");
                ex.printStackTrace();
            }
        }
        return false;
    }

    private String[] splitNameType(String cmpName) {
        String[] splitted = cmpName.split("[:]");
        return Arrays.stream(splitted).map(s -> s.replace("[", "").replace("]", "")).toArray(String[]::new);
    }

    private List<JSONObject>[] splitLinks(JSONObject model) throws JSONException {
        List<JSONObject> acmConnectors = new LinkedList<>(),
                thingConnectors = new LinkedList<>();

        JSONArray links = model.getJSONArray("links");
        for(int lnkidx = 0; lnkidx < links.length(); lnkidx++) {
            JSONObject link = links.getJSONObject(lnkidx),
                    from = link.getJSONObject("from"),
                    to = link.getJSONObject("to");
            if(from.getString("elementType").equals("ACMComponent") || to.getString("elementType").equals("ACMComponent")){
                // for ACM connectors touch up the instance names if needed so they conform to the WIMAC ThingML "notation" for later
                if(from.getString("elementType").equals("ACMComponent")){
                    // fix from
                    from.put("name", getNameForWIMACObject(from));
                }
                if(to.getString("elementType").equals("ACMComponent")){
                    // fix to
                    to.put("name", getNameForWIMACObject(to));
                }
                acmConnectors.add(link);
            } else {
                thingConnectors.add(link);
            }
        }
        return new List[]{acmConnectors, thingConnectors};
    }

    private ProvidedPort getProvidedPortInThing(Thing thing, String property) {
        // search local ports
        for (Port p:thing.getPorts()) {
            if(p instanceof ProvidedPort && p.getName().equals(property)) return (ProvidedPort) p;
        }
        // search ports in included fragments
        for(Thing include : thing.getIncludes()){
            // i think if its not a fragment the ports aren't merged?
            if(!include.isFragment()) continue;
            ProvidedPort w = getProvidedPortInThing(include, property);
            if(w!=null) return w;
        }
        return null;
    }

    private RequiredPort getRequiredPortInThing(Thing thing, String property) {
        for (Port p:thing.getPorts()) {
            if(p instanceof RequiredPort && p.getName().equals(property)) return (RequiredPort) p;
        }
        for(Thing include : thing.getIncludes()){
            if(!include.isFragment()) continue;
            RequiredPort w = getRequiredPortInThing(include, property);
            if(w!=null) return w;
        }
        return null;
    }

    private String getTimerThingForLanguage(String targetLanguage) {
        switch (targetLanguage) {
            case "arduino": return "TimerArduino";
            case "java": return "TimerJava";
            default: return null;
        }
    }

    public static Property getPropInThing(Thing thing, String property){
        for (Property p:thing.getProperties()) {
            if(p.getName().equals(property)) return p;
        }
        return null;
    }

    public void debugPrints(ThingMLModel model){
        System.out.println("configuration: " + model.getConfigs().get(0).getName());
        for(AbstractConnector c : model.getConfigs().get(0).getConnectors()){
            Connector cc = (Connector) c;
            Instance cli = cc.getCli(), srv = cc.getSrv();
            Port req = cc.getRequired(), prov = cc.getProvided();
            System.out.print("index:" + model.getConfigs().get(0).getConnectors().indexOf(cc) + ": ");
            System.out.println(
                (cli!=null?"[" + cli.getType().getName() + ":" + cli.getName() + "]":"null")
                + "." +
                (req != null? req.getName():"null")
                + " => " +
                (srv!=null?"[" + srv.getType().getName() + ":" + srv.getName() + "]":"null")
                + "." +
                (prov!=null?prov.getName():"null")
            );
        }
        System.out.println("------------");

        /*Thing t = (Thing) model.getTypes().get(30);
        ActionBlock ab = (ActionBlock) t.getBehaviour().getSubstate().get(0).getInternal().get(0).getAction();
        ConditionalAction ca = (ConditionalAction) ((ActionBlock)((ForAction)ab.getActions().get(2)).getAction()).getActions().get(0);
        ActionBlock ab2 = (ActionBlock) ((ConditionalAction)((ActionBlock)ca.getAction()).getActions().get(0)).getAction();
        SendAction sa = (SendAction) ab2.getActions().get(1);
        System.out.println(sa.getPort().getName());
        System.out.println(sa.getMessage().getName());
        System.out.println(sa.getParameters());

        System.out.println("------------");*/
    }
}

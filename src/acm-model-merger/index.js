﻿let ACMMetamodel = require("acm-metamodel");
let  PhysicalProcess = ACMMetamodel.PhysicalProcess;
let Action = ACMMetamodel.Action;
let SoftwareComponent = ACMMetamodel.SoftwareComponent;


var exports = module.exports = {};
exports.mergeModels = mergeModels;
exports.unmergeModels = unmergeModels;

function mergeModels(models, envModel, callback) {
    try {
        let physicalJSON = typeof envModel === "string" ? JSON.parse(envModel) : envModel;
        let physicalProcesses = [];

        for (let i = 0; i < physicalJSON.physical_processes.length; i++) {
            physicalProcesses.push(new PhysicalProcess(physicalJSON.physical_processes[i].id, physicalJSON.physical_processes[i].name, physicalJSON.physical_processes[i].x, physicalJSON.physical_processes[i].y, physicalJSON.physical_processes[i].location, physicalJSON.physical_processes[i].effect));
        }
        for (i = 0; i < physicalJSON.links.length; i++) {
            for (let j = 0; j < models.components.length; j++) {
                if (models.components[j].id === physicalJSON.links[i].from_id) {
                    for (let k = 0; k < physicalProcesses.length; k++) {
                        if (physicalProcesses[k].id === physicalJSON.links[i].to_id) {
                            // there is a physical process for the component, create an Action
                            models.components[j] = new Action(models.components[j].id, models.components[j].name, models.components[j].x, models.components[j].y, models.components[j].type, models.components[j].id_parent);
                            models.components[j].physicalProcess.push(physicalProcesses[k]);
                        }
                    }
                }
            }
        }
        models.physicalProcess = physicalProcesses;
        callback(models);
    } catch (e) {
        console.error("ERROR merging models");
        console.error(e);
        callback(models);
    }
}

function unmergeModels(model, callback) {
    model.physicalProcess = [];
    model.components = model.components.map(cmp => {
        if (cmp instanceof Action) {
            return new SoftwareComponent(cmp.id, cmp.name, cmp.x, cmp.y, cmp.type, cmp.id_parent);
        } else {
            return cmp;
        }
    });
    callback(model);
}


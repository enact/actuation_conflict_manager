const xml2js = require('xml2js');
const fs = require('fs');
const parser = new xml2js.Parser({ $key: "$" });
var peg = require("pegjs");
const _ = require("underscore");
var extend = require('util')._extend;
const path = require("path");
const os = require("os");

var exports = module.exports = {};
exports.verifyProperties = verifyProperties;
exports.verifyECA = verifyECA;
exports.verifySyntax = verifySyntax;
exports.createComponent = createComponent;

function verifySyntax(ecaString){
    var grammar = fs.readFileSync("../acm-custom-design/grammar/eca.pegjs", "utf8")
    var parser = peg.generate(grammar);
    try{
        var ecaFile = parser.parse(ecaString.ecaRules);
    }catch(error){
        return {"error":{"message":error.message,"description":JSON.stringify(error),"type":"syntax"}}
    }
    return ecaFile;
}

function verifyECA(ecaJson){
    /*
        error handle: 
        action has not been declared -> default to do,
        in trigger policy : input has not been declared correctly
        in predicate declaration : input has not been declared correctly 
    */
    var fsm = ecaJson.fsm
    var predicates = ecaJson.predicateConfiguration
    var actionDefinition = ecaJson.actionDefinition
    var limits = ecaJson.environmentConfiguration
    var error = null

    for (var i = 0; i < fsm.eca.length; i++) {
        var used = false
        for (var j = 0; j < predicates.length; j++) {
            if(predicates[j].predicate == fsm.eca[i].event)
                used = true;
        }
        if(!used){
            console.log("error: predicate "+fsm.eca[i].event+" is never used in rules")
            error = {"error":{"message":"ERROR","description":"predicate "+fsm.eca[i].event+" is never used in rules or is not specified","type":"semantic"}}
        }

        if(fsm.eca[i].actions.length != 0){
            used = false
            for (var j = 0; j < actionDefinition.length; j++) {
                if(fsm.eca[i].actions.includes(actionDefinition[j].actionName ))
                    used = true
            }
            if(!used){
                console.log("error: action "+ fsm.eca[i].actions+" is never used in rules")
                error = {"error":{"message":"ERROR","description":"action "+ fsm.eca[i].actions+" is not specified","type":"semantic"}}
            }   
        }
    }

    used = false
    for (var j = 0; j < actionDefinition.length; j++) {
        if(fsm.default.actions.includes(actionDefinition[j].actionName ))
            used = true
    }
    if(!used){
        console.log("error: action "+fsm.default.actions+" is never used in rules")
        error = {"error":{"message":"ERROR","description":"default action "+fsm.default.actions+" is not specified","type":"semantic"}}
    }

    return error
    //-warning sur les predicats non utilisé
    //-warning sur les actions non utilisés
    //-vérifier la cohérence entre les types.
    //-1 et unique affectation par output quoi qu’il arrive 
}

function createFSM(ecaFile){
    var fsm = ecaFile.fsm
    var eca = ecaFile.fsm.eca
    var predicates = ecaFile.predicateConfiguration
    var actionDefinition = ecaFile.actionDefinition
    var properties = ecaFile.properties
    var limits = ecaFile.environmentConfiguration

    var states = {};    
    var events = []
    //events from predicates
    for (var i = 0; i < predicates.length; i++) {
        events.push(predicates[i].predicate)
    }
    //states from eca
    for (var i = 0; i < eca.length; i++) {
        for (var j = 0; j < eca[i].transition.length; j++) {
            if(!states[eca[i].transition[j].variable]){
                states[eca[i].transition[j].variable] = [eca[i].transition[j]]
            }else{
                var stateExisting = false
                for (var k = 0; k < states[eca[i].transition[j].variable].length; k++) {
                    if(states[eca[i].transition[j].variable][k].value === eca[i].transition[j].value){
                        stateExisting = true
                    }
                }
                if(!stateExisting){
                    states[eca[i].transition[j].variable].push(eca[i].transition[j])
                }
            }
        }
    }
    if(JSON.stringify(states)==="{}"){
        states = {"default" : [{variable : "default",value:"init"}]}
    }

    //creation of the FSM
    states_product = cartesianProductOf(states)

    var ACM={init:fsm.inits,states:[]}
    if(fsm.inits){
        ACM.init = getStateName(fsm.inits)
    }else{
        ACM.init = getStateName(states_product[0])
    }
    for (var i = 0; i < states_product.length; i++) {
        var current_state = {name:getStateName(states_product[i]),transitions:[]}
        for (var j = 0; j < events.length; j++) {
            var transition = {event:events[j]}
            var destination = states_product[i]
            var actions = [] 
            for (var k = 0; k < eca.length; k++) {
                if(eca[k].event==events[j] && evalCondition(eca[k].condition,states_product[i])){
                    destination = getNewState(destination,eca[k].transition)                    
                    if(!transition.outputs){
                        transition.outputs = eca[k].actions
                    }else{
                        transition.outputs = transition.outputs.concat(eca[k].actions)
                    }
                }
            }
            transition.destination = getStateName(destination)
            
            current_state.transitions.push(transition)
        }
        ACM.states.push(current_state)
    }
    return ACM

}

function verifyProperties(ecaFile) {
    var fsm = ecaFile.fsm
    var eca = ecaFile.fsm.eca
    var predicates = ecaFile.predicateConfiguration
    var actionDefinition = ecaFile.actionDefinition
    var properties = ecaFile.properties
    var limits = ecaFile.environmentConfiguration

    var ACM = createFSM(ecaFile)

    var outputs = []
    var default_outputs = []
    for (var i = 0; i < actionDefinition.length; i++) {
        var actionName = actionDefinition[i].actionName 
        for (var j = 0; j < actionDefinition[i].assignement.length; j++) {
            var current_action = {}
            for (var k = 0; k < eca.length; k++) {
                if(eca[k].actions.includes(actionName)){
                    current_action.predicate = eca[k].event
                    current_action.condition = eca[k].condition
                    current_action.action = actionDefinition[i].assignement[j].value
                }
            }
            if(JSON.stringify(current_action)  != "{}"){
                if(!outputs[actionDefinition[i].assignement[j].output.outputPort]){
                    outputs[actionDefinition[i].assignement[j].output.outputPort] = [current_action]
                }else{
                    outputs[actionDefinition[i].assignement[j].output.outputPort].push(current_action)
                }
            }
            

            if(fsm.default.actions.includes(actionName)){

                default_outputs[actionDefinition[i].assignement[j].output.outputPort] = actionDefinition[i].assignement[j].value
            }
        }
    }


    /*************************** REAL GENERATION ***************************/

    var smvText = "MODULE main \n"
    smvText += "VAR \n"
    for (var i = 0; i < limits.length; i++) {
        if(limits[i].min!=undefined){
            smvText += getInputNameNuSMV(limits[i]) + " : " +limits[i].min+" .. "+limits[i].max+";\n"
        }else if(limits[i].setOfInput){
            smvText+= getInputNameNuSMV(limits[i]) + ": {"
            for (var j = 0; j < limits[i].setOfInput.length; j++) {
                if(j!=0)
                    smvText+=" , "
                smvText+= limits[i].setOfInput[j]
            }
            smvText+= "};\n"
        }
    }
    if(ACM.states.length==1){
        smvText += "ACM : {other, "+ACM.states[0].name+"};\n"    
    }else{
        smvText += "ACM : {"
        for (var i = 0; i < ACM.states.length; i++) {
            if(i!=0){
                smvText+=","
            }
                smvText+= ACM.states[i].name
        }
        smvText += "};\n"
    }
     

    smvText += "DEFINE\n"
    for (var i = 0; i < predicates.length; i++) {
        smvText += predicates[i].predicate + " := " + getConditionNuSMV(predicates[i].condition)  +";\n"
    }

    //handle properties with magic in case of no trigger

    for (var i = 0; i < properties.length; i++) {
        var prop = ""
            prop += getPropertiesNuSMV(properties[i].property)

        smvText += "property_"+i+" := "+prop+";\n"
    }

    //output
    var NuSMVOutputDefinition = []
    for (var i = 0; i < ACM.states.length; i++) {
        for (var j = 0; j < ACM.states[i].transitions.length; j++) {
            //add complete string to tabs
            if(ACM.states[i].transitions[j].outputs){
                for (var k = 0; k < ACM.states[i].transitions[j].outputs.length; k++) {
                    for (var l = 0; l < actionDefinition.length; l++) {
                        //console.log("actionDefinition[l]")
                        //console.log(actionDefinition[l])
                        if(actionDefinition[l].actionName==ACM.states[i].transitions[j].outputs[k]){
                            for (var m = 0; m < actionDefinition[l].assignement.length; m++) {
                                if(NuSMVOutputDefinition[actionDefinition[l].assignement[m].output.outputPort]==null){
                                    NuSMVOutputDefinition[actionDefinition[l].assignement[m].output.outputPort] = [ACM.states[i].transitions[j].event + " & ACM = " +ACM.states[i].name +" : "+ InterpretOutputValueNuSMV(actionDefinition[l].assignement[m].value) +" ;\n"]
                                }
                                else{
                                    NuSMVOutputDefinition[actionDefinition[l].assignement[m].output.outputPort] += ACM.states[i].transitions[j].event + " & ACM = " +ACM.states[i].name +" : "+ InterpretOutputValueNuSMV(actionDefinition[l].assignement[m].value) +" ;\n"
                                }

                            }
                        }
                    }
                }
            }
    
        }
    }
    for (var i = 0; i < NuSMVOutputDefinition.length; i++) {
        smvText += "output_"+i+" :=\n"
        smvText += "case\n"
        smvText+= NuSMVOutputDefinition[i]
        smvText += "TRUE : "
        smvText += InterpretOutputValueNuSMV(default_outputs[i])
        smvText+=" ;\n"

        smvText += "esac;\n"   
    }

    smvText += "ASSIGN\n"
        
    smvText += "init(ACM) := "+ACM.init+";\n"
    smvText += "next(ACM) := \n"
    smvText += "case\n" 
    for (var i = 0; i < ACM.states.length; i++) {
        for (var j = 0; j < ACM.states[i].transitions.length; j++) {
            smvText+= ACM.states[i].transitions[j].event + " & ACM = " +ACM.states[i].name +" : "+ ACM.states[i].transitions[j].destination +" ;\n"
        }
    }
    smvText += " TRUE: ACM;\n"
    smvText += "esac;\n" 


    for (var i = 0; i < properties.length; i++) {
        smvText += "SPEC AG !(property_"+i+")\n"
    }
    //console.log(smvText)


    let filename = os.tmpdir() + path.sep + "acm.smv";
    fs.writeFileSync(filename, smvText, function (err, data) {
        if (err) console.log(err);
        console.log("successfully written our update SMV to file");
    });


    const execSync = require('child_process').execSync;
    console.log("nusmv cmdline::" + __dirname + "/NuSMV/"+ process.platform + path.sep + process.arch +"/bin/NuSMV " + filename);
    code = execSync(__dirname + "/NuSMV/" + process.platform + path.sep + process.arch + "/bin/NuSMV " + filename);
    console.log("NuSMV report")
    console.log(bin2string(code))
    return bin2string(code)
}



function verifyPropertiesOptimizedChecking(ecaFile) {
    var fsm = ecaFile.fsm
    var predicates = ecaFile.predicateConfiguration
    var actionDefinition = ecaFile.actionDefinition
    var properties = ecaFile.properties
    var limits = ecaFile.environmentConfiguration

    var ACM = createFSM(ecaFile)

    var outputs = []
    var default_outputs = []
    for (var i = 0; i < actionDefinition.length; i++) {
        var actionName = actionDefinition[i].actionName 
        for (var j = 0; j < actionDefinition[i].assignement.length; j++) {
            var current_action = {}
            for (var k = 0; k < eca.length; k++) {
                if(eca[k].actions.includes(actionName)){
                    current_action.predicate = eca[k].event
                    current_action.condition = eca[k].condition
                    current_action.action = actionDefinition[i].assignement[j].value
                }
            }
            if(JSON.stringify(current_action)  != "{}"){
                if(!outputs[actionDefinition[i].assignement[j].output.outputPort]){
                    outputs[actionDefinition[i].assignement[j].output.outputPort] = [current_action]
                }else{
                    outputs[actionDefinition[i].assignement[j].output.outputPort].push(current_action)
                }
            }
            

            if(fsm.default.actions.includes(actionName)){
                default_outputs[actionDefinition[i].assignement[j].output.outputPort] = actionDefinition[i].assignement[j].value
            }
        }
    }


    /*************************** REAL GENERATION ***************************/

    var smvText = "MODULE main \n"
    smvText += "VAR \n"
    for (var i = 0; i < limits.length; i++) {
        if(limits[i].min!=undefined){
            smvText += getInputNameNuSMV(limits[i]) + " : " +limits[i].min+" .. "+limits[i].max+";\n"
        }else if(limits[i].setOfInput){
            smvText+= getInputNameNuSMV(limits[i]) + ": {"
            for (var j = 0; j < limits[i].setOfInput.length; j++) {
                if(j!=0)
                    smvText+=" , "
                smvText+= limits[i].setOfInput[j]
            }
            smvText+= "};\n"
        }
    }
    if(ACM.states.length==1){
        smvText += "ACM : {other, "+ACM.states[0].name+"};\n"    
    }else{
        smvText += "ACM : {"
        for (var i = 0; i < ACM.states.length; i++) {
            if(i!=0){
                smvText+=","
            }
                smvText+= ACM.states[i].name
        }
        smvText += "};\n"
    }
     

    smvText += "DEFINE\n"
    for (var i = 0; i < predicates.length; i++) {
        smvText += predicates[i].predicate + " := " + getConditionNuSMV(predicates[i].condition)  +";\n"
    }

    //handle properties with magic in case of no trigger

    for (var i = 0; i < properties.length; i++) {
        var prop = ""
            prop += getPropertiesNuSMV(properties[i].property)

        smvText += "property_"+i+" := "+prop+";\n"
    }

    //output
    var NuSMVOutputDefinition = []
    for (var i = 0; i < ACM.states.length; i++) {
        for (var j = 0; j < ACM.states[i].transitions.length; j++) {
            //add la string complète dans un tableau
            if(ACM.states[i].transitions[j].outputs){
                for (var k = 0; k < ACM.states[i].transitions[j].outputs.length; k++) {
                    for (var l = 0; l < actionDefinition.length; l++) {
                        if(actionDefinition[l].actionName==ACM.states[i].transitions[j].outputs[k]){
                            for (var m = 0; m < actionDefinition[l].assignement.length; m++) {
                                if(NuSMVOutputDefinition[actionDefinition[l].assignement[m].output.outputPort]==null){
                                    NuSMVOutputDefinition[actionDefinition[l].assignement[m].output.outputPort] = [ACM.states[i].transitions[j].event + " & ACM = " +ACM.states[i].name +" : "+ InterpretOutputValueNuSMV(actionDefinition[l].assignement[m].value) +" ;\n"]
                                }
                                else{
                                    NuSMVOutputDefinition[actionDefinition[l].assignement[m].output.outputPort] += ACM.states[i].transitions[j].event + " & ACM = " +ACM.states[i].name +" : "+ InterpretOutputValueNuSMV(actionDefinition[l].assignement[m].value) +" ;\n"
                                }

                            }
                        }
                    }
                }
            }
    
        }
    }
    for (var i = 0; i < NuSMVOutputDefinition.length; i++) {
        smvText += "output_"+i+" :=\n"
        smvText += "case\n"
        smvText+= NuSMVOutputDefinition[i]
        smvText += "TRUE : "
        smvText += InterpretOutputValueNuSMV(default_outputs[i])
        smvText+=" ;\n"

        smvText += "esac;\n"   
    }

    smvText += "ASSIGN\n"
        
    smvText += "init(ACM) := "+ACM.init+";\n"
    smvText += "next(ACM) := \n"
    smvText += "case\n" 
    for (var i = 0; i < ACM.states.length; i++) {
        for (var j = 0; j < ACM.states[i].transitions.length; j++) {
            smvText+= ACM.states[i].transitions[j].event + " & ACM = " +ACM.states[i].name +" : "+ ACM.states[i].transitions[j].destination +" ;\n"
        }
    }
    smvText += " TRUE: ACM;\n"
    smvText += "esac;\n" 


    for (var i = 0; i < properties.length; i++) {
        smvText += "SPEC AG !(property_"+i+")\n"
    }
    //console.log(smvText)


    let filename = os.tmpdir() + path.sep + "acm.smv";
    fs.writeFileSync(filename, smvText, function (err, data) {
        if (err) console.log(err);
        console.log("successfully written our update SMV to file");
    });


    const execSync = require('child_process').execSync;
    console.log("nusmv cmdline::" + __dirname + "/NuSMV/"+ process.platform + path.sep + process.arch +"/bin/NuSMV " + filename);
    code = execSync(__dirname + "/NuSMV/" + process.platform + path.sep + process.arch + "/bin/NuSMV " + filename);
    console.log("NuSMV report")
    console.log(bin2string(code))
    return bin2string(code)
}

function createComponent(ecaFile){
    
    var synchro = ecaFile.synchro 
    var fsm = ecaFile.fsm
    var predicates = ecaFile.predicateConfiguration
    var actionDefinition = ecaFile.actionDefinition
    var properties = ecaFile.properties
    var limits = ecaFile.environmentConfiguration
    //console.log(getConditionJS(synchro.condition))

    var ACM = createFSM(ecaFile)
    //console.log(getPredicatesForJS(predicates))
    console.log("SCXML")
    console.log(getSCXML(ACM))

    //console.log(getOutputs(actionDefinition))
    return {
        synchro:{condition:getConditionJS(synchro.strategy.condition),delay:synchro.strategy.delay},
        actions:getOutputs(actionDefinition),
        predicates:getPredicatesForJS(predicates),
        scxml:getSCXML(ACM)
    }
}

function getStateName(state){
    var text = ""
    for (var i = 0; i < state.length; i++) {
        if(i!=0)
            text += "-"
        text += state[i].variable + "_"+ state[i].value 
    }
    if(text==""){
        return "default_init"
    }
    return text
}

function getPredicatesForJS(predicates){
    var result = {}
    for (var i = 0; i < predicates.length; i++) {
        predicates[i]
        result[predicates[i].predicate] = getConditionJS(predicates[i].condition)
    }
    return result
}

function getOutputNameNuSMV(output){
    return "output_"+output.outputPort
}

function getInputNameNuSMV(input){
    return "input_"+input.inputPort+"_"+input.index
}

function getIONameNuSMV(IO){
    var result
    if(IO.inputPort!=undefined){
        result = getInputNameNuSMV(IO)
    }else if(IO.outputPort!=undefined){
        result = getOutputNameNuSMV(IO)
    }else{
        result = IO
    }
    //console.log(result)
    return result
}

function getOutputNameJS(output){
    return "output["+output.inputPort+"]["+output.index+"]"
}

function getInputNameJS(input){
    return "input["+input.inputPort+"]["+input.index+"]"
}

function getIONameJS(IO){
    if(IO.inputPort!=undefined){
        return getInputNameJS(IO)
    }else if(IO.outputPort!=undefined){
        return getOutputNameJS(IO)
    }else{
        return IO
    }
}



function getConditionNuSMV(cond){
    if(!cond){
        return cond 
    }
    if(cond.input){
        return getIONameNuSMV(cond.input)  + getNuSMVOperator(cond.operation) + getIONameNuSMV(cond.value)
    }
    else if(cond.operation=="()"){
        return "("+getConditionNuSMV(cond.expr)+")" 
    }
    else{
        return getConditionNuSMV(cond.left) + getNuSMVOperator(cond.operation) + getConditionNuSMV(cond.right) 
    }
    
}

function getPropertiesNuSMV(cond){
    if(!cond){
        return
    }
    if(!cond.operation){
        return getIONameNuSMV(cond)
    }
    else if(cond.operation=="()"){
        return "("+getPropertiesNuSMV(cond.expr)+")" 
    }
    else{
        return getPropertiesNuSMV(cond.left) + getNuSMVOperator(cond.operation) + getPropertiesNuSMV(cond.right) 
    }
}



function getConditionJS(cond){
    let str = ""
    if(!cond){
        return 
    }
    if(cond.input){
        if(typeof cond.value == "string" && (cond.value!="null"))
            return getInputNameJS(cond.input)  + cond.operation + "\""+cond.value+"\""
        return getInputNameJS(cond.input)  + cond.operation + cond.value

    }
    if(cond.operation=="()"){
        return "("+getConditionJS(cond.expr)+")" 
    }else{
        return getConditionJS(cond.left) + cond.operation + getConditionJS(cond.right) 
    }
}

function getNuSMVOperator(op){
    var res = op;
    switch(op){
        case "&&":
            res = "&"
            break;
        case "||":
            res = "|"
            break;
        case "==":
            res = "="
            break;
    }
    return res

}

function getNewState(dest,trans){
    var new_destination = extend([] ,dest)

    for (var i = 0; i < trans.length; i++) {
        for (var j = 0; j < new_destination.length; j++) {
            if(trans[i].variable == new_destination[j].variable)
                new_destination[j] = trans[i]
         } 
    }
    return new_destination
}


function InterpretOutputValueNuSMV(value){
    //console.log(value)
    var str = ""
    if(value.operation!=undefined){
        return InterpretOutputValueNuSMV(value.args[0])+value.operation+InterpretOutputValueNuSMV(value.args[1])
    }
    if(value.inputPort!=undefined){
        return getInputNameNuSMV(value)
    }
    return value
}

function InterpretOutputValueJS(value){
    //console.log(value)
    var str = ""
    if(value.operation!=undefined){
        return InterpretOutputValueJS(value.args[0])+value.operation+InterpretOutputValueJS(value.args[1])
    }
    if(value.inputPort!=undefined){
        return getInputNameJS(value)
    }
    return value
}


function getSCXML(fsm){
    var builder = require('xmlbuilder');

    var root = builder.create('scxml').att("xmlns","http://www.w3.org/2005/07/scxml").att("version","1.0");
    var acm = root.ele("state").att("id","ACM").att("initial",fsm.init)

    for (var i = 0; i < fsm.states.length; i++) {
        var new_state = acm.ele("state").att("id",fsm.states[i].name)
        for (var j = 0; j < fsm.states[i].transitions.length; j++) {
            var transition = new_state.ele("transition").att("event",fsm.states[i].transitions[j].event).att("target",fsm.states[i].transitions[j].destination)
            if(fsm.states[i].transitions[j].outputs){
                for (var k = 0; k < fsm.states[i].transitions[j].outputs.length; k++) {
                    var send_output = transition.ele("send").att("event",fsm.states[i].transitions[j].outputs[k]);
                }
            }
        }
    }


//var xml = root.end({ pretty: true});
var xml = root.end();

return xml

/*
fs.writeFile("SCXML/"+eca_name+".scxml", xml, function(err, data) {
    if (err) console.log(err);
    console.log("successfully written our update xml to file");
});
*/
}

function getOutputs(actions){
    var outputs = {}
    for (var i = 0; i < actions.length; i++) {
        var name = actions[i].actionName
        outputs[name] = {}
        for (var j = 0; j < actions[i].assignement.length; j++) {
            //console.log(actions[i].assignement[j])
            actions[i].assignement[j].output.outputPort
            var outputValue = InterpretOutputValueJS(actions[i].assignement[j].value)
            //console.log(outputValue)
            outputs[name][actions[i].assignement[j].output.outputPort] = outputValue
        }
    }
    return outputs
}

function getConditionForFSMGeneration(cond){
    let str = ""
    if(cond == "true")
        return true
    if(!cond){
        return 
    }
    if(cond.variable){
        if(typeof cond.variable == "string")
            return "stateReworked[\""+cond.variable+"\"]"  + cond.operator + "\""+cond.value+"\""
    }
    if(cond.operation=="()"){
        return "("+getConditionForFSMGeneration(cond.expr)+")" 
    }else{
        return getConditionForFSMGeneration(cond.left) + cond.operation + getConditionForFSMGeneration(cond.right) 
    }
}

function evalCondition(condition,state){
    var stateReworked = {}
    for (var i = 0; i < state.length; i++) {
        stateReworked[state[i].variable] = state[i].value  
    }

    return eval(getConditionForFSMGeneration(condition))

}

function cartesianProductOf(states) {
    var results = [[]];
    var keys = Object.keys(states);
    for (var i = 0; i < keys.length; i++) {
        var currentSubArray = states[keys[i]];
        var temp = [];
        for (var j = 0; j < results.length; j++) {
            for (var k = 0; k < currentSubArray.length; k++) {
                temp.push(results[j].concat(currentSubArray[k]));
            }
        }
        results = temp;
    }
    return results;
}

function parseSync(xml) {

    var error = null;
    var json = null;
    xml2js.parseString(xml, function (innerError, innerJson) {

        error = innerError;
        json = innerJson;
    });

    if (error) {

        throw error;
    }

    if (!error && !json) {

        throw new Error('The callback was suddenly async or something.');
    }

    return json;
}

function bin2string(array) {
    var result = "";
    for (var i = 0; i < array.length; ++i) {
        result += (String.fromCharCode(array[i]));
    }
    return result;
}
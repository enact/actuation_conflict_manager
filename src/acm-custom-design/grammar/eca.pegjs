
Main = synchroPredicate:PredicateConfiguration* synchro:SynchroConfiguration _ environmentConfiguration:EnvironmentConfiguration* _ predicateConfiguration:PredicateConfiguration* _ fsm:stateMachine _ actionDefinition:ActionDefinition* _ DesynchroConfiguration* _ properties:Properties* temporalProperties:TemporalProperties _{
    //return {"actionDefinition":fsm}
    return {
            "synchro":{"strategy":synchro,"synchroPredicate":synchroPredicate},
            "predicateConfiguration":predicateConfiguration,
            "fsm":fsm,
            "actionDefinition":actionDefinition,
            "properties":properties,
            "environmentConfiguration":environmentConfiguration,
            "temporalProperties":temporalProperties
    }
}

SynchroConfiguration =  condition:SynchroCondition? _ delay:SynchroDelay? _ "TRIGGER" _{
    return {"condition":condition,"delay": delay}
} 

SynchroCondition = "IF" _ cond:(predicateCondition/predicate) _ "THEN"{
    return cond
}

SynchroDelay = "delay(" _ delay:Integer _ ");" _ {
    return delay
}

PredicateConfiguration = _ predicate:predicate _ "=" _ condition:predicateCondition _{
    return {"predicate":predicate,"condition":condition}
}

predicateCondition = left:InputCondition _ '||' _ right:predicateCondition {return {"left":left,"operation":"||","right":right}}
            / left:InputCondition _ '&&' _ right:predicateCondition {return {"left":left,"operation":"&&","right":right}}
            // '(' condition:predicateCondition ')' {return {"expr":condition,"operation":"()"}}
            / condition:InputCondition {return condition}
            

InputCondition = condition:(IntegerInput/ DoubleInput/ StringInput){
    return condition        
}
/ '(' condition:predicateCondition ')' {return {"expr":condition,"operation":"()"}}
        
IntegerInput = input:Input _ element:("<="/">="/"=="/"!="/"<"/">") _ int:Integer _  {
    return {"input":input,"operation":element,"value":int}
    //return {"condition": "input_"+input.inputPort+"_"+input.index + " " + element+" "+ int}
} 

DoubleInput = input:Input _ element:("=="/"!="/"<"/">"/"<="/">=") _ double:Double _{
    return {"input":input,"operation":element,"value":double}
    //return {"condition": "input_"+input.inputPort+"_"+input.index +element+double}
}
        
StringInput = input:Input _ element:("=="/"!=") _ word:Word _ {
    return {"input":input,"operation":element,"value":word}
    //return {"condition": "input_"+input.inputPort+"_"+input.index +element+word}
}

stateMachine = inits:InitFSM* _ eca:ECARules* _ defaultMode:DefaultRule? _{
    return {"eca":eca,"inits":inits,"default":defaultMode}
}

InitFSM = init:VariableAssignement _{
    return init
}
//
ECARules = _ "ON" _ event:predicate _ "IF" _ cond:VariableCondition _ "DO" _ transition:VariableAssignement* _ action:predicate* _ ";" _{
    var transition = {"event":event,"condition":cond,"transition":transition,"actions":action}
    return transition
}

DefaultRule = "DEFAULT" _ "DO" _ action:predicate* _ ";"{
    var transition = {"event":"default","actions":action}
    return transition
}

VariableAssignement = variable:variable _ "=" _ value:Word _{
    return {"variable":variable,"value":value}
}

VariableCondition = left:VariableConditionMember _ '||' _ right:VariableCondition {return {"left":left,"operation":"||","right":right}}
            / left:VariableConditionMember _ '&&' _ right:VariableCondition {return {"left":left,"operation":"&&","right":right}}
            // '(' condition:VariableCondition ')' {return {"expr":condition,"operation":"()"}}
            / condition:VariableConditionMember {return condition}

VariableConditionMember =  variable:variable _ element:("=="/"!=") _ value:Word {return {"variable":variable,"operator":element,"value":value}}
                    / _"True" _ {return "true"}
                    / '(' condition:VariableCondition ')' {return {"expr":condition,"operation":"()"}}


ActionDefinition = _ actionName:predicate _ ":" first:OutputAssignement _ next:ActionDefinitionSuite*{
        var assignement = [first];
        for (var i = 0; i< next.length ;i++){
            assignement.push(next[i])
        }
        return {"actionName":actionName,"assignement":assignement}
}
ActionDefinitionSuite = _ "," item:OutputAssignement _  {
    return item
}

OutputAssignement = _ output:Output _ "=" _ value:(ActionFunction/ActionOperation/ Input){
    return {"output":output,"value":value}
}

ActionOperation = _ left:Input _ operation:("+"/"-"/"*"/"/") _ right:ActionOperation _ {return {"operation":operation,"args":[left,right]}}
                    / _ input:Input{return input}
                    / _ constant:(Integer/Word/Double){return constant}

ActionFunction = f:predicate _ "(" _ first:Input _ next:ActionSuite* _ ")"_{
    var args = [first]
    for(var i = 0; i<next.length; i++){
        args.push(next[i])
    }
    return {"function":f,"args":args}
}
ActionSuite = "," _ input:Input  _{
        return input    
}



DesynchroConfiguration = "toto"

Properties = "ASSERT" _ "!(" _ condition:PropertyCondition _")"_{
       var property = {}
       return {"time":0,"property":condition}
}

PropertyCondition = left:PropertyConditionMember _ '||' _ right:PropertyCondition {return {"left":left,"operation":"||","right":right}}
            / left:PropertyConditionMember _ '&&' _ right:PropertyCondition {return {"left":left,"operation":"&&","right":right}}
            / '(' condition:PropertyCondition ')' {return {"expr":condition,"operation":"()"}}
            / condition:PropertyConditionMember {return condition}

PropertyConditionMember= left:(Input/Output) _ element:("=="/"!="/"<"/">"/"<="/">=") _ right:(Integer/Word) _{
    if(element=="=="){
        element = "="
    }
    return  {"operation":element,"left":left,"right":right,"args":[left,right]}
}
/ '(' condition:PropertyCondition ')' {return {"expr":condition,"operation":"()"}}
    

EnvironmentConfiguration = input:Input _ "=" _ config:(EnvironmentIntConfiguration/EnvironmentStringConfiguration) _ {
    if(config.min!=undefined){
        return {"inputPort":input.inputPort,"index":input.index, "min":config.min,"max":config.max}
    }else{
        return {"inputPort":input.inputPort,"index":input.index, "setOfInput":config}
    }
}

EnvironmentIntConfiguration =  "[" _ min:Integer _ "," _ max:Integer _ "]" _{
    return {"min":min,"max":max}
}

EnvironmentStringConfiguration =  "{" _ strings:Word* _ "}" _{
    return strings
}

TemporalProperties = _ "LivenessInTime" _"("_ minNumberOfEvent:Integer _"," NumberSimulationStep:Integer _")"
                    _ "NoLostEvent" _ "()"{
    return {"minNumberOfEvent":minNumberOfEvent,"NumberSimulationStep":NumberSimulationStep}                    
}

////////////////////////////////////////////////////////////////////
Output =  "Output("_ outputPort:Word _ "," _ index:Integer _ ")"{
    return  {"outputPort":outputPort,"index":index}
} / "Output("_ outputPort:Word  _ ")"{return {"outputPort":outputPort} }

Input =  "Input("_ inputPort:Word _ "," _ index:Integer _ ")"{
    return  {"inputPort":inputPort,"index":index}
} / "Input("_ inputPort:Word  _ ")"{return {"inputPort":inputPort} }

Word = '\"' l:Letter+ '\"' _
 { return l.join(""); }

NULL = 'null'{
    return "null"
}

predicate = l:Letter+ _
 { return l.join(""); } 

variable = l:Letter+
 { return l.join(""); } 

Digit = [0-9]

Letter
 = [a-zA-Z_.-]

Double = [0-9]+.[0-9]+ / NULL

Integer "integer" 
  = _ [0-9]+ { return parseInt(text(), 10); } / NULL
  
ws "Whitespace"
  = [ \t\n\r]

_ "One or more whitespaces"
 = ws*

/*
IF Input("TV",0)!=null || Input("SmartPhone",0)!=null || Input("Cleaning",0)!=null THEN TRIGGER

Input("Cleaning",0) = {"Start" "Stop" "Resume" "Pause"}
Input("SmartPhone",0) = {"ON" "OFF"}
Input("TV",0) = {"ON" "OFF"}

PhoneMute = Input("SmartPhone",0) == "ON"
PhoneUnMute = Input("SmartPhone",0) == "OFF"
TVMute = Input("TV",0) == "ON"
TVUnMute = Input("TV",0) == "OFF"

cleanerClean = Input("Cleaning",0) == "Start"
cleanerPause = Input("Cleaning",0) == "Pause"
cleanerResume = Input("Cleaning",0) == "Resume"
cleanerStop = Input("Cleaning",0) == "Stop"

cleaner="off"
phone="unmute"
tv="unmute"
ON cleanerClean IF tv=="mute" && phone=="unmute" DO cleaner="on" doClean;
ON cleanerStop IF cleaner!="off" DO cleaner = "off" stopClean;
ON cleanerPause IF cleaner == "on" || cleaner=="resume" DO cleaner = "pause" pauseClean;
ON cleanerResume IF cleaner == "pause" DO cleaner = "resume" resumeClean;


ON PhoneMute IF cleaner=="on" || cleaner=="resume" DO cleaner = "pause" pauseClean;
ON PhoneMute IF phone!="mute" DO phone="mute" muteWithPhone;
ON PhoneUnMute IF True DO phone="unmute";
ON PhoneUnMute IF tv=="unmute" DO phone="unmute" unmuteWithPhone;
ON PhoneUnMute IF cleaner=="pause" && tv=="mute"  DO cleaner="resume" resumeClean;


ON TVMute IF cleaner=="pause" DO cleaner="resume" resumeClean;
ON TVMute IF True DO tv="mute" muteWithTV;
ON TVUnMute IF cleaner=="on" || cleaner=="resume" DO cleaner="pause" pauseClean;
ON TVUnMute IF phone=="unmute" DO tv="unmute" unmuteWithTV;
ON TVUnMute IF True DO tv="unmute";
DEFAULT DO letThrough;

doClean : Output("IoTSpace-Cleaner",0) = "Start"
stopClean	 : Output("IoTSpace-Cleaner",0) = "Stop"
pauseClean : Output("IoTSpace-Cleaner",0) = "Pause"
resumeClean : Output("IoTSpace-Cleaner",0) = "Resume"

muteWithPhone : Output("IoTSpace-TV",0) = "ON" 
unmuteWithPhone : Output("IoTSpace-TV",0) = "OFF"

muteWithTV : Output("IoTSpace-TV",0) = "ON"
unmuteWithTV : Output("IoTSpace-TV",0) = "OFF"
letThrough:  Output("IoTSpace-TV",0) = "Stop",  Output("IoTSpace-TV",0) = "ON"


ASSERT!(Output("IoTSpace-TV",0) == "Start" &&  Output("IoTSpace-TV",0) == "ON" )


LivenessInTime(10,20)
NoLostEvent ()

*/
let acm_metamodel = require("acm-metamodel");
const url = require('url');

let CommComp = acm_metamodel.CommunicationComponent;
let Link = acm_metamodel.Link;

let findGenesisParentIP = require("./utils").findGenesisParentIP;

let links = {
    // map mqtt detection functions to known mqtt types
    register: function (registry, matchFunctions, generatorFunctions) {
        registry["link in"] = this.processLinkIn;
        registry["link out"] = this.processLinkOut;

        matchFunctions["NodeRedLink"] = this.identifyMatches;

        generatorFunctions["NodeRedLink"] = this.generateNode;
    },

    // identify matches on a full WIMAC model
    identifyMatches: function (model) {
        let linkNodes = model.components.filter(cmp => cmp instanceof CommComp && cmp.configuration.link);
        let inputLinkNodes = linkNodes.filter(cmp => cmp.configuration.input);
        // go through output links to create links to inputs
        linkNodes.filter(cmp => cmp.configuration.output).forEach(outLink => {
            outLink.configuration.links.forEach(inputid => {
                let input = inputLinkNodes.find(inNode => inNode.id === inputid);
                if (input) {
                    model.links.push(new Link("v_" + outLink.id + "_" + input.id, "v_" + outLink.id + "_" + input.id, 0, 0, outLink, input, 0, true));
                } else {
                    console.error("failed to find matching input link " + inputid + " for node " + outLink.id);
                }
            })
        });
    },

    processLinkIn: function (node, nrmodel) {
        return new CommComp(node.id, node.name, node.x, node.y, "link in", node.z, { link: true, generator: "NodeRedLink", input: true, links: node.links });
    },
    processLinkOut: function (node, nrmodel) {
        return new CommComp(node.id, node.name, node.x, node.y, "link out", node.z, { link: true, generator: "NodeRedLink", output: true, links: node.links });
    },

    // generate nr code from wimac comp
    generateNode: function (component) {
        // get base node
        let base = component.buildNodeRedJson();

        // add specific parameters
        base.links = component.configuration.links;

        //console.log(base);
        return base;
    }
}

module.exports = links;
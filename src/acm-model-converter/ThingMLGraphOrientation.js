const fs = require('fs');
const util =  require("util")

// readSingleFile
var ports = [];
var things = [];
var resIn = [];
var resOut = [];
var resBoth = [];
var id = 0;
var linkId = 0;

// readSingleFile2
var res = "";
var res2 = "";
var resWithConflicts = "";
var couple = [];
var nextsIn = [];
var nextsOut = [];
var nextsBoth = [];
var psm = [];
var psmCouple = [];
var compoWithType = [];
var realName = [];
var words = [];
var compoConflict = [];

// god help us all
function reset() {
    ports = [];
    things = [];
    resIn = [];
    resOut = [];
    resBoth = [];
    id = 0;
    linkId = 0;

    res = "";
    res2 = "";
    resWithConflicts = "";
    couple = [];
    nextsIn = [];
    nextsOut = [];
    nextsBoth = [];
    psm = [];
    psmCouple = [];
    compoWithType = [];
    realName = [];
    words = [];
    compoConflict = [];
}

function readSingleFile(thingmlPath) {
    let ct = fs.readFileSync(thingmlPath, "utf8", function (err, data) {
        if (err) throw err;
        //console.log(data);
    });

    let ct1 = String(ct);
    let words = ct1.split('\n').join(' ').split('{').join(' { ').split('}').join(' } ').split('\t').join('').split(' ');

    words = words.filter(w => w !== "");

    for (let i = 0; i < words.length; i++) {
        if (words[i] === 'port') {
            ports.push(words[i + 1]);
        }
        if (words[i] === 'thing' && words[i + 1] !== "fragment") {
            things.push(words[i + 1]);
        }
    }
    for (let i = 0; i < words.length; i++) {
        if (words[i] === "sends") {
            let portName = "";
            let tmpi = i;
            while (words[tmpi] !== "port") {
                --tmpi;
            }
            portName = words[tmpi + 1];

            while (words[i] !== "}") {
                if (words[i] === "receives") {
                    if (ports.includes(portName) && !resBoth.indexOf(portName) > -1) {
                        resBoth.push(portName);
                    }
                }
                ++i;
            }
            if (ports.includes(portName))
                resOut.push(portName);
        } else if (words[i] === "receives") {
            let portName = "";
            let tmpi = i;
            while (words[tmpi] !== "port") {
                --tmpi;
            }
            portName = words[tmpi + 1];
            
            while (words[i] !== "}") {
                if (words[i] === "sends") {
                    if (ports.includes(portName) && !resBoth.indexOf(portName) > -1) {
                        resBoth.push(portName);
                    }
                }
                ++i;
            }
            if (ports.includes(portName))
                resIn.push(portName);
        }
    }

    resIn = resIn.filter(e => !resBoth.includes(e));
    resOut = resOut.filter(e => !resBoth.includes(e));
}


function readSingleFile2(thingmlPath, plantUMLPath) {
    readSingleFile(thingmlPath);
    let compos = [];
    let compos2 = [];
    let port1 = [];
    let port2 = [];
    let ct = String(fs.readFileSync(plantUMLPath, "utf8", function (err, data) {
        if (err) throw err;
        //console.log(data);
    }));
    words = ct.split('[').join(' [ ').split(']').join(' ] ').split(':').join(' : ').split('\n').join(' ').split(' ');
    for (let i = 0; i < words.length; i++) {
        // don't know why they make a difference between PIM/PSM
        // they're both things just some are platform independent??
        /*if (words[i] === "<<PSM>>") {
            let j = i;
            let str = "";
            while (words[j] !== "[") {
                --j;
            }
            while (words[j + 1] !== "]") {
                ++j;
                str = str + ' ' + words[j];
            }
            str = str.split(']').join('');
            psm.push(str);
        }*/

        if (words[i] === "<<PSM>>" || words[i] === "<<PIM>>") {
            let j = i;
            let str = "";
            while (words[j] !== "[") {
                --j;
            }
            while (words[j + 1] !== "]") {
                ++j;
                str = str + ' ' + words[j];
            }
            str = str.split(']').join('');
            compoWithType.push(obj = { compo: str, id: '' });
            /**/
            psm.push(str);
            /**/
        }

        if (words[i] === "-(0-") {
            let j = i;
            let str = "";
            while (words[j] !== "[") {
                --j;
            }
            while (words[j + 1] !== "]") {
                ++j;
                str = str + ' ' + words[j];
            }
            str = str.split(']').join('');
            compos.push(str);

            let k = i;
            let str2 = "";
            while (words[k] !== "[") {
                ++k;
            }
            while (words[k + 1] !== "]") {
                ++k;
                str2 = str2 + ' ' + words[k];
            }
            str2 = str2.split('[').join('');
            compos2.push(str2);

            let l = i;
            let str3 = "";
            let str4 = "";
            while (words[l] !== "=>") {
                ++l;
            }
            str3 = words[l - 1];
            str4 = words[l + 1];
            port1.push(str3);
            port2.push(str4);

        }
    }

    for (let i = 0; i < words.length; i++) {

        if (words[i] === "-(0-") {
            let j = i;
            let str = "";
            while (words[j] !== "[") {
                --j;
            }
            while (words[j + 1] !== "]") {
                ++j;
                str = str + ' ' + words[j];
            }
            str = str.split(']').join('');

            things.forEach(thing => {
                if (str.toUpperCase().search(thing.toUpperCase()) !== -1) {
                    let k = i;
                    let str2 = "";
                    let str3 = "";
                    while (words[k] !== "[") {
                        ++k;
                    }
                    while (words[k + 1] !== "]") {
                        ++k;
                        str2 = str2 + ' ' + words[k];
                    }
                    str2 = str2.split('[').join('');
                    while (words[k] !== "=>") {
                        ++k;
                    }
                    while (words[k - 1] !== ":") {
                        --k;
                        str3 = str3 + ' ' + words[k];
                    }
                    let obj = { compoType: str2, name: str3 };
                    realName.push(obj)
                }
            });
        }
    }

    for (let x = 0; x < compos.length; ++x) {
        if (!psm.includes(compos[x]) && !psm.includes(compos2[x])) {
            let obj = { compo1: compos[x], compo2: compos2[x], portCompo1: port1[x], portCompo2: port2[x], view: false };
            couple.push(obj);
        } else {
            let obj = { compo1: compos[x], compo2: compos2[x], portCompo1: port1[x], portCompo2: port2[x], view: false };
            psmCouple.push(obj);
        }
    }

    sortGen(couple);

    for (let i = 0; i < resIn.length; ++i) {
        nextsIn.push({ input: resIn[i], nexts: getReachable(resIn[i]) });
    }
    for (let i = 0; i < resOut.length; ++i) {
        nextsOut.push({ output: resOut[i], nexts: getReachable(resOut[i]) });
    }
    for (let i = 0; i < resBoth.length; ++i) {
        nextsBoth.push({ both: resBoth[i], nexts: getReachable(resBoth[i]) });
    }

    nextsIn.forEach(function (element) {
        clean(element.nexts)
    })
    nextsOut.forEach(function (element) {
        clean(element.nexts)
    })
    nextsBoth.forEach(function (element) {
        clean(element.nexts)
    })

    sortIn(nextsIn);
    sortOut(nextsOut);
    sortBoth(nextsBoth);

    nextsIn.forEach(function (element) {
        for (let i = 0; i < element.nexts.length; ++i) {
            ports.forEach(function (port) {
                if (element.input !== undefined) {
                    if (port.toUpperCase() !== element.input.toUpperCase() && element.nexts[i] !== undefined) {
                        if (element.nexts[i].toUpperCase().search(port.toUpperCase()) !== -1) {
                            element.nexts.splice(i, 1);
                            --i;
                        }
                    }
                }
            });
        }
    });
    nextsOut.forEach(function (element) {
        for (let i = 0; i < element.nexts.length; ++i) {
            ports.forEach(function (port) {
                if (element.output !== undefined) {
                    if (port.toUpperCase() !== element.output.toUpperCase() && element.nexts[i] !== undefined) {
                        if (element.nexts[i].toUpperCase().search(port.toUpperCase()) !== -1) {
                            element.nexts.splice(i, 1);
                            --i;
                        }
                    }
                }
            });
        }
    });
    nextsBoth.forEach(function (element) {
        for (let i = 0; i < element.nexts.length; ++i) {
            ports.forEach(function (port) {
                if (element.both !== undefined) {
                    if (port.toUpperCase() !== element.both.toUpperCase() && element.nexts[i] !== undefined) {
                        if (element.nexts[i].toUpperCase().search(port.toUpperCase()) !== -1) {
                            element.nexts.splice(i, 1);
                            --i;
                        }
                    }
                }
            });
        }
    });

    return display();
}

function sortGen(tab) {
    let resu = [];

    resIn.forEach(function (element) {
        for (let i = 0; i < couple.length; ++i) {
            things.forEach(thing => {
                if (couple[i].portCompo1.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].portCompo2.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].compo1.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].compo2.toUpperCase().search(element.toUpperCase()) !== -1 && couple[i].compo1.toUpperCase().search(thing.toUpperCase()) === -1 && couple[i].compo2.toUpperCase().search(thing.toUpperCase()) === -1) {
                    resu.push(couple[i])
                    tab.splice(i, 1);
                    //console.log(couple[i]);
                }
            });
        }
    });

    resOut.forEach(function (element) {
        for (let i = 0; i < couple.length; ++i) {
            things.forEach(thing => {
                if (couple[i].portCompo1.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].portCompo2.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].compo1.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].compo2.toUpperCase().search(element.toUpperCase()) !== -1 && couple[i].compo1.toUpperCase().search(thing.toUpperCase()) === -1 && couple[i].compo2.toUpperCase().search(thing.toUpperCase()) === -1) {
                    resu.push(couple[i])
                    tab.splice(i, 1);
                    //console.log(couple[i]);
                }
            });
        }
    });

    resBoth.forEach(function (element) {
        for (let i = 0; i < couple.length; ++i) {
            things.forEach(thing => {
                if (couple[i].portCompo1.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].portCompo2.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].compo1.toUpperCase().search(element.toUpperCase()) !== -1
                    || couple[i].compo2.toUpperCase().search(element.toUpperCase()) !== -1 && couple[i].compo1.toUpperCase().search(thing.toUpperCase()) === -1 && couple[i].compo2.toUpperCase().search(thing.toUpperCase()) === -1) {
                    resu.push(couple[i])
                    tab.splice(i, 1);
                    //console.log(couple[i]);
                }
            });
        }
    });
    tab.forEach(function (element) {
        resu.push(element);
    });

    couple = resu;
}

function getReachable(node) {
    currents = [findTypeByRealname(node)];
    reachable = [findTypeByRealname(node)];
    while (currents.length !== 0) {
        nexts = [];
        currents.forEach(function (node) {
            couple.forEach(function (element) {
                things.forEach(thing => {
                    if ((element.portCompo1.toUpperCase().search(node.toUpperCase()) !== -1) || (element.compo1.toUpperCase().search(node.toUpperCase()) !== -1 && element.compo1.toUpperCase().search(thing.toUpperCase()) === -1) && element.view === false) {
                        if (element.compo2.toUpperCase().search(thing.toUpperCase()) === -1) {
                            let adj = element.compo2;
                            if (!reachable.includes(adj)) {
                                nexts.push(adj);
                                reachable.push(adj)
                            }
                        } else {
                            let adj = element.compo1;
                            if (!reachable.includes(adj)) {
                                nexts.push(adj);
                                reachable.push(adj)
                            }
                        }
                    } else if ((element.portCompo2.toUpperCase().search(node.toUpperCase()) !== -1) || (element.compo2.toUpperCase().search(node.toUpperCase()) !== -1 && element.compo2.toUpperCase().search(thing.toUpperCase()) === -1) && element.view === false) {
                        if (element.compo1.toUpperCase().search(thing.toUpperCase()) === -1) {
                            let adj = element.compo1;
                            if (!reachable.includes(adj)) {
                                nexts.push(adj);
                                reachable.push(adj)
                            }
                        } else {
                            let adj = element.compo2;
                            if (!reachable.includes(adj)) {
                                nexts.push(adj);
                                reachable.push(adj)
                            }
                        }
                    }
                });
            });
        });
        currents = nexts.slice();
    }
    return reachable;
}

function clean(tab) {
    for (let x = 0; x < tab.length; ++x) {
        if (tab[x].length === 0) {
            tab.splice(x, 1);
            x--;
        }
    }
}

function display() {
    /*for (let i = 0; i < resIn.length; ++i) {
        nextsIn.forEach(function (nextsi) {
            if (nextsi.input === resIn[i] && nextsi.input !== "undefined") {
                let name = findTypeByRealname(resIn[i]);
                if (name !== "nothing") {
                    for (let j = 0; j < nextsi.nexts.length; ++j) {
                        if (nextsi.nexts[j].toUpperCase().search(name.toUpperCase()) !== -1) {
                            res += '[' + stateCharts[0] + ']' + ' <-- ' + '[' + nextsi.nexts[j] + ']';
                            res += "\n";
                        } else {
                            res += '[' + nextsi.nexts[j - 1] + ']' + ' <-- ' + '[' + nextsi.nexts[j] + ']';
                            res += "\n";
                        }
                    }
                }
            }
        });

    }
    for (let i = 0; i < resOut.length; ++i) {
        nextsOut.forEach(function (nextso) {
            if (nextso.output === resOut[i] && nextso.output !== "undefined") {
                let name = findTypeByRealname(resOut[i]);
                if (name !== "nothing") {
                    for (let j = 0; j < nextso.nexts.length; ++j) {
                        if (nextso.nexts[j].toUpperCase().search(name.toUpperCase()) !== -1) {
                            res += '[' + stateCharts[0] + ']' + ' --> ' + '[' + nextso.nexts[j] + ']';
                            res += "\n";
                        } else {
                            res += '[' + nextso.nexts[j - 1] + ']' + ' --> ' + '[' + nextso.nexts[j] + ']';
                            res += "\n";
                        }
                    }
                }
            }
        });

    }
    //console.log(require("util").inspect({ nextsBoth },false, 3));
    for (let i = 0; i < resBoth.length; ++i) {
        nextsBoth.forEach(function (nextsb) {
            if (nextsb.both === resBoth[i] && nextsb.both !== "undefined") {
                let name = findTypeByRealname(resBoth[i]);
                if (name !== "nothing") {
                    for (let j = 0; j < nextsb.nexts.length; ++j) {
                        console.log(nextsb);
                        if (nextsb.nexts[j].toUpperCase().search(name.toUpperCase()) !== -1) {
                            res += '[' + stateCharts[0] + ']' + ' <--> ' + '[' + nextsb.nexts[j] + ']';
                            res += "\n";
                        } else {
                            res += '[' + nextsb.nexts[j - 1] + ']' + ' <--> ' + '[' + nextsb.nexts[j] + ']';
                            res += "\n";
                        }
                    }
                }
            }
        });
    } */

    //console.log(util.inspect({ nextsIn, nextsOut, nextsBoth, psmCouple }, false, 3, true));

    for (let i = 0; i < psmCouple.length; ++i) {
        /*if (psmCouple[i].compo1.toUpperCase().search(stateCharts[0].toUpperCase()) !== -1) {
            res += '[' + stateCharts[0] + ']' + ' <-- ' + '[' + psmCouple[i].compo2 + ']';
            res += "\n";
        } else if (psmCouple[i].compo2.toUpperCase().search(stateCharts[0].toUpperCase()) !== -1) {
            res += '[' + psmCouple[i].compo1 + ']' + ' <-- ' + '[' + stateCharts[0] + ']';
            res += "\n";
        } else */{
            nextsIn.forEach(function (element) {
                if (findIn(element.nexts, psmCouple[i].compo1)) {
                    res += '[' + psmCouple[i].compo2 + ']' + ' <-- ' + '[' + psmCouple[i].compo1 + ']' + " --" + psmCouple[i].portCompo2 + "/" + psmCouple[i].portCompo1;
                    res += "\n";
                } else if (findIn(element.nexts, psmCouple[i].compo2)) {
                    res += '[' + psmCouple[i].compo1 + ']' + ' <-- ' + '[' + psmCouple[i].compo2 + ']' + " --" + psmCouple[i].portCompo1 + "/" + psmCouple[i].portCompo2;
                    res += "\n";
                }
            });
            nextsOut.forEach(function (element) {
                if (findIn(element.nexts, psmCouple[i].compo1)) {
                    res += '[' + psmCouple[i].compo2 + ']' + ' --> ' + '[' + psmCouple[i].compo1 + ']' + " --" + psmCouple[i].portCompo2 + "/" + psmCouple[i].portCompo1;
                    res += "\n";
                } else if (findIn(element.nexts, psmCouple[i].compo2)) {
                    res += '[' + psmCouple[i].compo1 + ']' + ' --> ' + '[' + psmCouple[i].compo2 + ']' + " --" + psmCouple[i].portCompo1 + "/" + psmCouple[i].portCompo2;
                    res += "\n";
                }
            });
            nextsBoth.forEach(function (element) {
                if (findIn(element.nexts, psmCouple[i].compo1) || findIn(element.nexts, psmCouple[i].compo2)) {
                    res += '[' + psmCouple[i].compo1 + ']' + ' <--> ' + '[' + psmCouple[i].compo2 + ']' + " --" + psmCouple[i].portCompo1 + "/" + psmCouple[i].portCompo2;
                    res += "\n";
                }
            });
        }
    }
    //console.log(res);
    return res;
}

function findIn(tab, element) {
    for (let x = 0; x < tab.length; ++x) {
        if (tab[x].toUpperCase().search(element.toUpperCase()) !== -1) {
            return true;
        }
    }
    return false;
}
function findCompoInMetaM(metamodel, compo) {
    let resultCompo = {};
    metamodel.Metamodel.components.forEach(function (element) {
        if (element.SoftwareComponent.name.toUpperCase().search(compo.toUpperCase()) !== -1) {
            resultCompo = element;
        }
    });
    return resultCompo;
}

function sortIn(tab) {
    let tab2 = tab;
    let res = [];
    let bool = false;
    tab.forEach(function (element) {
        let res = [];
        let tmp = "";
        for (let x = 0; x < element.nexts.length; ++x) {
            if (x === 0 && element.nexts[x].toUpperCase().search(element.input.toUpperCase()) !== -1) {
                bool = true;
                break;
            } else if (element.nexts[x].toUpperCase().search(element.input.toUpperCase()) !== -1) {
                tmp = element.nexts[x];
                element.nexts.splice(x, 1);
                res.push(tmp);
                element.nexts.forEach(function (e) {
                    if (res.indexOf(e) === - 1) {
                        res.push(e);
                    }
                });
                element.nexts = res;
                bool = true;
                break;
            }
        }
        if (bool === false) {
            realName.forEach(function (e) {
                if (e.name.toUpperCase().search(element.input.toUpperCase()) !== -1) {
                    if (res.indexOf(e.compoType) === - 1) {
                        res.push(e.compoType);
                    }
                    element.nexts.forEach(function (e) {
                        if (res.indexOf(e) === - 1) {
                            res.push(e);
                        }
                    });
                    element.nexts = res;
                }
            });
        }
    });
}

function sortOut(tab) {
    let tab2 = tab;
    let res = [];
    let bool = false;
    tab.forEach(function (element) {
        let res = [];
        let tmp = "";
        for (let x = 0; x < element.nexts.length; ++x) {
            if (x === 0 && element.nexts[x].toUpperCase().search(element.output.toUpperCase()) !== -1) {
                bool = true;
                break;
            } else if (element.nexts[x].toUpperCase().search(element.output.toUpperCase()) !== -1) {
                tmp = element.nexts[x];
                element.nexts.splice(x, 1);
                res.push(tmp);
                element.nexts.forEach(function (e) {
                    if (res.indexOf(e) === - 1) {
                        res.push(e);
                    }
                });
                element.nexts = res;
                bool = true;
                break;
            }
        }
        if (bool === false) {
            realName.forEach(function (e) {
                if (e.name.toUpperCase().search(element.output.toUpperCase()) !== -1) {
                    if (res.indexOf(e.compoType) === - 1) {
                        res.push(e.compoType);
                    }
                    element.nexts.forEach(function (e) {
                        if (res.indexOf(e) === - 1) {
                            res.push(e);
                        }
                    });
                    element.nexts = res;
                }
            });
        }
    });
}

function sortBoth(tab) {
    let tab2 = tab;
    let bool = false;
    tab.forEach(function (element) {
        let res = [];
        let tmp = "";
        for (let x = 0; x < element.nexts.length; ++x) {
            if (x === 0 && element.nexts[x].toUpperCase().search(element.both.toUpperCase()) !== -1) {
                bool = true;
                break;
            } else if (element.nexts[x].toUpperCase().search(element.both.toUpperCase()) !== -1) {
                tmp = element.nexts[x];
                element.nexts.splice(x, 1);
                res.push(tmp);
                element.nexts.forEach(function (e) {
                    if (res.indexOf(e) === - 1) {
                        res.push(e);
                    }
                });
                element.nexts = res;
                bool = true;
                break;
            }
        }
        if (bool === false) {
            realName.forEach(function (e) {
                if (e.name.toUpperCase().search(element.both.toUpperCase()) !== -1) {
                    if (res.indexOf(e.compoType) === - 1) {
                        res.push(e.compoType);
                    }
                    element.nexts.forEach(function (e) {
                        if (res.indexOf(e) === - 1) {
                            res.push(e);
                        }
                    });
                    element.nexts = res;
                }
            });
        }
    });
}

function stillView(tab) {
    let bool = false;
    for (let x = 0; x < tab.length; ++x) {
        if (tab[x].view === false) {
            bool = true;
        }
    }
    return bool;
}

function findTypeByRealname(real) {
    let res = "nothing";
    realName.forEach(function (element) {
        let test = element.name.split(" ").join("");
        if (real.toUpperCase().search(test.toUpperCase()) !== -1) {
            let tab = real.toUpperCase().split(test.toUpperCase());
            //console.log(tab);
            let bool = true;
            tab.forEach(function (elem) {
                //console.log(elem.length);
                if (elem.length > 1) {
                    bool = false;
                    //console.log("false");
                }
            });
            if (bool) {
                res = element.compoType;
            }
        }
    });
    return res;
}

module.exports = { readSingleFile2, reset }
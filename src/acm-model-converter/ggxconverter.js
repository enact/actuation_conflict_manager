var xml2js = require('xml2js');
var crypto = require("crypto");
var fs = require("fs");
var ACMMetamodel = require("acm-metamodel");

var util = require("util");

var exports = module.exports = {};
exports.fromMetamodel = function (metamodel, callback) {
	var templatexml = fs.readFileSync("ACMRuleTemplateV2.ggx");
	var parser = new xml2js.Parser();

	let PhysicalProcessesInGGX = {};

    parser.parseString(templatexml, function (err, template) {
        // parse types
        processGGXTypes(template.Document.GraphTransformationSystem[0].Types[0]);

		template.Document.GraphTransformationSystem[0].Graph[0].Node = [];
		template.Document.GraphTransformationSystem[0].Graph[0].Edge = [];
		let commcmps = [];

		// components
		for (let compidx in metamodel.components) {
			let component = metamodel.components[compidx];
			// this is a cool optimization but it breaks "end" comm comps
			/*if (component instanceof ACMMetamodel.CommunicationComponent) {
				commcmps.push(component);
			} else */{
				let newNode = {
					'$': { ID: component.id, name: component.id, type: getGGXTypeForMetamodelType(component) },
					NodeLayout: [{ '$': { X: Math.round(component.x) || 0, Y: Math.round(component.y) || 0 } }],
					additionalLayout:
						[{ '$': { age: '0', force: '10', frozen: 'true', zone: '50' }}]
				};
				if(component instanceof ACMMetamodel.SoftwareComponent){
					newNode.Attribute = [{ '$':{constant:"true", type:"I5"},Value:[{ string: component.id_parent}]}]; 
					
				}
				template.Document.GraphTransformationSystem[0].Graph[0].Node.push(newNode);
				component.tagged = true;

				// add the physical process agg component
				for (let phyidx in component.physicalProcess) {
					let physicalProcess = component.physicalProcess[phyidx];

					// only add the phy proc if it's not instantiated already
					if (!PhysicalProcessesInGGX[physicalProcess.id]) {
						let newphy = {
							'$': { ID: physicalProcess.id, name: physicalProcess.id, type: getGGXTypeForMetamodelType(physicalProcess) }
						};
						template.Document.GraphTransformationSystem[0].Graph[0].Node.push(newphy);
						PhysicalProcessesInGGX[physicalProcess.id] = 1;
					}

					let newLink = {
						'$': { ID: component.id + "_" + physicalProcess.id, type: ggxtypedic.Link, name: component.id + "_" + physicalProcess.id, source: component.id, target: physicalProcess.id }
					};
					template.Document.GraphTransformationSystem[0].Graph[0].Edge.push(newLink);
				}
			}
		}

		// links
		let commlnks = [];
		let vlkns = [];
		for (let lnkidx in metamodel.links) {
            let link = metamodel.links[lnkidx];
			if (link.from && link.to) {
				if (link.from instanceof ACMMetamodel.CommunicationComponent || link.to instanceof ACMMetamodel.CommunicationComponent) {
					if (!link.virtual) {
						commlnks.push(link);
					} else {
						vlkns.push(link);
                    }
				} else {
					let newLink = {
						'$': { ID: link.id, type: ggxtypedic.Link, name: link.id, source: link.from.id, target: link.to.id },
						EdgeLayout: [{ '$': { bendX: '0', bendY: '0', textOffsetX: '0', textOffsetY: '-22' } }],
						additionalLayout:
							[{ '$': { aktlength: '200', force: '10', preflength: '200' } }]
					};
					template.Document.GraphTransformationSystem[0].Graph[0].Edge.push(newLink);
				}
            }
		}

		// process comm stuff, turning them into direct links for conflict identification purposes
		// start by getting the list of links coming into communication components
		let commtolnks = commlnks.filter(lnk => lnk.to instanceof ACMMetamodel.CommunicationComponent && !(lnk.from instanceof ACMMetamodel.CommunicationComponent));
		// find comm links that don't have a corresponding input link and treat them as regular softcomps
		let comminputs = metamodel.components.filter(cmp => cmp instanceof ACMMetamodel.CommunicationComponent && (metamodel.links.findIndex(lnk => lnk.to.id === cmp.id) === -1));
		let ctllen = commtolnks.length;
		comminputs.forEach(comminput => commtolnks.push(...commlnks.filter(lnk => lnk.from.id === comminput.id)));
		console.log({ premerge: ctllen, postmerge: commtolnks.length });

		// for each of those links, build "point to point" megalinks (may be several)
		//console.log("commtolnks " + commtolnks.length);
		commtolnks.forEach(commtolnk => {
			//console.log("commtolink " + commtolnk.from.id + "_" + commtolnk.to.id);
			let linkarray = getNextLinks(commtolnk, metamodel.links,0);
			//console.log("la length " + linkarray.length);
			linkarray.forEach(ll => {
				process.stdout.write("la: ");
				ll.forEach(lll => {
					process.stdout.write(lll.from.id + "_" + lll.to.id + " -- ");
				});

				let newLink = {
					'$': { ID: "ML_" + generateNRID(), type: ggxtypedic.Link, /*name: link.id,*/ source: ll[0].from.id, target: ll[ll.length-1].to.id },
					EdgeLayout: [{ '$': { bendX: '0', bendY: '0', textOffsetX: '0', textOffsetY: '-22' } }],
					additionalLayout:
						[{ '$': { aktlength: '200', force: '10', preflength: '200' } }]
				};
				template.Document.GraphTransformationSystem[0].Graph[0].Edge.push(newLink);
			});
		});


		let builder = new xml2js.Builder();
		callback(builder.buildObject(template));
	});
};


// used to build the megalink
function getNextLinks(source, links, cidx) {
	//console.log("gnl " + source.from.id + "_" + source.to.id);
	let ret = [];
	let n = links.filter(lnk => lnk.from.id === source.to.id);
	n.forEach(followLink => {
		if (followLink.to instanceof ACMMetamodel.CommunicationComponent) {
			let nextLinks = getNextLinks(followLink, links, cidx + 1);
			if (nextLinks.length > 0) {
				nextLinks.forEach(nextLink => {
					// only proceed through comm comps, stop at softcomps
					let wret = [];
					wret.push(...nextLink);
					ret.push(wret);
				});
			}
		} else {
			ret.push([followLink]);
        }
	});
	// add the current step to the list
	if (n.length === 0) {
		ret = [[source]];
	} else {
		ret = ret.map(warr => [source, ...warr]);
    }
	return ret;
}

exports.toMetamodel = function (ggx, metamodel, callback) {
	let ACMs = [];
	let idsggxtomm = {};
	let PhysicalProcesses = {};
	let modifiedACMs = [];

	// hardcoded coordinates since agg api is determined to not add them for raisins
	let X = 0, Y = 0;

	var parser = new xml2js.Parser();
    parser.parseString(ggx, function (err, ggxmodel) {
        // parse types
        processGGXTypes(ggxmodel.Document.GraphTransformationSystem[0].Types[0]);

		// components
		for (let ggxcompidx in ggxmodel.Document.GraphTransformationSystem[0].Graph[0].Node) {
			var ggxcomp = ggxmodel.Document.GraphTransformationSystem[0].Graph[0].Node[ggxcompidx];
			//console.log(metamodel.components[mmcompidx].id + "===" + ggxcomp.$.name);
			
			//if ggxcomp is an already existing ACM and if it has been modified through a rule, it must be reconfigured
			try {
				if(ggxcomp.$.type === ggxtypedic.ACMComponent && !(ggxcomp.$.name === undefined || ggxcomp.$.name.startsWith("new_component")) && ggxcomp.Attribute[0].Value[0].string[0] == "reset"){
					let acm = new ACMMetamodel.ACMComponent(ggxcomp.$.ID, ggxcomp.$.name, X, Y, ACMMetamodel.ACMType, "conflict_" + ggxcomp.$.ID, "classic", "the_best");
					// dont add to metamodel just now, wait until we know if it should be instantiated several times across flows
					modifiedACMs.push(acm);
				}	
			} catch (error) {
				
			}
			// nodes created by agg have no name
			// now they do have a new_component_X name
			if (ggxcomp.$.name === undefined || ggxcomp.$.name.startsWith("new_component")) {				
				// node doesn't exist in metamodel, it is a new ACM or monitor, add it
				// they don't have parents because i can't figure out how to get that to work
				let id = generateNRID();
				if (ggxcomp.$.type === ggxtypedic.ACMComponent) {
					let acm = new ACMMetamodel.ACMComponent(id, "ACM " + ACMs.length, X, Y, ACMMetamodel.ACMType, "conflict_" + id, "classic", "the_best");
					Y = X += 100;
					// dont add to metamodel just now, wait until we know if it should be instantiated several times across flows
					//metamodel.components.push(acm);
					ACMs.push(acm);
                } else if (ggxcomp.$.type === ggxtypedic.Monitor) {
					let mon = new ACMMetamodel.Monitor(id, "Monitor " + ACMs.length, X, Y, ACMMetamodel.monitorType, "conflict_" + id, "classic", null, null);
					Y = X += 100;
					//metamodel.components.push(mon);
					ACMs.push(mon);
                } else if (ggxcomp.$.type === ggxtypedic.PhysicalProcess) {
					// remove the physical processes added to the agg graph for computation
					PhysicalProcesses[ggxcomp.$.ID] = 1;
					continue;
				} else {
					console.log("uh why the f is the comp type " + ggxcomp.$.type);
					continue;
				}
				console.log("adding " + ggxcomp.$.ID + " (" + ggxcomp.$.name + " - " + ggxcomp.$.type+") === " + id);
				idsggxtomm[ggxcomp.$.ID] = id;
			} else {
				// remove the physical processes added to the agg graph for computation
                if (ggxcomp.$.type === ggxtypedic.PhysicalProcess) {
					PhysicalProcesses[ggxcomp.$.ID] = 1;
				}
				// Add an association between ggx id and metamodel id
				idsggxtomm[ggxcomp.$.ID] = ggxcomp.$.name;
			}
		}
		
		// links
		let foundLinks = {};
		let newlinkcount = 0, oldlinkcount = 0;
		for (let ggxlnkidx in ggxmodel.Document.GraphTransformationSystem[0].Graph[0].Edge) {
			let ggxedge = ggxmodel.Document.GraphTransformationSystem[0].Graph[0].Edge[ggxlnkidx];
			// links created in agg have no name
			if (ggxedge.$.name === undefined) {
				// add a new link
				//id, name,x,y, from, to
				/*console.log(idsggxtomm[ggxedge.$.source]);
				console.log(idsggxtomm[ggxedge.$.target]);
				finalLog(metamodel.components);*/

				if (PhysicalProcesses[ggxedge.$.source] || PhysicalProcesses[ggxedge.$.target]) {
					continue;
				}

				// find components in either the metamodel, or the pending ACM lists
				let sourcecmp = metamodel.components.find(mmcmp => mmcmp.id === idsggxtomm[ggxedge.$.source]) || ACMs.find(mmcmp => mmcmp.id === idsggxtomm[ggxedge.$.source]);
				let destcmp = metamodel.components.find(mmcmp => mmcmp.id === idsggxtomm[ggxedge.$.target]) || ACMs.find(mmcmp => mmcmp.id === idsggxtomm[ggxedge.$.target]);

				let id = generateNRID();
				let link = new ACMMetamodel.Link(id, "genLink" + id, 0, 0, sourcecmp, destcmp);
				metamodel.links.push(link);
				foundLinks[link.id] = ggxedge.$.ID;
				newlinkcount++;
			} else {
				// list all the already present links still present in agg output
				foundLinks[ggxedge.$.name] = ggxedge.$.ID;
				oldlinkcount++;
			}
		}
		console.log({ oldlinkcount, newlinkcount });

		// process comm stuff, turning them into direct links for conflict identification purposes
		// start by getting the list of links coming into communication components
		let commlnks = metamodel.links.filter(lnk => lnk.to instanceof ACMMetamodel.CommunicationComponent || lnk.from instanceof ACMMetamodel.CommunicationComponent);
		let commtolnks = commlnks.filter(lnk => lnk.to instanceof ACMMetamodel.CommunicationComponent && !(lnk.from instanceof ACMMetamodel.CommunicationComponent));
		// find comm links that don't have a corresponding input link and treat them as regular softcomps
		let comminputs = metamodel.components.filter(cmp => cmp instanceof ACMMetamodel.CommunicationComponent && (metamodel.links.findIndex(lnk => lnk.to.id === cmp.id) === -1));
		let ctllen = commtolnks.length;
		comminputs.forEach(comminput => commtolnks.push(...commlnks.filter(lnk => lnk.from.id === comminput.id)));
		console.log({ premerge: ctllen, postmerge: commtolnks.length });

		// for each of those links, build "point to point" megalinks (may be several)
		let megalinks = [];
		commtolnks.forEach(commtolnk => {
			let linkarray = getNextLinks(commtolnk, metamodel.links, 0);
			linkarray.forEach(ll => {
				megalinks.push({ from:ll[0].from.id, to: ll[ll.length-1].to.id, links: ll });
			});
		});

		// delete all links not found in agg
		var newlnks = [];
		for (let lnkidx in metamodel.links) {
			if (foundLinks[metamodel.links[lnkidx].id]) {
				// link exists copy
				newlnks.push(metamodel.links[lnkidx]);
			} else {
				/*process.stdout.write("deleting link: ");
				console.log(metamodel.links[lnkidx].from.id + "_" + metamodel.links[lnkidx].to.id);*/
			}
		}
		metamodel.links = newlnks;

		// identify megalinks and turn them back into their components
		megalinks.forEach(megalink => {
			// the obsolete flag isn't technically the correct filtering but it's not like we can get more data out of agg
			let aggmegalink = metamodel.links.find(link => link.from.id === megalink.from && link.to.id === megalink.to);
			if (aggmegalink) {
				//console.log(megalink.from + "_" + megalink.to + ":" + aggmegalink.obsolete);
				aggmegalink.obsolete = true;
				metamodel.links.push(...megalink.links);
			} else {
				process.stdout.write("no agg megalink for megalink " + megalink.from + "_" + megalink.to + " trying to find acm...");
				// if the agg megalink doesnt exist it most likely means that an ACM has been introduced
				// try to find two links from megalink source to dest with an ACM in the middle
				let aggmegalinkPreACM = metamodel.links.filter(link => (link.from.id === megalink.from && link.to instanceof ACMMetamodel.ACMComponent));
				let aggmegalinksPostACM = metamodel.links.filter(link => (link.to.id === megalink.to && link.from instanceof ACMMetamodel.ACMComponent));
				if (aggmegalinkPreACM.length === 1 && aggmegalinksPostACM.length > 0) {
					console.log("(y) found acm");
					// take the last link in the megalink and redirect it to the acm
					//but first,it verifies if the destination and the origin is not the same
					if(megalink.links[megalink.links.length - 1].from != aggmegalinkPreACM[0].to){
						megalink.links[megalink.links.length - 1].to = aggmegalinkPreACM[0].to;
					}
					// push all those links back
					metamodel.links.push(...megalink.links);
					// add the last aggmegalink found, it should be from ACM to end
					//metamodel.links.push(aggmegalinks[1]);

					// mark the first aggmegalink as obsolete
					aggmegalinkPreACM[0].obsolete = true;
				} else {
					console.log("(n) found " + aggmegalinkPreACM.length + " pre acm links and " + aggmegalinksPostACM.length + " which is not good no. skipping.");
				}
				console.log(aggmegalinkPreACM.map(aml => aml.from.id + "_" + aml.to.id));
				console.log(aggmegalinksPostACM.map(aml => aml.from.id + "_" + aml.to.id));
			}
		});

		let b4obsoletedel = metamodel.links.length;
		metamodel.links = metamodel.links.filter(l => !l.obsolete);
		console.log({ b4obsoletedel, after: metamodel.links.length });

		// delete duplicate links
		// i give up
		// agg generates duplicates so yeah
		let deduped = [];
		metamodel.links.forEach(l => {
			if (deduped.findIndex(ll => l.from === ll.from && l.to === ll.to && l.port === ll.port && l.thingmlport === ll.thingmlport) === -1) {
				deduped.push(l);
			}
		});
		console.log({ before: metamodel.links.length, after: deduped.length });
		metamodel.links = deduped;
		

		// extra step
		// components we added have no parent id
		// we need to find a link pointing to, or from that component, and get the z from the other end
		// ye it sucks
		for (let newacmidx in ACMs) {

			let newacm = ACMs[newacmidx];
			let targetlinks = [];
			let sourcelinks = [];
			// find nodes communicating with current ACM
			for (let lnkidx in metamodel.links) {
				let lnk = metamodel.links[lnkidx];
				if (!lnk.from || lnk.from.id === newacm.id) {
					targetlinks.push(lnk);
				}
				if (!lnk.to || lnk.to.id === newacm.id) {
					sourcelinks.push(lnk);
				}
			}

			// meme way to eliminate duplicates
			targetlinks = [...new Set(targetlinks)];
			sourcelinks = [...new Set(sourcelinks)];
			if (targetlinks.length === 0) {
				// how did you get there
				console.log("error: acm with no incoming links?????");
			} else if (targetlinks.length === 1) {
				// only one output, plop ACM in the flow with that node
				newacm.id_parent = targetlinks[0].to.id_parent;

				// single basic acm, set the id_conflict to its id
				newacm.id_conflict = newacm.id;

				// let's also fix x and y
				newacm.x = targetlinks[0].to.x - 100;
				newacm.y = targetlinks[0].to.y;

				// fix link references
				targetlinks[0].from = newacm;
				for (let srclnkidx in sourcelinks) {
					sourcelinks[srclnkidx].to = newacm;
				}

				// and add it to the metamodel
				metamodel.components.push(newacm);
			} else {
				// more than output, duplicate ACM in each flow
				let newacmids = {};
				// make a list of flows to make nodes for
				for (let targetidx in targetlinks) {
					let targetlnk = targetlinks[targetidx];
					if (!newacmids[targetlnk.to.id_parent]) {
						newacmids[targetlnk.to.id_parent] = generateNRID();
					}
				}

				// create acms
				for (let newacmid_parent in newacmids) {
					// create new acm node, copy of "floating" one but with id_parent to link destination
					let newacmcopy = new ACMMetamodel.ACMComponent(newacmids[newacmid_parent], newacm.name, newacm.x, newacm.y, newacm.type, newacmid_parent, newacm.acm_type, newacm.strategy, false, newacm.id_parent.replace("conflict_", ""));

					// update links
					for (let targetlnkidx in targetlinks) {
						let targetlnk = targetlinks[targetlnkidx];
						if (targetlnk.to.id_parent === newacmid_parent) {
							targetlnk.from = newacmcopy;

							// fix x and y
							newacmcopy.x = targetlnk.to.x - 100;
							newacmcopy.y = targetlnk.to.y;
						}
					}
					for (let sourcelnkidx in sourcelinks) {
						let srclnk = sourcelinks[sourcelnkidx];
						if (srclnk.from.id_parent === newacmid_parent) {
							srclnk.to = newacmcopy;
						}
					}

					// plop the new acm in the metamodel
					metamodel.components.push(newacmcopy);
				}
			}
		}

		//check all the modified ACM to remove their configuration
		//also add other ACMComponents if needed in the solution
		for (let i = 0; i < modifiedACMs.length; i++) {
			let newacm = metamodel.components.find(mmcmp => mmcmp.id === modifiedACMs[i].name)
			if(newacm){
				newacm.configured = false;
				newacm.strategy = null;

				let targetlinks = [];
				let sourcelinks = [];
				// find nodes communicating with current ACM
				for (let lnkidx in metamodel.links) {
					let lnk = metamodel.links[lnkidx];
					if (!lnk.from || lnk.from.id === newacm.id) {
						targetlinks.push(lnk);
					}
					if (!lnk.to || lnk.to.id === newacm.id) {
						sourcelinks.push(lnk);
					}
				}
	
				// meme way to eliminate duplicates
				targetlinks = [...new Set(targetlinks)];
				sourcelinks = [...new Set(sourcelinks)];

				// more than output, duplicate ACM in each flow
				let newacmids = {};
				// make a list of flows to make nodes for
				for (let targetidx in targetlinks) {
					let targetlnk = targetlinks[targetidx];
					if (!newacmids[targetlnk.to.id_parent]) {
						newacmids[targetlnk.to.id_parent] = generateNRID();
					}
				}

				// create acms
				for (let newacmid_parent in newacmids) {
					// create new acm node, copy of "floating" one but with id_parent to link destination
					let newacmcopy = new ACMMetamodel.ACMComponent(newacmids[newacmid_parent], newacm.name, newacm.x, newacm.y, newacm.type, newacmid_parent, newacm.acm_type, newacm.strategy, false, newacm.id_parent.replace("conflict_", ""));

					// update links
					for (let targetlnkidx in targetlinks) {
						let targetlnk = targetlinks[targetlnkidx];
						if (targetlnk.to.id_parent === newacmid_parent) {
							targetlnk.from = newacmcopy;

							// fix x and y
							newacmcopy.x = targetlnk.to.x - 100;
							newacmcopy.y = targetlnk.to.y;
						}
					}
					for (let sourcelnkidx in sourcelinks) {
						let srclnk = sourcelinks[sourcelnkidx];
						if (srclnk.from.id_parent === newacmid_parent) {
							srclnk.to = newacmcopy;
						}
					}

					// plop the new acm in the metamodel
					metamodel.components.push(newacmcopy);
				}
				const index = metamodel.components.indexOf(newacm);
				if (index > -1) {
					metamodel.components.splice(index, 1);
				}
			}
		}

		callback(metamodel);
	});
};

var ggxtypedic = {
	SoftwareComponent: "I2",
	SoftwareComponent_Application:"I5",
    ACMComponent: "I6",
	Action: "I9",
	Action_Application: "I11",
    PhysicalProcess: "I13",
    Monitor: "I14",
    Link: "I15",
    default: "I2"
};

function getGGXTypeForMetamodelType(component) {
	if (component instanceof ACMMetamodel.Monitor) {
        return ggxtypedic.Monitor;
	}
	if (component instanceof ACMMetamodel.ACMComponent) {
        return ggxtypedic.ACMComponent;
	}
	if (component instanceof ACMMetamodel.Action) {
        return ggxtypedic.Action;
	}
	if (component instanceof ACMMetamodel.PhysicalProcess) {
        return ggxtypedic.PhysicalProcess;
	}
    if (component instanceof ACMMetamodel.SoftwareComponent) {
        return ggxtypedic.SoftwareComponent;
    }

    return ggxtypedic.default;
}

function processGGXTypes(types) {
    // node types
    for (let typeidx in types.NodeType) {
        let nodeType = types.NodeType[typeidx].$;
        switch (nodeType.name.split("%")[0]) {
            case "softComp":
                ggxtypedic.SoftwareComponent = nodeType.ID;
                break;
            case "ACM":
                ggxtypedic.ACMComponent = nodeType.ID;
                break;
            case "Action":
                ggxtypedic.Action = nodeType.ID;
                break;
            case "physical":
                ggxtypedic.PhysicalProcess = nodeType.ID;
                break;
            case "Monitor":
                ggxtypedic.Monitor = nodeType.ID;
                break;
            default: break;
        }
    }

    // link type
    ggxtypedic.Link = types.EdgeType[0].$.ID;

    // copy softcomp to default
    ggxtypedic.default = ggxtypedic.SoftwareComponent;

    console.log("Running GGX conversion using type dictionary:");
    finalLog(ggxtypedic);
    console.log("---------------");
}

function generateNRID() {
	return generateID(8) + "." + generateID(6);
}

// generate hex id https://stackoverflow.com/a/27747377
// dec2hex :: Integer -> String
function dec2hex(dec) {
	return ('0' + dec.toString(16)).substr(-2);
}

// generateId :: Integer -> String
function generateID(len) {
	var arr = new Uint8Array((len || 40) / 2);
	crypto.randomFillSync(arr);
	return Array.from(arr, dec2hex).join('');
}

function finalLog(o) {
	console.log(util.inspect(o, false, null, true));
}
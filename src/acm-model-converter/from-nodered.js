﻿/**
 * from node red to pivot model
 **/
const util = require('util');
var exports = module.exports = {};
const http = require("http");
const URL = require('url').URL;
const acm_metamodel = require("acm-metamodel");

var SoftwareComponent = acm_metamodel.SoftwareComponent;
var Action = acm_metamodel.Action;
var Monitor = acm_metamodel.Monitor;
var ACMComponent = acm_metamodel.ACMComponent;
var Link = acm_metamodel.Link;
var Metamodel = acm_metamodel.Metamodel;

var WebsocketC = acm_metamodel.WebsocketC;
var AMQPC = acm_metamodel.AMQPCommunication;
var HTTPC = acm_metamodel.HTTPCommunication;

var componentType = acm_metamodel.componentType;
var actionType = acm_metamodel.actionType;
var ACMType = acm_metamodel.ACMType;
var monitorType = acm_metamodel.monitorType;
//communication
var httpRequestType = acm_metamodel.httpRequestType;
var httpInType = acm_metamodel.httpInType;
var httpOutType = acm_metamodel.httpOutType;
var websocketInType = acm_metamodel.websocketInType;
var websocketOutType = acm_metamodel.websocketOutType;
var AMQPInType = acm_metamodel.AMQPInType;
var AMQPOutType = acm_metamodel.AMQPOutType;

// subflow matching regex
let subflowInstanceRE = /^subflow:(.+)$/;

// ignored types
let ignoredTypes = [acm_metamodel.flowType, acm_metamodel.commentType, "ui_base", "ui_tab", "ui_group", "ui_spacer"];

// processing type registry
let ProcessingRegistry = require('./DynamicProcessingTypesRegistry');

exports.fromURL = function (nodeRedFlowURL, callback, url) {
	url = new URL(nodeRedFlowURL);
	var options = {
		path: url.pathname,
		host: url.hostname,
		port: url.port,
		method: 'GET'
    };

    // detect a flow UI url and replace it by an API url
    if (url.pathname === '/') {
        nodeRedFlowURL = nodeRedFlowURL.replace("#", "");
        url = new URL(nodeRedFlowURL);
        // weird check but ok
        if (url.pathname === "/") {
            nodeRedFlowURL = nodeRedFlowURL + "flows";
            url = new URL(nodeRedFlowURL);
        }
        var options = {
            path: url.pathname,
            host: url.hostname,
            port: url.port,
            method: 'GET'
        };
    }

	var req = http.request(options, function (httpIncomingMessage) {
		var nodeRedFlowResponse = "";
		httpIncomingMessage.setEncoding('utf8');
		httpIncomingMessage.on('data', (chunk) => {
			nodeRedFlowResponse += chunk;
		});
        httpIncomingMessage.on('end', () => {
			exports.fromString(nodeRedFlowResponse, callback, url.href);
			//console.log(nodeRedFlowResponse);
		});
	});
	req.on('error', (e) => {
		console.error(`problem with request: ${e.message}`);
	});
	req.end();
};

exports.fromString = function (nodeRedFlowStr, callback, url) {
	try {
		var flow = JSON.parse(nodeRedFlowStr);
	} catch (e) {
		console.log("unable to parse json " + nodeRedFlowStr);
		return;
	}
	return exports.fromJSON(flow, callback, url);
};

exports.fromJSON = function (nodeRedFlow, callback, url) {
    //creating all software components and parts of links
    let softwareComponents = [];
    let links = [];

    // the loaded model is a dumb node list
    // we need to make it some sort of object to hold extra data
    if (nodeRedFlow.nodes === undefined) {
        nodeRedFlow = {
            nodes: nodeRedFlow,
            isACMGeneratedModel: true,
            label: "Flow",
            id: null
        };
    }
    let nodesList = nodeRedFlow.nodes;
    let subflows = { types: [], instances: [] };

    // go through the list
    for (let i = 0; i < nodesList.length; i++) {
        // update id
        if (!nodeRedFlow.id && nodesList[i].z !== "") nodeRedFlow.id = nodesList[i].z;
        let component = null;

        component = processNode(nodesList, nodesList[i], softwareComponents, links, subflows);

        if (component === undefined || component === null) continue;

        // set the disabled flag 
        component.disabled = nodesList[i].disabled;

        //get all links
        if (nodesList[i].wires) {
            for (let port in nodesList[i].wires) {
                for (let j = 0; j < nodesList[i].wires[port].length; j++) {
                    // eliminate duplicated wires
                    // not sure how it happens but it happens in one of the test flows so
                    if (nodesList[i].wires[port].indexOf(nodesList[i].wires[port][j]) === j) {
                        links.push(new Link("Lnk_" + nodesList[i].id + "-" + port + "_" + nodesList[i].wires[port][j], nodesList[i].id + "_" + nodesList[i].wires[port][j], component.x, component.y, component, nodesList[i].wires[port][j], port));
                    } else {
                        console.log("! duplicated wire " + nodesList[i].wires[port][j] + " for node " + component.id);
                    }
                }
            }
        }
    }

    // process subflows, add input links
    subflows.instances.forEach(sfinst => {
        // { node: currentNode, prefix: prefix, sftemplateid, nodes: newNodes };
        // get template
        let template = subflows.types.find(sftpl => sftpl.id === sfinst.sftemplateid);
        // get destination node ids
        let destids = template.in[0].wires.map(d => sfinst.prefix + d.id);
        // get links to sf node
        links.filter(lnk => lnk.to === sfinst.node.id).forEach(lnktosf => {
            // replace link by links to all inputs
            destids.forEach(destid => {
                links.push(new Link("sfIn_" + destid + "_" + lnktosf.id, destid + "_" + lnktosf.name, lnktosf.x, lnktosf.y, lnktosf.from, destid, lnktosf.port, true));
            });
        });
        links = links.filter(lnk => lnk.to !== sfinst.node.id);
    });

    // delete all nodes created from subflow templates
    softwareComponents = softwareComponents.filter(cmp => subflows.types.findIndex(sftpl => sftpl.id === cmp.id_parent) === -1);

    //updating the "to" field of links
    // delete broke links with no valid "to" target
    let newLinks = [];
	for (let i = 0; i < links.length; i++) {
		for (let j = 0; j < softwareComponents.length; j++) {
            if (links[i].to === softwareComponents[j].id) {
                links[i].to = softwareComponents[j];
                if (links[i].to) {
                    newLinks.push(links[i]);
                }
            }
		}
    }
    if (links.length != newLinks.length) {
        console.log("couldnt fix all links");
        links.filter(l => newLinks.findIndex(ll => l.id === ll.id) === -1).forEach(lll => console.log(lll.to));
    }
    links = newLinks;

    // delete the ACM tags and syncs and replace them with links
    // set the ACMs to not deployed that should trigger a generation in the output phase
    for (let i = 0; i < softwareComponents.length; i++) {
        let softcomp = softwareComponents[i];
        if (softcomp instanceof ACMComponent) {
            //console.log(softcomp);
            switch (softcomp.type) {
                case acm_metamodel.ACMType:
                    softcomp.deployed = false;
                    break;
                case acm_metamodel.ACMSyncType:
                case acm_metamodel.ACMTagType:
                    // get link from and to
                    let from = links.filter(l => l.to === softcomp);
                    let to = links.filter(l => l.from === softcomp);

                    //console.log("f/t : " + from.length + " " + to.length);
                    if (from.length === 1 && to.length === 1) {
                        // easy case: 1/1
                        links.push(new Link("vacmdel_" + from[0].from.id + "_" + to[0].to.id, from[0].from.id + "_" + to[0].to.id, 0, 0, from[0].from, to[0].to, from[0].port, false));
                    } else if (from.length >= 2 && to.length === 1) {
                        // 2/1 case
                        // 2 from, 1 to, generate two links
                        for (let j = 0; j < from.length; j++) {
                            links.push(new Link("vacmdel_" + from[j].from.id + "_" + to[0].to.id, from[j].from.id + "_" + to[0].to.id, 0, 0, from[j].from, to[0].to, from[j].port, false));
                        }
                    } else if (from.length === 1 && to.length >= 2) {
                        // 1/2 case
                        // 1 from, 2 to, generate two links
                        for (let j = 0; j < to.length; j++) {
                            links.push(new Link("vacmdel_" + from[0].from.id + "_" + to[j].to.id, from[0].from.id + "_" + to[j].to.id, 0, 0, from[0].from, to[j].to, from[j].port, false));
                        }
                    } else {
                        console.log("ACM Sync/Tag with more or less than 1 link from or to (" + from.length + "/" + to.length + "), wont be fixed if not n/1 or 1/n.");
                        break;
                    }

                    console.log("links length pre filter " + links.length);
                    links = links.filter(l => l.from !== softcomp && l.to !== softcomp);
                    console.log("links length post filter " + links.length);
                    softwareComponents.splice(i, 1);
                    i--;
                    break;

                default:
                    break;
            }
        }
    }

	// tag nrflow as nr model for later reconstruction 
    nodeRedFlow.acm_model_type = "nodered";
    nodeRedFlow.url = url;
	var models = new Metamodel(softwareComponents, links, nodeRedFlow);
	callback(models);
};

// process a single node, here to be used recursively for subflows
function processNode(nodesList, currentNode, softwareComponents, links, subflows, subflowInstancePrefix) {
    let component = null;
    switch (currentNode.type) {
        case ACMType:
            /*console.log("acm from nodered");
            console.log(currentNode);*/
            component = new ACMComponent(currentNode.id, currentNode.name, currentNode.x, currentNode.y, ACMType, currentNode.z, "classic", "the_best", true, currentNode.id, true);
            component.setParamsFromNodeRed(currentNode);
            softwareComponents.push(component);
            break;
        case acm_metamodel.ACMSyncType:
            /*console.log("acm sync from nodered");
            console.log(currentNode);*/
            component = new ACMComponent(currentNode.id, currentNode.name, currentNode.x, currentNode.y, acm_metamodel.ACMSyncType, currentNode.z, "classic", "the_best", true, currentNode.id.split("_sync")[0], true);
            component.setParamsFromNodeRed(currentNode);
            softwareComponents.push(component);
            break;
        case acm_metamodel.ACMTagType:
            /*console.log("acm tag from nodered");
            console.log(currentNode);*/
            component = new ACMComponent(currentNode.id, currentNode.name, currentNode.x, currentNode.y, acm_metamodel.ACMTagType, currentNode.z, "classic", "the_best", true, currentNode.id.split("-tag-")[0], true);
            component.setParamsFromNodeRed(currentNode);
            softwareComponents.push(component);
            break;
        case monitorType:
            component = new Monitor(currentNode.id, currentNode.name, currentNode.x, currentNode.y, monitorType, currentNode.z, "monitor", null, null, true, true);
            component.setParamsFromNodeRed(currentNode);
            softwareComponents.push(component);
            break;
        case acm_metamodel.subflowType:
            subflows.types.push(currentNode);
            break;
        default:
            // check if its a type to ignore
            if (ignoredTypes.findIndex(t => t === currentNode.type) > -1) {
                console.log("from-nodered, skipping node " + currentNode.id + " because its type " + currentNode.type + " is ignored");
                break;
            }
            // first check if the node can be processed using the type registry
            if (ProcessingRegistry.registry[currentNode.type]) {
                component = ProcessingRegistry.registry[currentNode.type](currentNode, nodesList);
                if (component) {
                    softwareComponents.push(component);
                }
            // check if its a subflow
            // subflows are matched with a regex in node-red runtime too (@node-red/runtime/lib/nodes/flows/util.js#parseConfig)
            } else
            if (subflowInstanceRE.exec(currentNode.type)) {
                let newNodes = [];
                let subflowTemplateID = currentNode.type.split(":")[1];
                let basex = currentNode.x - 150;
                let basey = currentNode.y - 50;

                // create nodes from template
                let prefix = "sfinstance_" + currentNode.id + "_";
                let subflowNodes = nodesList.filter(n => n.z === subflowTemplateID);
                subflowNodes.forEach(sfn => {
                    // recursive node process
                    let newnode = processNode(nodesList, sfn, softwareComponents, links, subflows, prefix);
                    newnode.id_parent = currentNode.z;
                    newNodes.push(newnode);

                    // quick correction of position
                    newnode.x += basex;
                    newnode.y += basey;

                    // add links for newly created instance
                    if (sfn.wires) {
                        sfn.wires.forEach(wirearray =>
                            wirearray.forEach((wire, port) => {
                                links.push(new Link("sfLnk_" + newnode.id + "_" + prefix + wire, newnode.id + "_" + prefix + wire, 0, 0, newnode, prefix + wire, port))
                            })
                        );
                    }
                });
                let newInstance = { node: currentNode, sftemplateid: subflowTemplateID, prefix: prefix, nodes: newNodes };
                subflows.instances.push(newInstance);

                // acquire subflow def for in/outs
                let subflowdef = nodesList.find(n => n.id === subflowTemplateID);
                // updating the out side: add new links between the generated nodes and the subflow nodes' outputs
                if (subflowdef.out) {
                    // ports
                    subflowdef.out.forEach((outport, portidx) => {
                        if (outport.wires) {
                            // wires in ports from sf
                            outport.wires.forEach(outwire => {
                                let sfnid = prefix + outwire.id;
                                let sfnode = softwareComponents.find(s => s.id === sfnid);

                                if (!sfnode) console.log("ERROR:: cant find node " + sfnid);
                                // wires out of sf
                                if (currentNode.wires && sfnode) {
                                    currentNode.wires[portidx].forEach(cnwire => {
                                        links.push(new Link("sfLnk_out_" + sfnode.id + "_" + cnwire, sfnode.id + "_" + cnwire, 0, 0, sfnode, cnwire, outwire.port))
                                    })
                                }
                            });
                        }
                    });
                }
                // updating the in side is done later 
            } else {
                // anything that isn't a special protected type is a software component
                component = new SoftwareComponent(currentNode.id, currentNode.name, currentNode.x, currentNode.y, currentNode.type, currentNode.z);
                softwareComponents.push(component);
            }
            break;
    }
    // add a prefix for subflow nodes
    if (subflowInstancePrefix) {
        component.id = subflowInstancePrefix + component.id;
        //currentNode.id = component.id;
        //if (currentNode.wires) {
        //    currentNode.wires = currentNode.wires.map(warr => warr.map(w => subflowInstancePrefix + w));
        //}
    }
    return component;
}
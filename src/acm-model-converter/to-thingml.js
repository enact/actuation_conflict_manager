var exports = module.exports = {};
const fs = require("fs")
const acm_metamodel = require("acm-metamodel");
const path = require("path"); 
const search = require("recursive-search");
const http = require("http");

let useJava = true;

exports.fromModel = function (model, genesiscmp, callback) {
    let imports = [];
    let instances = [];
    let thingConnectors = [];
    let acmConnectors = [];

    console.log(require("util").inspect(model.components, false, 3));

    // get the list of components to import and instances to create
    model.components.forEach(cmp => {
        if (cmp instanceof acm_metamodel.ACMComponent) {
            console.log("toThingML: new component " + cmp.name);
            imports.push({ path: cmp.type + ".thingml" });
            instances.push({ type: getThingForACMComponent(cmp), name: getInstanceForComponent(cmp), acmtype: cmp.type, component: cmp });
        }
    });
    // deduplicate import list
    imports = imports.filter((imp, index) => imports.findIndex(i => i.path === imp.path) === index);

    // copy template files
    imports.forEach(i => fs.copyFileSync("./thingml/thingml_acm_repository/" + i.path, path.dirname(genesiscmp.file) + "/" + i.path));
    // copy datatypes
    fs.copyFileSync("./thingml/thingml_acm_repository/acm-datatypes.thingml", path.dirname(genesiscmp.file) + "/acm-datatypes.thingml");

    // split links into existing links vs new ACM links
    model.links.forEach(lnk => {
        if (lnk.from instanceof acm_metamodel.ACMComponent || lnk.to instanceof acm_metamodel.ACMComponent) {
            acmConnectors.push({
                from: getInstanceForComponent(lnk.from),
                to: getInstanceForComponent(lnk.to),
                portfrom: lnk.port.source,
                portto: lnk.port.target,
                type: lnk.to.type
            });
        } else {
            thingConnectors.push({
                from: getInstanceForComponent(lnk.from),
                to: getInstanceForComponent(lnk.to),
                portfrom: lnk.port.source,
                portto: lnk.port.target
            });
        }
    });

    // only proceed with the new file if instances need to be created
    if (instances.length > 0) {
        // generate imports
        let importsStr = "";
        imports.forEach(i => {
            // add the relevant line
            importsStr += "import \"" + i.path + "\"\n";
        });
        // add the datatypes import
        importsStr += "import \"acm-datatypes.thingml\"\n";

        // sort instances to insert tags before syncs before acms
        instances = instances.sort((first, second) => {
            let fval = first.acmtype === "acm" ? 1 : (first.acmtype === "acm-sync" ? 2 : (first.acmtype === "acm-tag" ? 3 : 0));
            let sval = second.acmtype === "acm" ? 1 : (second.acmtype === "acm-sync" ? 2 : (second.acmtype === "acm-tag" ? 3 : 0));
            return sval - fval;
        });
        console.log(instances);

        // use Java transform server
        if (useJava) {
            fs.writeFileSync("thingml/tothingml.json", JSON.stringify({ thingmlInputPath: genesiscmp.file, thingmlOutputPath: genesiscmp.file.replace(".thingml", "_acm.thingml"), targetLanguage: genesiscmp.target_language, instances, imports, model }));
            callJavaServer({ thingmlInputPath: genesiscmp.file, thingmlOutputPath: genesiscmp.file.replace(".thingml", "_acm.thingml"), targetLanguage: genesiscmp.target_language, instances, imports, model }, callback);
            genesiscmp.file = genesiscmp.file.replace(".thingml", "_acm.thingml");
        } else {
            // use JS transform method
            // !!! PROBABLY BROKEN NEEDS THE GOOD TEMPLATES

            // read current file
            let thingmlfile = fs.readFileSync(genesiscmp.file).toString();
            // remove comments
            // tried to make my own but someone smarter made a good one
            // https://stackoverflow.com/a/15123777
            thingmlfile = thingmlfile.replace(/\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm, "$1");

            // extract configuration
            let configurationStart = thingmlfile.indexOf("configuration");
            let endOfFile = thingmlfile.substr(configurationStart);
            let configurationEnd = endOfFile.indexOf("}"); // we purposely skip the end bracket to make insertion easier
            let configurationData = endOfFile.substr(0, configurationEnd);

            // get ports and their message type
            let thingsWithTypedPorts = getThingsWithTypedPorts(genesiscmp);

            // get already declared instances 
            let configurationinstances = findInstancesInConfiguration(configurationData);

            // get link list
            let connectorIndexes = getAllIndexes(configurationData, "connector");
            let existingConnectors = connectorIndexes.map(i => {
                let indexOfStartOfInstance = i + 9; // literal connector
                let indexOfStartOfPort = configurationData.indexOf(".", indexOfStartOfInstance);
                let indexOfArrow = configurationData.indexOf("=>", indexOfStartOfPort);
                let indexOfStartOfInstance2 = indexOfArrow + 2;
                let indexOfStartOfPort2 = configurationData.indexOf(".", indexOfStartOfInstance2);
                let indexOfEndOfPort2 = configurationData.substring(indexOfStartOfPort2).search(/[\t\ \n\r]/) + indexOfStartOfPort2;

                return {
                    from: configurationData.substr(indexOfStartOfInstance, indexOfStartOfPort - indexOfStartOfInstance).trim(),
                    to: configurationData.substr(indexOfStartOfInstance2, indexOfStartOfPort2 - indexOfStartOfInstance2).trim(),
                    portfrom: configurationData.substr(indexOfStartOfPort + 1, indexOfArrow - indexOfStartOfPort - 1).trim(),
                    portto: configurationData.substr(indexOfStartOfPort2 + 1, indexOfEndOfPort2 - indexOfStartOfPort2).trim(),
                    startIndex: i
                }
            });

            // disable links no longer in the model
            // generate the list of dead links at the same time
            let inc = 0;
            let deadConnectors = existingConnectors.filter(conn => {
                if (thingConnectors.findIndex(aconn => aconn.from === conn.from && aconn.to === conn.to && aconn.portfrom === conn.portfrom && aconn.portto === conn.portto) === -1) {
                    // add a comment that kinda overwrites the existing connector but hey it works
                    configurationData = configurationData.substr(0, conn.startIndex + inc) + "//" + configurationData.substr(conn.startIndex + inc);
                    inc += 2;
                    return true;
                }
                return false;
            });

            // insert instances and their configuration
            let tagindex = 0;
            configurationData += "\n// ACM INSTANCES\n";
            instances.forEach(i => {
                configurationData += "\tinstance " + i.name + " : " + i.type + "\n";
                if (i.acmtype === "acm-tag") {
                    // set tag and index
                    configurationData += "\tset " + i.name + ".tag=\"" + i.name + "\"\n";
                    configurationData += "\tset " + i.name + ".idx=" + tagindex + "\n";
                    tagindex++;
                }
                if (i.acmtype === "acm-sync") {
                    // add timer instance
                    configurationData += "\tinstance " + i.name + "_timer : " + getTimerForTargetLanguage(genesiscmp.target_language) + "\n";
                    // set buffersize and add timer connector
                    configurationData += "\tset " + i.name + ".bufferlen=" + tagindex + "\n";
                    configurationData += "\tconnector " + i.name + ".timer=>" + i.name + "_timer.timer\n";
                }
                if (i.acmtype === "acm") {
                    // set buffer size
                    configurationData += "\tset " + i.name + ".bufferlen=" + tagindex + "\n";
                }
                configurationData += "\n";
            });
            configurationData += "// ACM INSTANCES END\n";

            // fix new links by giving them ports from dead links and insert them
            // first we order acm connectors so the port property naturally passes through from tag to link to acm
            acmConnectors.sort((first, second) => {
                let fval = first.type === "acm" ? 1 : (first.type === "acm-sync" ? 2 : (first.type === "acm-tag" ? 3 : 0));
                let sval = second.type === "acm" ? 1 : (second.type === "acm-sync" ? 2 : (second.type === "acm-tag" ? 3 : 0));
                return fval < sval;
            });
            configurationData += "\n// ACM CONNECTORS\n";
            acmConnectors.forEach(conn => {
                let replacedConnector = deadConnectors.find(dconn => dconn.to === conn.to || dconn.from === conn.from) || acmConnectors.find(aconn => aconn.to === conn.from);
                if (replacedConnector) {
                    conn.portfrom = replacedConnector.portfrom;
                    conn.portto = replacedConnector.portto;
                    // insert link
                    configurationData += "\n\t\tconnector " + conn.from + "." + conn.portfrom + " => " + conn.to + "." + conn.portto;
                } else {
                    console.log("new link with no port:\n" + require("util").inspect(conn) + "\nwill be discarded");
                }
            });
            configurationData += "\n// ACM CONNECTORS END\n";

            // build entire string
            // carefully put the imports before the first thing declaration
            let indexOfThing = thingmlfile.indexOf("thing ");
            thingmlfile = thingmlfile.substr(0, indexOfThing)
                + importsStr + "\n"
                + thingmlfile.substr(indexOfThing, configurationStart - indexOfThing)
                + configurationData + "\n"
                + endOfFile.substr(configurationEnd);

            // build the datatypes thing file
            let destinationdir = path.dirname(genesiscmp.file);
            let types = buildDatatypes(acmConnectors, thingsWithTypedPorts, configurationinstances, destinationdir);

            // generate the other acm files from the type
            generateACMs(imports, types, destinationdir);

            // save new file
            //console.log(configurationData);
            genesiscmp.file = genesiscmp.file.replace(".thingml", "_acm.thingml");
            fs.writeFileSync(genesiscmp.file, thingmlfile);
        }
    } else {
        console.log("error: no instances");
        callback();
    }
}

function buildDatatypes(acmConnectors, thingsWithTypedPorts, configurationinstances, destinationdir) {
    let types = {};
    // thanks to the previous connector sort, we know the first one has to be acm-tag
    // note: we're going to keep assuming homogeneous types for now
    let acmtagconn = acmConnectors[0];

    // find the type with portfrom
    let inputthingtype = thingsWithTypedPorts[configurationinstances[acmtagconn.from]];
    let port = inputthingtype.ports.find(p => p.name === acmtagconn.portfrom);
    // only take the first message in the sends
    types.input = {
        IN_NAME: port.sends[0].type,
        IN_PARAMS: port.sends[0].params.map(p => p.name + ": " + p.type).join(", "),
        IN_ACM_PARAMS: port.sends[0].params.map(p => p.name + ": " + p.type + "[]").join(", "),
        IN_SEND: port.sends[0].params.map(p => "i." + p.name).join(", "),

        ACM_SYNC_SEND1: port.sends[0].params.map(p => "buffer_" + p.name).join(", "),
        ACM_SYNC_SEND2: port.sends[0].params.map(p => "buffer_" + p.name + "2").join(", "),
        ACM_SYNC_BUFFERS1: port.sends[0].params.map(p => "property buffer_" + p.name + " : " + p.type + "[bufferlen]").join("\n"),
        ACM_SYNC_BUFFERS2: port.sends[0].params.map(p => "property buffer_" + p.name + "2 : " + p.type + "[bufferlen]").join("\n"),
        ACM_SYNC_BUFFER_INPUT1: port.sends[0].params.map(p => "buffer_" + p.name + "[i.idx] = i." + p.name).join("\n"),
        ACM_SYNC_BUFFER_INPUT2: port.sends[0].params.map(p => "buffer_" + p.name + "2[i.idx] = i." + p.name).join("\n"),
        ACM_SYNC_BUFFER_CLEAN1: port.sends[0].params.map(p => "buffer_" + p.name + "[index] = " + getNullExpressionForPort(p)).join("\n"),
        ACM_SYNC_BUFFER_CLEAN2: port.sends[0].params.map(p => "buffer_" + p.name + "2[index] = " + getNullExpressionForPort(p)).join("\n"),
        ACM_SYNC_FIRST_BUFFER1: "buffer_" + port.sends[0].params[0].name,
        ACM_SYNC_FIRST_BUFFER2: "buffer_" + port.sends[0].params[0].name + "2",

        ACM_INPUT: port.sends[0].params.map(p => "buffer_" + p.name + " = i." + p.name).join("\n"),
        ACM_FIRST_BUFF_TMP: "buffer_" + port.sends[0].params[0].name,
        ACM_FIRST_BUFF_NULL: getNullExpressionForPort(port.sends[0].params[0]),
        ACM_SEND: port.sends[0].params.map(p => "buffer_" + p.name+ "[index]").join(", "),
    };

    // now make the datatype file from the template in the repository
    let datatypetemplatestr = fs.readFileSync("./thingml/thingml_acm_repository/acm-datatypes.thingml").toString();
    datatypetemplatestr = datatypetemplatestr.replace(/IN_NAME/g, types.input.IN_NAME);
    datatypetemplatestr = datatypetemplatestr.replace(/IN_PARAMS/g, types.input.IN_PARAMS);
    datatypetemplatestr = datatypetemplatestr.replace(/IN_ACM_PARAMS/g, types.input.IN_ACM_PARAMS);
    fs.writeFileSync(destinationdir + path.sep + "acm-datatypes.thingml", datatypetemplatestr);
    
    return types;
}

function getNullExpressionForPort(param) {
    switch (param.type) {
        case "String": return "`null`";
        case "Integer": return Number.MIN_SAFE_INTEGER;
        default: return 0;
    }
}

function generateACMs(imports, types, destinationdir) {
    imports.forEach(i => {
        // replace all the data in the template file
        let datatypetemplatestr = fs.readFileSync("./thingml/thingml_acm_repository/" + i.path).toString();
        datatypetemplatestr = datatypetemplatestr.replace(/IN_NAME/g, types.input.IN_NAME);
        datatypetemplatestr = datatypetemplatestr.replace(/IN_PARAMS/g, types.input.IN_PARAMS);
        datatypetemplatestr = datatypetemplatestr.replace(/IN_ACM_PARAMS/g, types.input.IN_ACM_PARAMS);
        datatypetemplatestr = datatypetemplatestr.replace(/IN_SEND/g, types.input.IN_SEND);

        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_SEND1/g, types.input.ACM_SYNC_SEND1);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_SEND2/g, types.input.ACM_SYNC_SEND2);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_BUFFERS1/g, types.input.ACM_SYNC_BUFFERS1);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_BUFFERS2/g, types.input.ACM_SYNC_BUFFERS2);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_BUFFER_INPUT1/g, types.input.ACM_SYNC_BUFFER_INPUT1);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_BUFFER_INPUT2/g, types.input.ACM_SYNC_BUFFER_INPUT2);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_BUFFER_CLEAN1/g, types.input.ACM_SYNC_BUFFER_CLEAN1);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_BUFFER_CLEAN2/g, types.input.ACM_SYNC_BUFFER_CLEAN2);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_FIRST_BUFFER1/g, types.input.ACM_SYNC_FIRST_BUFFER1);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SYNC_FIRST_BUFFER2/g, types.input.ACM_SYNC_FIRST_BUFFER2);

        datatypetemplatestr = datatypetemplatestr.replace(/ACM_FIRST_BUFF_TMP/g, types.input.ACM_FIRST_BUFF_TMP);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_FIRST_BUFF_NULL/g, types.input.ACM_FIRST_BUFF_NULL);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_INPUT/g, types.input.ACM_INPUT);
        datatypetemplatestr = datatypetemplatestr.replace(/ACM_SEND/g, types.input.ACM_SEND);
        fs.writeFileSync(destinationdir + path.sep + i.path, datatypetemplatestr);
    });
}

function findInstancesInConfiguration(configurationData) {
    let instanceindexes = getAllIndexesRegex(configurationData, /instance[ \t]+(?<name>[a-zA-Z0-9_]+)[ \t]+:[ \t]+(?<type>[a-zA-Z0-9_]+)/g, 1);
    let ret = {};
    instanceindexes.forEach(iidx => {
        ret[iidx.groups.name] = iidx.groups.type;
    });
    return ret;
}

function getTimerForTargetLanguage(targetLanguage) {
    switch (targetLanguage) {
        case "arduino": return "TimerArduino";
        case "java": return "TimerJava";
        default: return null;
    }
}

function getThingForACMComponent(cmp) {
    switch (cmp.type) {
        case "acm": return "ACMThing";
        case "acm-sync": return "ACMSyncThing";
        case "acm-tag": return "ACMTagThing";
        default: return null;
    }
}

function getAllIndexes(str, val, start = 0) {
    let i = start;
    let indexes = [];
    while (i !== -1) {
        i = str.indexOf(val, i+1);
        if (i !== -1) { indexes.push(i); }
    }
    return indexes;
}

function getAllIndexesRegex(str, regex, raw=0) {
    let indexes = [];
    let match;
    while ((match = regex.exec(str)) !== null) {
        indexes.push(raw?match:match.index);
    }
    return indexes;
}

function getInstanceForComponent(component) {
    // append ID to name for ACM components, otherwise just use the name to conform to ThingML instance names
    // remove dots from NRIDs
    return component.name.split(":")[0].replace("[", "").replace(" ", "") + (component instanceof acm_metamodel.ACMComponent ? "_" + component.id.replace(/\./g,"_").replace(/-/g, "_"):"");
}

// load the _merged.thingml file to get the list of all messages
function getThingsWithTypedPorts(genesiscmp) {
    let messageTypes = [];
    let things = {};

    // load the full merged post compilation thingml file as it contains ALL the data
    let compilationDirectory = path.resolve(process.cwd(), "./thingml/" + genesiscmp.name + "/");
    let mergedfilename = search.recursiveSearchSync(/_merged.thingml/, compilationDirectory)[0];
    let mergedFileData = fs.readFileSync(mergedfilename).toString();

    // nuke them tabs
    mergedFileData.replace('/\t/', "");

    // get a list of things
    let thingIndices = getAllIndexesRegex(mergedFileData, /thing([ \t]+fragment)?[ \t]+[a-zA-Z0-9_]+([ \t]+includes[ \t]+[a-zA-Z0-9_ ,]+)?/gm);
    thingIndices.forEach(i => {
        // get the thing's name/ID
        let startfrag = mergedFileData.indexOf("fragment", i);
        let startid = mergedFileData.indexOf(" ", i);
        if (startfrag !== -1) {
            // check that the fragment we found belongs to this declaration
            let startbracket = mergedFileData.indexOf("{", i)
            if (startfrag < startbracket) {
                // theres a fragment move to the next white space
                startid = mergedFileData.indexOf(" ", startid + 1);
            }
        }
        let endid = mergedFileData.indexOf(" ", startid + 1);
        let thingid = mergedFileData.substr(startid, endid - startid).trim();
        let startbracket = mergedFileData.indexOf("{", i);

        // get the thing's length
        let thingdeclend = getEndOfThing(mergedFileData, startbracket+1, 1);

        // build the thing object
        let thing = { id: thingid, messages: [], ports: [] };

        // parse includes
        let includesstart = mergedFileData.indexOf("includes", i);
        if (includesstart !== -1) {
            if (includesstart < startbracket) {
                // this is the includes for the current thing, parse em
                let includessubstr = mergedFileData.substring(includesstart + 8, startbracket);
                // cut at annotation start if they exist
                let annotidx = includessubstr.indexOf("@");
                if (annotidx !== -1) {
                    includessubstr = includessubstr.substring(0, annotidx);
                }
                thing.includes = includessubstr.split(",").map(inclstr => inclstr.trim());
            }
        }

        // parse messages
        let messageIndicesForThing = getAllIndexes(mergedFileData, "message ", i).filter(messidx => messidx < thingdeclend);
        messageIndicesForThing.forEach(messidx => {
            let paramidx = mergedFileData.indexOf("(", messidx);
            let paramendidx = mergedFileData.indexOf(")", messidx);
            let name = mergedFileData.substring(messidx + 8, paramidx).trim();
            let params = mergedFileData.substring(paramidx + 1, paramendidx);
            let messageType = {
                type: name,
                params: params.split(",").map(ps => { let pss = ps.split(":"); return { name: pss[0].trim(), type: pss[1].trim() }; })
            };
            messageTypes.push(messageType);
            thing.messages.push(messageType);
        });

        // parse ports
        let portIndicesForThing = getAllIndexes(mergedFileData, " port ", i).filter(portidx => portidx < thingdeclend);
        portIndicesForThing.forEach(portidx => {
            let portsubstr = mergedFileData.substring(portidx, mergedFileData.indexOf("}", portidx)).trim();
            let namestartidx = portsubstr.indexOf(" ");
            let name = portsubstr.substring(namestartidx, portsubstr.indexOf(" ", namestartidx+1)).trim();

            let port = { name, sends: [], receives: [] };

            // get all send/receive block indexes for substrinings
            let indexofsends = getAllIndexes(portsubstr, "sends");
            let indexofreceives = getAllIndexes(portsubstr, "receives");
            indexofsends.push(...indexofreceives);
            indexofsends.sort();
            indexofsends.forEach((blockidx, index) => {
                // get the block itself
                let endblock;
                if (index === indexofsends.length - 1) {
                    endblock = portsubstr.length;
                } else {
                    endblock = indexofsends[index + 1];
                }
                let blocksubstr = portsubstr.substring(blockidx, endblock).trim();

                // split and find type
                let blocktokens = blocksubstr.split(" ");
                if (blocktokens[0] === "sends") {
                    port.sends.push(...blocktokens.slice(1).map(msg => msg.trim()));
                } else {
                    port.receives.push(...blocktokens.slice(1).map(msg => msg.trim()));
                }
            });

            thing.ports.push(port);
        });

        things[thing.id] = thing;
    });

    // propagate includes to make port type resolution easier
    for (let thingid in things) {
        let thing = things[thingid];
        if (thing.includes) {
            thing.includes = thing.includes.map(inclstr => things[inclstr]);
        }
    }

    // resolve message types
    for (let thingid in things) {
        let thing = things[thingid];
        thing.ports.forEach(port => {
            port.sends = port.sends.map(mess => {
                return findType(mess, thing);
            });
            port.receives = port.receives.map(mess => {
                return findType(mess, thing);
            });
        });
    }
    return things;
}

function findType(mess, thing) {
    let localmess = thing.messages.find(m => m.type === mess);
    if (localmess) {
        return localmess;
    } else {
        if (thing.includes) {
            // meh would rather it returns once it finds a result than this
            for (let inclidx in thing.includes) {
                let ret = findType(mess, thing.includes[inclidx]);
                if (ret) return ret;
            }
        } else {
            return null;
        }
    }
}


function getEndOfThing(data, index, startlevel) {
    let level = startlevel;
    let i = index;
    while (level > 0) {
        if (data[i] == "{") level++;
        if (data[i] == "}") level--;
        i++;
    }
    return i;
}

function callJavaServer(payload, callback) {
    let options = {
        hostname: 'localhost',
        port: 8007,
        path: '/toThingML',
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        }
    };
    let req = http.request(options, function (res) {
        // get data as its streamed by the server
        let buff = "";
        res.on("data", function (chunk) {
            buff += chunk;
        });

        // request finish start processing back
        res.on("end", function () {
            callback(buff);
        });
    });
    req.write(JSON.stringify(payload));
    req.end();
}
﻿const acm_metamodel = require("acm-metamodel");

const ACMComponent = acm_metamodel.ACMComponent;
const CommunicationComponent = acm_metamodel.CommunicationComponent;

// processing type registry
let ProcessingRegistry = require('./DynamicProcessingTypesRegistry');

const util = require('util');

var exports = module.exports = {};

exports.fromModel = fromModel;

function fromModel(model, callback) {
	console.log("to-nodered fromModel");
	let flow = model.models;
	let nodesList = flow.nodes;
	if (nodesList === undefined) {
		nodesList = flow;
	}
	delete flow.acm_model_type;

	let nodebuff = {};
	let lowx = {};
	let lowy = {};
	// process components
	model.components.forEach((component) => {
		if (component instanceof ACMComponent) {
            if (component.configured) {
                let newnode = component.buildNodeRedJson();
                nodebuff[component.id] = newnode;

                // replace node if already deployed previously
                let prevnodeidx = nodesList.findIndex(node => node.id === component.id);
                if (prevnodeidx !== -1) {
                    nodesList.splice(prevnodeidx, 1);
                }
            }
        } else if (component.configuration && component.configuration.generator/*component instanceof CommunicationComponent*/) {
            // communication components use the dynamic type rebuilder logic
            let newnode = ProcessingRegistry.generatorFunctions[component.configuration.generator](component);
            nodebuff[component.id] = newnode;

            // replace node if already deployed previously
            let prevnodeidx = nodesList.findIndex(node => node.id === component.id);
            if (prevnodeidx !== -1) {
                nodesList.splice(prevnodeidx, 1);
            }
        } else {
			// non metamodel node
			let rawnodeidx = nodesList.findIndex(node => node.id === component.id);
			if (rawnodeidx === -1) {
				console.log("ERROR: cannot find  node " + component.id + " in nodered model");
			} else {
				let rawnode = nodesList[rawnodeidx];

				// it is however needed to remove existing links
				rawnode.wires = [];
				// and update positions
				rawnode.x = component.x;
				rawnode.y = component.y;

				nodebuff[rawnode.id] = rawnode;

				// and delete that node from the list of non processed nodes
				nodesList.splice(rawnodeidx, 1);
			}
		}

		// store lowest coordinates for flow
		if (lowx[component.id_parent] == undefined) {
			lowx[component.id_parent] = component.x;
			lowy[component.id_parent] = component.y;
        }
		if (component.x > 0 && lowx[component.id_parent] > component.x) {
			lowx[component.id_parent] = component.x;
		}
		if (component.y > 0 && lowy[component.id_parent] > component.y) {
			lowy[component.id_parent] = component.y;
		}
	});

	// process links, that need to be added to the compoonents
	model.links.forEach((link) => { 
		// don't process virtual links at all
		if (!link.virtual) {
			// destination exists in nodered model
			if (nodebuff[link.from.id]) {
				// initialize the port if needed
				if (!nodebuff[link.from.id].wires[link.port]) { nodebuff[link.from.id].wires[link.port] = []; }

				if (link.to instanceof ACMComponent && !link.to.configured) {
					nodebuff[link.from.id].wires[link.port].push(model.links.find(lnk => lnk.from.id === link.to.id).to.id);
				} else {
					nodebuff[link.from.id].wires[link.port].push(link.to.id);
				}
			}
		} else {
			// virtual link, check if it's a link to a subflow node and fix it if that's the case
			if (link.to.id.startsWith("sf")) {
				// this will have to be improved really
				let sfid = link.to.id.split("_")[1];
				// initialize the port if needed
				if (!nodebuff[link.from.id].wires[link.port]) { nodebuff[link.from.id].wires[link.port] = []; }
				nodebuff[link.from.id].wires[link.port].push(sfid);
			} 
        }
	});

	// copy all the softcomps to flow
	flow.nodes = Object.keys(nodebuff).map(function (key) { return nodebuff[key]; });

	// reprocess components to have them moved out of negative coordinates
	flow.nodes.forEach(component => {
		// some components, like mqtt-broker (configuration) dont have x and y, dont add them
		// if components have x and y values they're deployed wrong
		if (component.x && component.y) {
			component.x -= (lowx[component.z] - 100);
			component.y -= (lowy[component.z] - 100);
		}
	});

	// remove empty wire arrays because that COMPLETELY breaks nodered deployment
	flow.nodes.forEach(cmp => {
		if (cmp.wires && cmp.wires.length == 0) delete cmp.wires;
		if (cmp.wires) {
			let before = cmp.wires.length;
			//cmp.wires = cmp.wires.filter(cw => cw);
			for (let i = 0; i < cmp.wires.length; i++) {
				if(!cmp.wires[i])
					cmp.wires[i] = []
			}
			let after = cmp.wires.length;
			if (before !== after) console.log("WARNING:: deleted null links for " + cmp.id);
		}
	});

    // update all acm sync nodes to use that mqtt broker
	let mqttneeded = false;
	let acmgens = false;
    flow.nodes.forEach(node => {
		if (node.type === "acm-sync") {
			acmgens = true;
            //mqttneeded = true;
            node.brokerAddress = "mqtt://172.17.0.1:1883";
		}
		else if (node.type === "acm-tag") {
			acmgens = true;
		}

    });
    if (mqttneeded) {
        // slap in an MQTT broker with a funky port
        let mqttbroker = { "id": "bf93afb6.0dbf6", "type": "mosca in", "z": "ef9a1111.8c6dd", "mqtt_port": "18883", "mqtt_ws_port": "", "name": "acm broker", "username": "", "password": "", "dburl": "", "x": 70, "y": 20, "wires": [[]] };
        flow.nodes.push(mqttbroker);
    }

	// process bullshit nodes with no z values like mqtt broker configs, that sort of garbage
	// they should be the only ones left in nodesList, append to processed nodes
	flow.nodes.push(...nodesList);

	//require("fs").writeFileSync("deploy_dump.json", JSON.stringify(flow));

	callback(flow, acmgens, mqttneeded);
}

function finalLog(o) {
	console.log(util.inspect(o, false, null, true));
}
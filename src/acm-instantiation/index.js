﻿var acm_metamodel = require("acm-metamodel");

var ACMComponent = acm_metamodel.ACMComponent;
var Link = acm_metamodel.Link;


const crypto = require('crypto');

var exports = module.exports = {};
exports.instantiateACM = instantiateACM;
exports.instantiateMonitor = instantiateMonitor;

function instantiateMonitor(component) {
	// architecturally correct but a bit useless
	return component;
}

function instantiateACM(model, component/*, isFirstCall=true*/) {
    // treat indirect conflicts (multiple acms)
	/*console.log("idconf" + component.id_conflict);
	if (isFirstCall && component.id_conflict) {
		var ret = {
			acm: [],
			components: [],
			links: []
		};

		model.components.forEach(function (cmp) {
			if (cmp.id_conflict === component.id_conflict) {
				var w = instantiateACM(model, cmp, false);
				ret.acm.push(...w.acm);
				ret.components.push(...w.components);
				ret.links.push(...w.links);
			}
		});

		return ret;
	} else {*/
	let taggingNodesToGenerate = [];
	// first pass: direct links
    model.links.forEach(function (link) {
		if (link.to.id === component.id) {
			if (link.from instanceof acm_metamodel.CommunicationComponent) {
				// link is from a comm comp, find where it leads to and add the tag there in front
				model.links.filter(lnk => lnk.to.id === link.from.id).map(lnk => model.links.filter(wlnk => wlnk.to.id === lnk.from.id)).forEach(lnkarr => {
					if (lnkarr.length !== 1) { console.log("acm-instant lnkarr.length " + lnkarr.length + " probably not gonna work properly"); }
					if (lnkarr.length < 1) return;
					lnkarr.forEach(lnk => {
						if (lnk.from) {
							let tagNodeData = {};
							let node = getNode(lnk.from.id, model.components);
							tagNodeData.id = component.id + "-tag-" + lnk.from.id;
							tagNodeData.x = node.x + 80;
							tagNodeData.y = node.y;
							tagNodeData.tag = lnk.from.id;

							tagNodeData.isRemote = true;
							tagNodeData.linkToBreak = lnk;
							tagNodeData.id_parent = node.id_parent;

							taggingNodesToGenerate.push(tagNodeData);
						}
					});
				});
			} else {
				let tagNodeData = {};
				tagNodeData.id = component.id + "-tag-" + link.from.id;

				// get coordinates of app node to place tagging node alongside
				let node = getNode(link.from.id, model.components);
				tagNodeData.x = node.x + 80;
				tagNodeData.y = node.y;
				tagNodeData.tag = link.from.id;
				taggingNodesToGenerate.push(tagNodeData);
			}
        }
	});

    // generate all the nodes
    return synthesizeACM(component, taggingNodesToGenerate);
    //}
}


function synthesizeACM(component, taggingNodesToGenerate) {
	var retnodes = [], retlinks = [];
	var strategy = { "taggingNodesToGenerate": taggingNodesToGenerate };
	/*console.log("inside synthesis"):
	//component is the casual node red json
	console.log("component");
	console.log(component);
	console.log("taggingNodesToGenerate");
	console.log(taggingNodesToGenerate);*/
	
	if (component.strategy.configuration != null) {
		strategy.syncPolicy = component.strategy.configuration.synchro;
	} else {
		strategy.syncPolicy = { "condition": "true", "delay": 500 };
	}

	/*console.log("strategy");
	console.log(strategy);*/

	// store links to break for tags
	// we do it here and pass it on to avoid messing with the model for the next component
	let linksToBreak = [];


	// generate the sync node
	let syncNode = new ACMComponent(component.id + "_sync", "ACM Sync"/* " + component.id*/, component.x - 80, component.y, acm_metamodel.ACMSyncType, component.id_parent, "sync", strategy, true, component.id_conflict);
	retnodes.push(syncNode);

	// generate the tagging nodes
	for (let nodeindex = 0; nodeindex < taggingNodesToGenerate.length; nodeindex++) {
		let tagData = taggingNodesToGenerate[nodeindex];
		let taggingNode = new ACMComponent(tagData.id, "ACM tag"/* " + taggingNodesToGenerate[nodeindex].tag*/, tagData.x, tagData.y, acm_metamodel.ACMTagType, component.id_parent, "tag", tagData.tag, true);
		// treat "remote" tag, ie. in front of a comm link
		if (tagData.isRemote) {
			// remote, change id_parent 
			taggingNode.id_parent = tagData.id_parent;
			// create new link from sync to comm
			let syncLink = new Link(taggingNode.id + ":" + component.id, "", 0, 0, taggingNode, tagData.linkToBreak.to);
			retlinks.push(syncLink);
			// change target of link and generate new one
			//tagData.linkToBreak.to = taggingNode;
			linksToBreak.push({ link: tagData.linkToBreak, target: taggingNode });
		} else {
			// not remote, add link from tag to sync
			let syncLink = new Link(taggingNode.id + ":" + component.id, "", 0, 0, taggingNode, syncNode);
			retlinks.push(syncLink);
        }

		retnodes.push(taggingNode);
	}

	// generate link from sync to ACM
	let acmLink = new Link(component.id + "_sync", "", 0, 0, syncNode, component);
	retlinks.push(acmLink);

	return {
		acm: component,
		components: retnodes,
		links: retlinks,
		linksToBreak: linksToBreak
	};
}

function getNode(nodeID, nodes) {
	for (var nidx in nodes) {
		if (nodes[nidx].id === nodeID) return nodes[nidx];
	}
}

function generateNRID() {
	return "ACM_" + generateID(8) + "." + generateID(6);
}

// generate hex id https://stackoverflow.com/a/27747377
// dec2hex :: Integer -> String
function dec2hex(dec) {
	return ('0' + dec.toString(16)).substr(-2);
}

// generateId :: Integer -> String
function generateID(len) {
	var arr = new Uint8Array((len || 40) / 2);
	crypto.randomFillSync(arr);
	return Array.from(arr, dec2hex).join('');
}

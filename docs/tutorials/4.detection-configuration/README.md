# How to change the default configuration of ACM 

In this example we will use ACM enabler on a local Node-RED flow to see how to change the configuration for conflict detection.

## Prerequisites

- Node-RED
- Running ACM instance, instructions on getting ACM up can be found [here](../../../README.md)

## Load the flow with Node-RED:

- Open Node-RED in your browser: http://localhost:1880

- Open the import from clipboard menu
![NodeRED menu](./images/nrmenu.jpg "NodeRED Menu")

- Load [flow.json](./flow.json) using the dialog, press import, click to place the nodes in a flow.
![NodeRED flow](./images/nrflow.jpg "NodeRED Flow")

This example contains 2 actuators that will interract through their environement. This interraction will cause an indirect conflict. 

## Load the flow in ACM for analysis

- Open the ACM interface in your browser: http://localhost:3333/

- Click "Load Model" in the menu at the top.

- Go back to Node-RED, select the flow tab and copy its URL
![NR Flow URL](./images/acmnrurl.jpg "NodeRED Flow URL")

- Paste the URL in the "Select application" dialog, select Node-RED as application type, and press Select
![ACM Select Application](./images/acmselectapp.jpg "ACM Select Application")

- The flow appears in the main ACM view, looking very much like the Node-RED flow 
![ACM Main View](./images/acmmainview.jpg "ACM Main View")
 
- Click on the "Actuator 1" node to bring up a dialog to attach it to a physical process

- In the drop down, select "Create new..." and press Go!

- Input a name for the physical process, and press Go!

- Repeat the same steps for "Actuator 2"

- The physical process will appear on the main view as a blue pentagon, and it will be linked to "Actuator 1" and "Actuator 2" .
![ACM Main View](./images/acmmainpp.jpg "ACM Main View PhyProc")


## Detect conflicts

- The next step is to click "Find conflicts" in the menu at the top. When ACM finishes processing the model, the view will be updated with several new nodes, marking actions the ACM could take.
![ACM Main View](./images/acmmainfind.jpg "ACM Main View Conflicts Identified")
    - a red ACM node before application 3

Note : By finding conflicts with the default configuration, an indirect conflict and a direct conflict are detected. In our context, the direct conflict seems irrelevant since it's only 2 "inject" nodes that are used to test the application. 
It is possible to just configure this ACM as "pass through" to get the correct behaviour, but it would place an ACM where there is not really a conflict. To avoid this situation, the configuraton of conflict detection needs to be edited

- click on clear ACMs on the top menu to remove all ACMs.

## Edit configuration for conflict detection

- click Edit rule application strategy
- click on the checkbox "Default"
![ACM edit configuration screen](./images/acmEditConfigurationMainScreen.jpg "ACM edit configuration screen")

the configuration file is made of 2 different part:
1. custom_rules :  they allow you to create rules from already existing ones. 
2. request : request corresponds to the order of application of the rules to detect conflicts.	

3 custom rules are already created : 
1. custom_physicalConflict : used to find indirect conflict 
2. custom_addACMForAction : used to find direct conflict on actuator
3. custom_addACMForSoft : used to find direct conflict on software component. 

the request specified by default is in 2 step:
1. apply rule custom_addACMForSoft 20 times if possible 
2. apply rule custom_physicalConflict 20 times if possible 

In this flow, direct conflicts aren't problematic so we can just remove custom_addACMForSoft of the request to focus only on indirect conflicts. 
![request modified](./images/requestmodified.jpg "request modified")

- after clicking "find conflicts" again, only one new ACM will be added for the indirect conflict.
![model conflictless after config](./images/flowAfterConfig.jpg "model conflictless after config")
un
## Configuring an actuation conflict manager

- In the main interface, click on the red ACM node
- A dialog opens with the available strategies from the palette
![ACM Palette](./images/acmpalette.jpg "ACM Palette")
- Select the desired one
- The ACM's icon changes to signal it's configured and ready to be deployed

## Deploying
- Select Deploy => Deploy to target in the menu at the top
- The updated flow will be deployed in Node-RED, and visible after refreshing the Node-RED UI page
![ACM Deployed](./images/acmdeployed.jpg "ACM Deployed")
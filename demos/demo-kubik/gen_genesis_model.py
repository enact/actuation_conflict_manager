import os
import re
import fileinput

for step_num in range (0, 2):
    step = "step" + str(step_num)
    filename_in  = "deployment-model-template-" + step + ".json"
    filename_tmp = "model-tmp.json"
    filename_out = "deployment-model-" + step + ".json"
    print(filename_out + ":");
    
    # First pass on the model-template file to add path_flow info in nr_flow (to be able to work on single line and not multiple lines)
    f_tmp = open(filename_tmp, 'w')
    with open(filename_in, 'r') as f_in:
        text = f_in.read()
        text = re.sub('^(.*)\"nr_flow\": \[\],\n(.*)\"path_flow\": \"(.+)\",\n', '\\1\"nr_flow\": [#_ENACT_#\\3],\n\\2\"path_flow\": \"\",\n', text, flags=re.MULTILINE)
        f_tmp.seek(0)
        f_tmp.write(text)
    f_tmp.close()
    
    # Second pass to remove filename from nr_flow and replace it by the file content
    f_out = open(filename_out, 'w')
    regex = re.compile("^(.*)\"nr_flow\": \[#_ENACT_#(.*)\],")
    with open(filename_tmp, 'r') as f_in:
        for line in f_in:
            match = regex.search(line)
            if (match == None):
                f_out.write(line)
            else:
                print(match.groups()[1])
                f_out.write('{}\"nr_flow\": \n'.format(match.groups()[0]))
                with open(match.groups()[1], 'r') as nodered_flow:
                    f_out.write(nodered_flow.read())
                f_out.write(',\n')
    f_out.close()
    
    f_tmp.close()
    os.remove(filename_tmp)

    # Third pass to replace IP addresses (change network)
    for line in fileinput.input(filename_out, inplace=True):
        print('{}'.format(line.replace("192.168.0.", "192.168.10.")), end='')
    fileinput.close()
   
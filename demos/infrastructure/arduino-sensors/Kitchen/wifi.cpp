#include "wifi.h"
#include "Arduino.h"
#include "watchdog.h"

#define RETRIES 5

#define SERIAL Serial
#define debug  SERIAL

#if defined(HAVE_HWSERIAL1)
#define WifiSerial Serial1
#elif defined(ARDUINO_SEEED_ZERO)
//the different board of samd have different serialx
#define WifiSerial Serial2   //serial number of seeeduino_zero (compatible with Wio Lite W600)
#elif defined(SEEED_XIAO_M0)
#define WifiSerial Serial1
#else
SoftwareSerial WifiSerial(2, 3);
#endif

#define FS(x) (__FlashStringHelper*)(x)

Wifi::Wifi() {
	_wifi.begin(WifiSerial, 9600);
}

void Wifi::configure() {
  for (int attempt = 0; attempt < RETRIES; attempt ++) {
    debug.print(F("Configuring wifi, attempt:")); 
	  debug.println(attempt + 1);
    _wifi.sendAT(F("AT+Z")); // wifi_reset
    delay(1500);
    if (_wifi.wifiSetMode(STA)) {
      debug.println(F("wifi configured"));
      delay(100);
      return;
    }
  }
  debug.println(F("wifi configuration failed"));
}

int Wifi::connectToAP(const char *wifi_ssid, const char *wifi_passwd) {
  bool ssid_set = false;
  bool psswd_set = false;
  bool joined = false;
  int attempt = 0;
  debug.println(F("setting ssid ..."));
  while (!ssid_set && attempt < RETRIES) {
    ssid_set = _wifi.wifiStaSetTargetApSsid(FS(wifi_ssid));
    delay(150);
  } if (!ssid_set) {
    debug.println(F("failed to set ssid"));
    return 0;
  }

  attempt = 0;
  debug.println(F("setting password"));
  while (!psswd_set && attempt < RETRIES) {
    psswd_set = _wifi.wifiStaSetTargetApPswd(FS(wifi_passwd));
    delay(150);
  } if (!psswd_set) {
    debug.println(F("failed to set password"));
    return 0;
  }

  attempt = 0;
  while (!joined && attempt < RETRIES) {
    joined = _wifi.sendAT(F("AT+WJOIN")); //join network
    delay(1500);
  } if (!joined) {
    debug.println(F("failed to join network"));
    return 0;
  }

  debug.println(F("connected to AP"));
  return 1;
}

int Wifi::createSocket(const char*target_ip, uint16_t target_port, uint16_t local_port) {
  int socket = -1;
  for (int attempt = 0; attempt < RETRIES; attempt ++) {
    debug.print(F("Creating socket to remote server, attempt:")); debug.println(attempt + 1);
    socket = _wifi.wifiCreateSocketSTA(TCP, Client, target_ip, target_port, local_port);
    if (socket >= 0) {
      debug.print(F("connected to remote server. Socket=")); debug.println(socket);
      delay(400);
      return socket;
    }
    delay(1000);
  }
  debug.println(F("failed to connect to remote server"));
  return socket;
}


bool Wifi::send(int socket, char *char_val) {
  static int send_id = 0, last_send_id = 0;
  bool sent ;

  send_id++; // store the call number

  sent = _wifi.httpPost(
    socket,
    F("POST /seeeduino_nano_1 HTTP/1.1\n"),
    F("Host: 192.168.0.27\n"),
    F("User-Agent: seeeduino\n"),
    F("Content-Type: application/x-www-form-urlencoded\n"),
    F("Accept: */*\n"),
    char_val
  );
  if (sent) {
    last_send_id = send_id; // store the last send id
  }
  if (send_id > last_send_id + 5) { // if more than 5 consecutive messages not sent
    debug.println("#### More than 5 consecutoive message not sent, reboot");
    reboot();
  }
}

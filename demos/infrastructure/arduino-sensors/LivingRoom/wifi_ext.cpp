
#include "wifi_ext.h"

static char value[32]; 
static char char_val [8];

WifiExt::WifiExt() : Wifi() {
}

void WifiExt::send(int socket, char *name_val, int int_val) {
  snprintf(value, sizeof(value), "%s=%i\n\n", name_val, int_val);
  Wifi::send(socket, value);
}

void WifiExt::send(int socket, char *name_val, float float_val) {
  dtostrf(float_val, 6, 2, char_val);
  snprintf(value, sizeof(value), "%s=%s\n\n", name_val, char_val);
  Wifi::send(socket, value);
}

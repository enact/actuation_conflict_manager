#ifndef __WIFI_EXT_H__
#define __WIFI_EXT_H__

#include "wifi.h"

class WifiExt : public Wifi {
  public:
    WifiExt();
    void send(int socket, char *name_val, int int_val);
    void send(int socket, char *name_val, float float_val);
};
#endif
/*******************************************************************************
 * Copyright (c) 2009,2011 Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Raul Otaolea (Tecnalia) - initial API, implementation and documentation
 *    Fran Ruiz (Tecnalia) - initial API, implementation and documentation
 *******************************************************************************/

package org.smool.kpi.model.smart.encoding.m3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.smool.kpi.common.ClasspathManager;
import org.smool.kpi.common.Logger;
import org.smool.kpi.common.rdfm3.M3Parser;
import org.smool.kpi.common.rdfm3.RDFM3;
import org.smool.kpi.common.rdfm3.RDFM3Triple;
import org.smool.kpi.model.exception.KPIModelException;
import org.smool.kpi.model.exception.RDFM3ParseException;
import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.IAbstractOntConcept;
import org.smool.kpi.model.smart.SmartModel;
import org.smool.kpi.model.smart.encoding.AbstractQueryProcessor;
import org.smool.kpi.model.smart.encoding.m3.util.IndividualRelationship;
import org.smool.kpi.model.smart.encoding.m3.util.Notification;
import org.smool.kpi.model.smart.encoding.m3.util.ResultsData;
import org.smool.kpi.model.smart.encoding.m3.util.ResultsProperty;
import org.smool.kpi.model.smart.slots.FunctionalDatatypeSlot;
import org.smool.kpi.model.smart.slots.FunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalDatatypeSlot;
import org.smool.kpi.model.smart.slots.NonFunctionalObjectSlot;
import org.smool.kpi.model.smart.slots.Slot;
import org.smool.kpi.model.smart.subscription.ISubscription;
import org.smool.kpi.ssap.ComposedPendingTask;
import org.smool.kpi.ssap.PendingTask;
import org.smool.kpi.ssap.exception.KPISSAPException;
import org.smool.kpi.ssap.message.parameter.SSAPMessageRDFParameter.TypeAttribute;
import org.smool.kpi.ssap.message.parameter.SSAPMessageStatusParameter.SIBStatus;
import java.util.*;

/**
 * The RDFM3QueryProcessor class is the class that is responsible for querying
 * and inquiring using the RDF-M3 query language as a reference
 * 
 * @author Fran Ruiz, fran.ruiz@tecnalia.com, Tecnalia
 * @author Raul Otaolea, raul.otaolea@tecnalia.com, Tecnalia
 *
 */
public class RDFM3QueryProcessor extends AbstractQueryProcessor {

	/** Derived subscriptions */
	private Map<Long, AbstractOntConcept> derivedSubscriptions;

	/** Subscription individuals */
	private ConcurrentHashMap<Long, ArrayList<AbstractOntConcept>> subscriptionIndividuals;

	/**
	 * Subscription hierarchy relates a subscription with the parent subscription
	 */
	private ConcurrentHashMap<Long, Long> subscriptionHierarchy;

	/** The list of relationships between individuals */
	private ArrayList<IndividualRelationship> individualRelationships;

	private ArrayList<PropertyCharacteristic> propertiesCharacteristics;

	private ConcurrentHashMap<Long, AbstractOntConcept> individualSubscription;

	/** The rdf:type IRI */
	private static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

	/** For notification sending */
	private ConcurrentHashMap<Long, Notification> notifications;

	/**
	 * The constructor
	 * 
	 * @param model   the smart model
	 * @param timeout the timeout for waiting a response from the sib
	 */
	public RDFM3QueryProcessor(SmartModel model) {
		super(model);
		derivedSubscriptions = Collections.synchronizedMap(new HashMap<Long, AbstractOntConcept>());
		subscriptionIndividuals = new ConcurrentHashMap<Long, ArrayList<AbstractOntConcept>>();
		subscriptionHierarchy = new ConcurrentHashMap<Long, Long>();
		individualRelationships = new ArrayList<IndividualRelationship>();
		propertiesCharacteristics = new ArrayList<PropertyCharacteristic>();
		notifications = new ConcurrentHashMap<Long, Notification>();
		individualSubscription = new ConcurrentHashMap<Long, AbstractOntConcept>();
	}

	/**
	 * Queries about a given concept
	 * 
	 * @param concept the concept to be queried about
	 * @throws KPIModelException when cannot query about a given concept
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <C extends IAbstractOntConcept> List<C> query(Class<C> concept) throws KPIModelException {

		/** The return instances */
		ArrayList<AbstractOntConcept> individuals = new ArrayList<AbstractOntConcept>();

		try {
			/** Creates a new query based on the input param */
			String conceptIRI = getConceptIRI(concept);

			// Get all individuals that are subclassesOf the given AOC
			// query = [*, rdfs:subClassOf, conceptClassId]
			String query = this.getSubClassOfQuery(conceptIRI).toString();

			PendingTask pt = model.getSIB().query(query, TypeAttribute.RDFM3);

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());

			if (!pt.isFinished()) { // the timeout has expired
				throw new KPIModelException("The query finished by timeout");
			}

			// Gets the status of the response
			SIBStatus status = (SIBStatus) pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The query cannot be executed due to " + status.getValue());
			}

			// Gets the results parameter from the QUERY RESPONSE
			String results = (String) pt.getProperty("results");

			// Here is is stored the ontology class set to be queried over individuals
			ArrayList<String> ontClassSet = new ArrayList<String>();

			if ((results != null) && !results.equals("")) {
				// Parses the results
				M3Parser parser = new M3Parser();
				RDFM3 m3 = parser.parse(results);

				// In our case, it is the subject the one that has the subclasses uris
				for (RDFM3Triple triple : m3.listTriples()) {
					// Creates the individual query for each of the subclasses of the given class
					String classURL = triple.getSubject();
					if (model.getConceptName(classURL) != null) {
						ontClassSet.add(triple.getSubject());
					}
				}
			}

			try {
				if (!ontClassSet.contains(conceptIRI)) {
					ontClassSet.add(conceptIRI);
				}

				// Obtain all the individuals (perform a query) and create the corresponding
				// AbstractOntConcepts
				individuals.addAll(getIndividuals(ontClassSet));
			} catch (NullPointerException ex) {
				// FIXME You should REALLY know what you are doing if you swallow an exception
				// -- hartmut.benz 20110721
				// do nothing
			}

		} catch (KPIModelException ex) {
			ex.printStackTrace();
			// FIXME Why not just re-throw the exception (or not catching it her at all and
			// let it go higher).
			// FIXME The only thing you are achieving is 'loosing' theSIBStatus that may be
			// in ex
			// FIXKE If that is the intent, document it
			throw new KPIModelException(ex.getMessage(), ex.getCause());
		} catch (Exception e) {
			e.printStackTrace(); // FIXME better: remove printStackTrace; and: new KPIModelException("Error doing
									// Whatever.", e);
			throw new KPIModelException(e.getMessage(), e.getCause());
		}

		try {
			if (individuals.size() > 0) {
				for (AbstractOntConcept individual : individuals) {
					// Fill with data
					populateIndividual(individual);
					individual._setPublished();
//					individual._setModel(model);
//					individual._setPublished(true);
//					model.addPublished(individual);
				}
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot perform population of properties in individuals", ex.getCause());
			// FIXME see FIXME with previous try
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Error in filling concepts", ex.getCause());
			// FIXME see FIXME with previous try
		}

		return (List<C>) individuals;
	}

	/**
	 * Queries about a given concept
	 * 
	 * @param concept the concept to be queried about
	 * @throws KPIModelException when cannot query about a given concept
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <C extends IAbstractOntConcept> C query(Class<C> concept, String individualID) throws KPIModelException {

		try {
			/** Creates a new AbstractOntConcept based on the input param */
			AbstractOntConcept individual = getConceptImpl(concept);
			individual._setIndividualID(individualID);

			// Fill with data
			populateIndividual(individual);
			individual._setPublished();
//			individual._setModel(model);
//			individual._setPublished(true);
//			model.addPublished(individual);
			return (C) individual;
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot perform population of properties in individual", ex.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Error in filling individual", ex.getCause());
		}
	}

	/**
	 * Gets the Abstract Ontology Concept IRI, as defined in the ontology
	 * 
	 * @param concept the concept class
	 * @return the IRI associated in the ontology to that class definition
	 * @throws KPIModelException if something strange occurs
	 */
	public static String getConceptIRI(Class<? extends IAbstractOntConcept> concept) throws KPIModelException {
		try {
			return FieldUtils.readStaticField(concept, "CLASS_IRI").toString();
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}
		/** Creates a new query based on the input param */
		AbstractOntConcept aoc;
		try {
			aoc = (AbstractOntConcept) concept.newInstance();
			return aoc._getClassIRI();
		} catch (InstantiationException e) {
			throw new KPIModelException(
					"Internal error - failed to access field CLASS_IRI of: " + concept.getCanonicalName(), e);
		} catch (IllegalAccessException e) {
			throw new KPIModelException(
					"Internal error - failed to access field CLASS_IRI of: " + concept.getCanonicalName(), e);
		}
	}

	/**
	 * Gets the concept implementation
	 * 
	 * @param concept the concept class
	 * @return an AbstractOntConcept instance
	 * @throws KPIModelException if it is not working
	 */
	public AbstractOntConcept getConceptImpl(Class<? extends IAbstractOntConcept> concept) throws KPIModelException {
		try {
			AbstractOntConcept individual;
			if (concept.isInterface()) {
				final String conceptIRI = getConceptIRI(concept);
				final String conceptImplClassName = model.getConceptName(conceptIRI);

				if (conceptImplClassName != null) {
					individual = (AbstractOntConcept) Class
							.forName(conceptImplClassName, true, ClasspathManager.getInstance().getClassLoader())
							.newInstance();
				} else {
					throw new KPIModelException("IRI undefined in current KP: " + conceptIRI);
				}
			} else {
				individual = (AbstractOntConcept) concept.newInstance();
			}

			return individual;
		} catch (ClassNotFoundException e) {
			throw new KPIModelException("Failed to create instance for " + concept);
		} catch (InstantiationException e) {
			throw new KPIModelException("Failed to create instance for " + concept);
		} catch (IllegalAccessException e) {
			throw new KPIModelException("Failed to create instance for " + concept);
		}
	}

	/**
	 * Subscribes to a given class
	 * 
	 * @param concept the AbstractOntConcept class to subscribe
	 * @param as      the AbstractSubscription that receives the Indications
	 * @throws KPIModelException when cannot subscribe to a given concept
	 */
	@Override
	public <C extends IAbstractOntConcept> void subscribe(Class<C> concept, ISubscription<? super C> as)
			throws KPIModelException {
		try {
			String conceptIRI = getConceptIRI(concept);

			String query = this.getSubClassOfQuery(conceptIRI).toString();
			PendingTask pt = model.getSIB().query(query, TypeAttribute.RDFM3);

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());

			if (!pt.isFinished()) { // the timeout has expired
				throw new KPIModelException("The query finished by timeout");
			}

			// Gets the status of the response
			SIBStatus status = (SIBStatus) pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The query cannot be executed due to " + status.getValue());
			}

			// Gets the results parameter from the QUERY RESPONSE
			String results = (String) pt.getProperty("results");

			// Here is is stored the ontology class set to be queried over individuals
			ArrayList<String> ontClassSet = new ArrayList<String>();

			if ((results != null) && !results.equals("")) {
				// Parses the results
				M3Parser parser = new M3Parser();
				RDFM3 m3 = parser.parse(results);

				// In our case, it is the subject the one that has the subclasses uris
				for (RDFM3Triple triple : m3.listTriples()) {
					// Creates the individual query for each of the subclasses of the given class
					String classURL = triple.getSubject();
					if (model.getConceptName(classURL) != null) {
						ontClassSet.add(triple.getSubject());
					}
				}
			}

			try {
				if (!ontClassSet.contains(conceptIRI)) {
					ontClassSet.add(conceptIRI);
				}

				// Subscribe to the set of classes
				subscribeToClasses(ontClassSet, as);
			} catch (NullPointerException ex) {
				// do nothing
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw new KPIModelException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * Subscribes to a given individual
	 * 
	 * @param concept the AbstractOntConcept class to subscribe
	 * @param
	 * @param as      the AbstractSubscription that receives the Indications
	 * @throws KPIModelException when cannot subscribe to a given concept
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <C extends IAbstractOntConcept> void subscribe(Class<C> concept, String individualID,
			ISubscription<? super C> as) throws KPIModelException {
		try {
			/** Creates a new query based on the input param */
			AbstractOntConcept individual = getConceptImpl(concept);
			individual._setIndividualID(individualID);

			Long subscriptionId = this.subscribeToIndividual(individual);

			if (subscriptionId != null) {
				subscriptions.put(subscriptionId, (ISubscription<AbstractOntConcept>) as);
				individualSubscription.put(subscriptionId, individual);
			}

		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw new KPIModelException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * Unsubscribes from the concepts associated to a given AbstractSubscription
	 * 
	 * @param as the AbstractSubscription to what unsubscribe
	 * @return <code>true</code> if unsubscribe was correctly made
	 * @throws KPIModelException
	 */
	@Override
	public <C extends IAbstractOntConcept> boolean unsubscribe(ISubscription<C> as) throws KPIModelException {
		try {

			// get the subscriptionId associated to the abstract subscription
			long subscription = -1;
			// Obtain the subscription corresponding to the abstract subscription
			if (subscriptions.containsValue(as)) {
				for (long subscriptionId : subscriptions.keySet()) {
					if (subscriptions.get(subscriptionId).equals(as)) {
						subscription = subscriptionId;
					}
				}
			}

			if (subscription != -1) {
				individualSubscription.remove(subscription);
			}

			return super.unsubscribe(as);
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(ex.getMessage(), ex.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			throw new KPIModelException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * Gets the data for a given indication received
	 * 
	 * @param subscriptionId  the subscription identifier associated with the
	 *                        received indication
	 * @param newResults      the new results received
	 * @param obsoleteResults the obsolete results received
	 * @throws KPIModelException when cannot process received data
	 */
	@Override
	public void indicationReceived(final long subscriptionId, final String newResults, final String obsoleteResults)
			throws KPISSAPException {
		Logger.debug("Adding indication [" + subscriptionId + "]:\n");
		synchronized (lock) {
			pendingRequests++;
		}

		indicationQueue.submit(new Runnable() {
			public void run() {
				synchronized (model.getLock()) {
					try {
						Logger.debug("Processing received indication [" + subscriptionId + "]:\n");
						if (hasContentRDFM3(newResults)) {
							Logger.debug("New Results\n===========\n" + newResults + "\n");
						}
						if (hasContentRDFM3(obsoleteResults)) {
							Logger.debug("Obsolete Results\n================\n" + obsoleteResults + "\n");
						}

						final ISubscription<AbstractOntConcept> dirSubscription = subscriptions.get(subscriptionId);
						if (dirSubscription != null) {
							final AbstractOntConcept individual = individualSubscription.get(subscriptionId);
							if (individual == null) { /** DIRECT SUBSCRIPTION */
								// When an indication is received and refers to a direct subscription means that
								// a direct individual has been added or removed.
								// Direct subscriptions pattern is [*, rdf:type, SubscribedClass]
								Logger.debug("Direct subscription received");
								notifyDirectSubscription(subscriptionId, newResults, obsoleteResults);

							} else { /** DIRECT INDIVIDUAL SUBSCRIPTION */
								// When an indication is received and refers to a direct subscription means that
								// an individual has been modified.
								// Direct subscriptions pattern is [individualId, property, value]
								Logger.debug("Direct individual subscription received");

								AbstractOntConcept obsoleteIndividual = (AbstractOntConcept) individual.clone();

								// The list of subscription data
								ArrayList<ResultsData> indicationData = new ArrayList<ResultsData>();

								// Generates an structure based on RDF-M3 format with new and obsolete data
								indicationData.addAll(importResults(newResults, obsoleteResults));

								// Update cache of AOC that has the slot to be removed
								individual.setLastChangesCache(indicationData);

								// now find the corresponding concept(s)
								for (ResultsData data : indicationData) {
									udpateIndividual(individual, data, subscriptionId);
								}

								individualSubscription.put(subscriptionId, individual);
								model.addPublished(individual);

								// Set as concept added
								dirSubscription.conceptUpdated(individual, obsoleteIndividual);

								// for testing purposes
								model.replaceIndividualInstance(individual);

							}
						} else { // this should correspond to an indirect subscription
							/** INDIRECT SUBSCRIPTION */
							// When an indication is received and refers to an indirect subscription means
							// that
							// some property has been added, removed or updated from a given individual
							// Indirect subscriptions pattern is [Individual, *, *]
							Logger.debug("Indirect subscription received");
							notifyDerivedSubscription(subscriptionId, newResults, obsoleteResults);
						}
					} catch (KPIModelException e) {
						Logger.error("Error processing RDFM3 indication", e);
					} catch (CloneNotSupportedException e) {
						Logger.error("Error cloning updated individual in RDFM3 indication", e);
					}
				}
			}
		});
	}

	/**
	 * Notifies a direct subscription When an indication is received and refers to a
	 * direct subscription means that a direct individual has been added or removed.
	 * Direct subscriptions pattern is [*, rdf:type, SubscribedClass]
	 * 
	 * @param subscriptionId  the subscription identifier
	 * @param newResults      a string representing the new results in RDF-M3 format
	 * @param obsoleteResults a string representing the obsolete results in RDF-M3
	 *                        format
	 * @throws KPIModelException when cannot notify a direct subscription
	 */

	private <C extends AbstractOntConcept> void notifyDirectSubscription(long subscriptionId, String newResults,
			String obsoleteResults) throws KPIModelException {

		/**
		 * A subscription can refer both to a class subscription and to a individual
		 * subscription
		 */

		try {
			if (hasContentRDFM3(newResults)) {
				// Direct subscriptions has the following format
				// [NewInstance rdf:type AOC]
				ArrayList<AbstractOntConcept> individuals = new ArrayList<AbstractOntConcept>();
				individuals.addAll(createEmptyIndividuals(newResults));
				// individuals.addAll(createEmptyIndividuals(newResults)); ARF 19/05/2014 remove
				// duplicated line to prevent subscription error

				for (AbstractOntConcept individual : individuals) {
					// if new individual appeared, subscribe to each of the individual
					subscribeToIndividual(individual);

					// Save individuals
					for (AbstractOntConcept aoc : subscriptionIndividuals.get(subscriptionId)) {
						if (aoc.equals(individual)) {
							throw new KPIModelException("Received indication individual "
									+ individual._getIndividualIRI() + " was already in the model.");
						}
					}

					if (individual != null) {
						subscriptionIndividuals.get(subscriptionId).add(individual);
						individual._setPublished();
//						individual._setModel(model);
//						individual._setPublished(true);
						model.addPublished(individual);
						// Set as concept added
						if (subscriptions.get(subscriptionId) != null) {
							subscriptions.get(subscriptionId).conceptAdded(individual);
						}
					} else {
						throw new KPIModelException(
								"Cannot create individual because it does not exists in the KP subscribed concepts for subscription "
										+ subscriptionId);
					}
				}
			} else if (hasContentRDFM3(obsoleteResults)) { // concept removed
				// Parses the results
				M3Parser parser = new M3Parser();
				RDFM3 m3 = parser.parse(obsoleteResults);

				for (RDFM3Triple triple : m3.listTriples()) {
					// gets the triples associated, where the subject corresponds to the instance
					// reference
					// and the object corresponds to the class reference
					String conceptName = model.getConceptName(triple.getObject());

					if (conceptName != null) {
						AbstractOntConcept individual = getSubscriptionIndividual(subscriptionId, triple.getSubject());

						if (individual != null) {
							// Removes the corresponding derived subscriptions to direct and inherited
							// object properties
							removeDerivedSubscriptions(individual, subscriptionId);

							// The model is set to null
//							individual._setModel(null);
							individual._setUnpublished();

							// Set as concept removed
							subscriptions.get(subscriptionId).conceptRemoved(individual);
						} else {
							throw new KPIModelException(
									"Individual has been removed, so cannot inform to the user that the concept "
											+ triple.getSubject() + "was removed");
						}
					}
				}
			} // concept updated is not possible for RDF-M3, because it references to
				// individual iris

		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot subscribe to individual", ex);
		} catch (RDFM3ParseException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot parse received data in direct subscription", ex);
		} finally {
			synchronized (lock) {
				pendingRequests--;
			}
		}
	}

	/**
	 * Notifies an indirect subscription When an indication is received and refers
	 * to an indirect subscription means that some property has been added, removed
	 * or updated from a given individual Indirect subscriptions pattern is
	 * [Individual, *, *]
	 * 
	 * @param subscriptionId  the subscription identifier. this subscription
	 *                        corresponds to an indirect subscription
	 * @param newResults      a string representing the new results in RDF-M3 format
	 * @param obsoleteResults a string representing the obsolete results in RDF-M3
	 *                        format
	 * @throws KPIModelException when cannot notify a direct subscription
	 */
	private void notifyDerivedSubscription(long subscriptionId, String newResults, String obsoleteResults)
			throws KPIModelException {
		try {
			if (derivedSubscriptions.get(subscriptionId) == null) {
				Logger.debug("The received subscription does not longer exist to the list of internal subscriptions:"
						+ subscriptionId + "\n[N]:" + newResults + "\n[O]:" + obsoleteResults);
				return;
			}

			// The list of subscription data
			ArrayList<ResultsData> indicationData = new ArrayList<ResultsData>();

			// Generates an structure based on RDF-M3 format with new and obsolete data
			indicationData.addAll(importResults(newResults, obsoleteResults));

			// now find the corresponding concept(s)
			for (ResultsData data : indicationData) {
				// check if it has relation with another root subscription
				if (subscriptionHierarchy.get(subscriptionId) != null) {
					Logger.debug("A subscription in the subscription tree indication received");
					// if not, it has relation with another root subscription

					// 1. Gets the root individual
					// The root individual is the one that we have associated to a direct
					// subscription
					AbstractOntConcept obsoleteIndividual = getRootIndividual(subscriptionId);

					if (obsoleteIndividual == null) {
						Logger.debug("Indication does no longer exist in current KP");
						return;
					}

					// 2. Get all the subscription ids where the individual is present
					// This allows us to get the ISubscription listener to be notified
					ArrayList<Long> classSubscriptionIds = this.getRelatedSubscriptionIds(obsoleteIndividual);

					// 3. Clone the individual
					// This allows us to have two AbstractOntConcepts: the updated one and the old
					// one.
					AbstractOntConcept newIndividual = getLatestIndividual(obsoleteIndividual);

					// 4. Update the new individual with received data of the SIB
					udpateIndividual(newIndividual, data, subscriptionId);

					// 5. Store the individual for notification
					for (Long classSubscriptionId : classSubscriptionIds) {
						if (classSubscriptionId != null) {
							notifications.put(classSubscriptionId, new Notification(newIndividual, obsoleteIndividual));
						}
					}

					// 6. Update cache
					newIndividual.setLastChangesCache(indicationData);

				} else {
					Logger.debug("An individual subscription indication has been received");

					// we have received an indication from a subscribed individual

					// 1. Get the subscription ids
					ArrayList<Long> classSubscriptionIds = this.getRelatedSubscriptionIds(data.getIndividualIRI());

					if (classSubscriptionIds.size() == 0) {
						Logger.debug("Received indication for individual " + data.getIndividualIRI()
								+ " is not an indirect subscription");
						return;
					}

					// 2. Get individual reference
					//AbstractOntConcept obsoleteIndividual = getIndividual(classSubscriptionIds,data.getIndividualIRI());
					
					//ARF 07-08-2020: sometimes obsoleteindividual was not correct, raising errors later in subscriptions. Tos solve it , fiirst we take the  individual from the subscriptions and also we check if the obsolete is thee same id as the expected in the subscription id
					AbstractOntConcept obsoleteIndividual = getIndividual2(classSubscriptionIds, data.getIndividualIRI());
					preventOverrideIndividual(obsoleteIndividual, subscriptionId);

					if (obsoleteIndividual == null) {
						throw new KPIModelException("Individual with IRI " + data.getIndividualIRI()
								+ " is not part of the current subscriptions");
					}

					if (!data.isRemovableIndividual()) { // this belongs to a class removal
						// 3. Clone the obsolete individual
						// This allows us to have two AbstractOntConcepts: the updated one and the old
						// one.
						AbstractOntConcept newIndividual = (AbstractOntConcept) obsoleteIndividual.clone();

						// 4. Update the new individual with received data of the SIB
						udpateIndividual(newIndividual, data, subscriptionId);

						// TODO check after
						model.addPublished(newIndividual);

						// 5. Store the individual for notification
						for (Long classSubscriptionId : classSubscriptionIds) {
							if (classSubscriptionId != null) {
								// FIXME WARNING potential error: both branches of the if are identical except
								// for the log statement - hartmut.benz
								if (notifications.get(classSubscriptionId) != null) {
									Logger.debug("Updates the notification");
									notifications.put(classSubscriptionId,
											new Notification(newIndividual, obsoleteIndividual));
								} else {
									Logger.debug("Creates a new notification");
									notifications.put(classSubscriptionId,
											new Notification(newIndividual, obsoleteIndividual));
								}
							}
						}

						// 6. Update cache
						newIndividual.setLastChangesCache(indicationData);
					}
				}
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot subscribe to individual", ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot parse received data in indirect subscription", ex);
		} finally {
			synchronized (lock) {
				if (--pendingRequests > 0) {
					Logger.debug("Pending request in the queue (" + pendingRequests + ")...");
				} else {
					// notify changes in subscriptions
					for (Long notifySubscriptionId : notifications.keySet()) {
						Notification notification = notifications.get(notifySubscriptionId);
						Logger.debug("Sending notification...");

						if (notifySubscriptionId != null) {
							if ((notification.getNewIndividual() != null)
									&& (notification.getOldIndividual() != null)) { // concept updated
								Logger.debug("1. Notifying an update for subscription " + notifySubscriptionId);
								notification.getNewIndividual()._setPublished();
								model.addPublished(notification.getNewIndividual());
								subscriptions.get(notifySubscriptionId).conceptUpdated(notification.getNewIndividual(),
										notification.getOldIndividual());
								if (subscriptionIndividuals.get(notifySubscriptionId) != null) {
									subscriptionIndividuals.get(notifySubscriptionId)
											.remove(notification.getOldIndividual());
									subscriptionIndividuals.get(notifySubscriptionId)
											.add(notification.getNewIndividual());
								}

								if (individualSubscription.get(notifySubscriptionId) != null) {
									individualSubscription.put(notifySubscriptionId, notification.getNewIndividual());
								}
							} else if ((notification.getNewIndividual() != null)
									&& (notification.getOldIndividual() == null)) { // added new individual
								Logger.debug("2. Notifying an addition for subscription " + notifySubscriptionId);
								notification.getNewIndividual()._setPublished();
								model.addPublished(notification.getNewIndividual());
								subscriptions.get(notifySubscriptionId).conceptAdded(notification.getNewIndividual());
								subscriptionIndividuals.get(notifySubscriptionId).add(notification.getNewIndividual());
							} else if (notification.getOldIndividual() != null) { // removed individual
								Logger.debug("3. Notifying a removal for subscription " + notifySubscriptionId);
								notification.getOldIndividual()._setUnpublished();
								// model.removePublished(notification.getOldIndividual());
								subscriptions.get(notifySubscriptionId).conceptRemoved(notification.getOldIndividual());
								subscriptionIndividuals.get(notifySubscriptionId)
										.remove(notification.getOldIndividual());
								removeDerivedSubscriptions(notification.getOldIndividual(), null);
							}
						}
					}

					notifications.clear();
				}
			}
		}
	}

	/**
	 * Gets an individual from a subscription individual list
	 * 
	 * @param subscriptionIds the list of subscription ids
	 * @param individualIRI   the individual iri to find
	 * @return the AbstractOntConcept reference
	 * @deprecated ARF TODO do not use this method because it has errors when retrieving the individual sometimes when the subid is already there but the returned individual by using notifications object is NOT the right one
	 */
	private AbstractOntConcept getIndividual(ArrayList<Long> subscriptionIds, String individualIRI) {
		for (Long subcriptionId : subscriptionIds) {
			if (notifications.get(subcriptionId) != null) {
				if (notifications.get(subcriptionId).getNewIndividual() != null) {
					return notifications.get(subcriptionId).getNewIndividual();
				} else {
					return notifications.get(subcriptionId).getOldIndividual();
				}
			} else {
				for (AbstractOntConcept individual : subscriptionIndividuals.get(subcriptionId)) {
					if (individual._getIndividualIRI().equals(individualIRI)) {
						return individual;
					}
				}
			}
		}
		return null;

	}
	
	private AbstractOntConcept getIndividual2(ArrayList<Long> subscriptionIds, String individualIRI) {
		System.out.println("INDIVIDUAL2");
		for (Long subcriptionId : subscriptionIds) {
			for (AbstractOntConcept individual : subscriptionIndividuals.get(subcriptionId)) {
				if (individual._getIndividualIRI().equals(individualIRI)) {
					return individual;
				}
			}
		}
		return null;
	}
	
	

	/**
	 * Creates a set of empty individuals (without values in properties) for the
	 * given RDF-M3 triples
	 * 
	 * @param triples a String representing one or more triple set in RDF-M3 format,
	 *                standing the following triple format [IndividualIRI, rdf:type,
	 *                AOCIRI]
	 * @return a List of empty individuals
	 * @throws KPIModelException when cannot parse incoming data or create the
	 *                           individuals
	 */
	@SuppressWarnings("unchecked")
	private <C extends AbstractOntConcept> List<C> createEmptyIndividuals(String triples) throws KPIModelException {
		try {
			ArrayList<C> emptyIndividuals = new ArrayList<C>();

			// Parses the results
			M3Parser parser = new M3Parser();
			RDFM3 m3 = parser.parse(triples);

			for (RDFM3Triple triple : m3.listTriples()) {
				// gets the triples associated, where the subject corresponds to the instance
				// reference
				// and the object corresponds to the class reference
				String aocIRI = triple.getObject();
				String individualIRI = triple.getSubject();
				String conceptName = model.getConceptName(aocIRI);

				if (conceptName != null) {
					Object obj = Class.forName(conceptName, true, ClasspathManager.getInstance().getClassLoader())
							.newInstance(); // gets the instance
					C individual = (C) obj; // performs the casting
					individual._setIndividualIRI(individualIRI); // assigns the individual id

					// adds the empty individual to the list of return individuals
					emptyIndividuals.add(individual);
				} else {
					throw new KPIModelException("IRI undefined in current KP: " + aocIRI);
				}
			}

			return emptyIndividuals;
		} catch (Exception ex) {
			throw new KPIModelException("cannot create empty individuals", ex);
		}
	}

	/**
	 * Finds the parent Individual that is associated with modification received
	 * from an indication
	 * 
	 * @param derivedSubscriptionId the derived subscription id
	 * @return the root individual to what is related the derived subscription
	 */
	private AbstractOntConcept getRootIndividual(long derivedSubscriptionId) {
		Long rootSubscriptionId = derivedSubscriptionId;
		while (subscriptionHierarchy.get(rootSubscriptionId) != null) {
			rootSubscriptionId = subscriptionHierarchy.get(rootSubscriptionId);
		}
		// now we have the root subscription id for a given derivedSubscription

		if (rootSubscriptionId != null) {
			if (notifications.get(rootSubscriptionId) != null) {
				return notifications.get(rootSubscriptionId).getOldIndividual();
			} else {
				return derivedSubscriptions.get(rootSubscriptionId);
			}
		}

		return null;
	}

	/**
	 * Finds the parent Individual that is associated with modification received
	 * from an indication
	 * 
	 * @param derivedSubscriptionId the derived subscription id
	 * @return the root individual to what is related the derived subscription
	 * @throws KPIModelException
	 */
	private AbstractOntConcept getLatestIndividual(AbstractOntConcept obsoleteIndividual) throws KPIModelException {
		try {
			for (long subscriptionId : notifications.keySet()) {
				if (notifications.get(subscriptionId).getOldIndividual().equals(obsoleteIndividual)) {
					Logger.debug("Creating a new indiviudal from previous notifications");
					return (AbstractOntConcept) notifications.get(subscriptionId).getNewIndividual().clone();
				}
			}

			Logger.debug("Creating a completely new individual from obsolete clone");
			return (AbstractOntConcept) obsoleteIndividual.clone();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot get latest individual");
		}
	}

	/**
	 * Finds the root subscription for a given nested subscription
	 * 
	 * @param derivedSubscriptionId the derived subscription id
	 * @return the root subscription corresponding to the derived subscription
	 * @throws KPIModelException when some error appeared in getting individuals
	 */
	private ArrayList<Long> getRelatedSubscriptionIds(AbstractOntConcept individual) throws KPIModelException {
		if (individual == null) {
			throw new KPIModelException("Cannot get class subscription id due to individual is null valued");
		}

		try {
			return getRelatedSubscriptionIds(individual._getIndividualIRI());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot get class subscription id:" + ex.getMessage());
		}
	}

	/**
	 * Finds the root subscription for a given nested subscription
	 * 
	 * @param individualIRI the individual IRI
	 * @return the list of class subscriptions directly associated to the individual
	 * @throws KPIModelException when some error appeared in getting individuals
	 */
	private ArrayList<Long> getRelatedSubscriptionIds(String individualIRI) throws KPIModelException {
		if (individualIRI == null) {
			throw new KPIModelException("Cannot get class subscription id due to individual is null valued");
		}

		ArrayList<Long> relatedSubscriptionIds = new ArrayList<Long>();

		try {
			// check in all subscription individuals
			for (Long classSubscriptionId : subscriptionIndividuals.keySet()) {
				if (classSubscriptionId != null) {
					// check all the individuals
					for (AbstractOntConcept subcribedIndividual : subscriptionIndividuals.get(classSubscriptionId)) {
						if (subcribedIndividual._getIndividualIRI().equals(individualIRI)) {
							relatedSubscriptionIds.add(classSubscriptionId);
						}
					}
				}
			}

			// check all individual subscriptions
			for (Long individualSubscriptionId : individualSubscription.keySet()) {
				if (individualSubscriptionId != null) {
					if (individualSubscription.get(individualSubscriptionId)._getIndividualIRI()
							.equals(individualIRI)) {
						relatedSubscriptionIds.add(individualSubscriptionId);
					}
				}
			}

			return relatedSubscriptionIds;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Cannot get class subscription id for individual " + individualIRI + " due to" + ex.getMessage());
		}
	}

	private void preventOverrideIndividual(AbstractOntConcept individual,  Long dataSubscriptionId) {
		String rootId=getRootIndividual(dataSubscriptionId)._getIndividualID();
		String id=individual._getIndividualID();
		if(individual == null || rootId.equals(id) == false) {
			System.out.println("*********CRITICAL***********"); //this SHOULD never appear, otherwise, there is an error overriding subscriptions in this class.
		}
	}
	
	/**
	 * Updates an individual with a given data
	 * 
	 * @param individual         the indidivual to be updated
	 * @param updateData         the data to be updated in the individual
	 * @param dataSusbcriptionId the identifer of subscription data to be updated
	 * @throws KPIModelException when cannot update individual
	 */
	private synchronized void udpateIndividual(AbstractOntConcept individual, ResultsData updateData, Long dataSubscriptionId)
			throws KPIModelException {
		
		if (dataSubscriptionId == null) { // check null value of datasubscriptionId
			throw new KPIModelException("Subscription data is a null value");
		}

		if (individual == null) { // check null-value of individual
			throw new KPIModelException("Cannot populate a null valued individual");
		}

		if ((updateData == null) || !updateData.hasContent()) { // check null-value of individual
			Logger.error("Cannot populate without data for individual " + individual._getIndividualID()
					+ " and subscription " + dataSubscriptionId + " because data does not longer exist");
			return;
		}

		Logger.debug("updateIndividual(" + individual._getIndividualID() + "," + updateData + "," + dataSubscriptionId
				+ ")");

		try {
			if (updateData.isRemovableIndividual()) {
				Logger.debug("The individual " + updateData.getIndividualIRI() + " is set to be removed");
				Slot<?> slot = findRemoveSlot(dataSubscriptionId);

				if (slot == null) {
					Logger.error("Cannot find the slot for subscription " + dataSubscriptionId);
					return;
				}

				boolean present = isDataPresent(slot, updateData.getIndividualIRI());

				if (present) {
					Logger.debug("Found property " + slot.getName() + " to remove the individual "
							+ updateData.getIndividualIRI());

					removeProperty(slot, updateData, dataSubscriptionId);
					removeSubscriptionData(individual, dataSubscriptionId);

				} else {
					Logger.debug(
							"Individual " + updateData.getIndividualIRI() + " does not longer exists in individual "
									+ individual._getIndividualID() + " in property " + slot.getName());
				}

			} else if (subscriptionHierarchy.get(dataSubscriptionId) == null) {
				Logger.debug(
						"An update request for the root individual " + updateData.getIndividualIRI() + " is received");

				// update properties with result property values
				for (ResultsProperty property : updateData.listProperties()) {
					updateIndividual(individual, property, dataSubscriptionId);
					updateSubscriptionData(individual, dataSubscriptionId);
				}
			} else {
				Logger.debug("An update request for the indirect individual " + updateData.getIndividualIRI()
						+ " is received");

				// find the property to update
				Slot<?> slot = findSlot(dataSubscriptionId);

				// we have located the object property to update
				// in this property we can update one or more properties associated to this
				// object individual
				if ((slot == null) || !slot.isObjectProperty()) {
					Logger.error("Hierachy " + subscriptionHierarchy);
					Logger.error("Data subscription id: " + dataSubscriptionId);
					throw new KPIModelException(
							"Cannot find a relationship relating individual " + individual._getIndividualID()
									+ " with object individual " + updateData.getIndividualIRI());
				}

				boolean present = isDataPresent(slot, updateData.getIndividualIRI());

				if (present) {
					updateProperty(slot, updateData, dataSubscriptionId);
					individual._addProperty(slot);
					updateSubscriptionData(individual, dataSubscriptionId);
				} else {
					Logger.debug("Data cannot be updated because it does no longer exists");
				}

			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot update the individual " + individual._getIndividualID());
		}
	}

	/**
	 * Removes subscription data
	 * 
	 * @param updatedIndividual  the updated individual
	 * @param dataSubscriptionId the data subscription to be removed
	 */
	private void removeSubscriptionData(AbstractOntConcept updatedIndividual, Long dataSubscriptionId) {
		Long rootSubscriptionId = getRootSubscriptionId(dataSubscriptionId);
		subscriptionHierarchy.remove(dataSubscriptionId);
		if (rootSubscriptionId != null) {
			derivedSubscriptions.put(rootSubscriptionId, updatedIndividual);
		}

	}

	/**
	 * Update subscription data
	 * 
	 * @param updatedIndividual  the updated individual
	 * @param dataSubscriptionId the data subscription to be updated
	 */
	private synchronized void updateSubscriptionData(AbstractOntConcept updatedIndividual, Long dataSubscriptionId) {
		Long rootSubscriptionId = getRootSubscriptionId(dataSubscriptionId);

		if (rootSubscriptionId != null) {
			derivedSubscriptions.put(rootSubscriptionId, updatedIndividual);
		}

	}


	private Long getRootSubscriptionId(long derivedSubscriptionId) {
		Long subscriptionId = derivedSubscriptionId;
		while (subscriptionHierarchy.get(subscriptionId) != null) {
			subscriptionId = subscriptionHierarchy.get(subscriptionId);
		}

		return subscriptionId;
	}

	/**
	 * Finds a Slot corresponding to a subscription
	 * 
	 * @param subscriptionId the subscription identifier
	 * @return the Slot correspondinte to a subscription
	 */
	private Slot<?> findSlot(long derivedSubscriptionId) throws KPIModelException {
		Long subscriptionId = derivedSubscriptionId;
		HashMap<Long, Long> routeToProperty = new HashMap<Long, Long>();
		while (subscriptionHierarchy.get(subscriptionId) != null) {
			routeToProperty.put(subscriptionHierarchy.get(subscriptionId), subscriptionId);
			subscriptionId = subscriptionHierarchy.get(subscriptionId);
		}

		if (subscriptionId != null) {
			// now we got the root subscription
			Logger.debug("Root subscription of " + derivedSubscriptionId + " corresponds to " + subscriptionId);
			AbstractOntConcept aoc = derivedSubscriptions.get(subscriptionId);
			AbstractOntConcept individual = getLatestIndividual(aoc);
			return findSlot(subscriptionId, individual._getIndividualIRI(), derivedSubscriptionId, routeToProperty);
		}

		return null;
	}

	/**
	 * Finds a Slot corresponding to a subscription
	 * 
	 * @param subscriptionId the subscription identifier
	 * @return the Slot corresponding to a subscription
	 */
	private Slot<?> findRemoveSlot(long derivedSubscriptionId) throws KPIModelException {
		Long subscriptionId = derivedSubscriptionId;
		Long parentSubscriptionId = null;
		HashMap<Long, Long> routeToProperty = new HashMap<Long, Long>();
		while (subscriptionHierarchy.get(subscriptionId) != null) {
			if (parentSubscriptionId == null) {
				parentSubscriptionId = subscriptionId;
			}
			routeToProperty.put(subscriptionHierarchy.get(subscriptionId), subscriptionId);
			subscriptionId = subscriptionHierarchy.get(subscriptionId);
		}

		if (subscriptionId != null) {
			// now we got the root subscription
			Logger.debug("Root subscription of " + derivedSubscriptionId + " corresponds to " + subscriptionId);
			AbstractOntConcept aoc = derivedSubscriptions.get(subscriptionId);
			AbstractOntConcept individual = getLatestIndividual(aoc);
			return findSlot(subscriptionId, individual._getIndividualIRI(), parentSubscriptionId, routeToProperty);
		}

		return null;
	}

	/**
	 * Checks if the remove individual is already inside the slot
	 * 
	 * @param slot                a Slot
	 * @param removeIndividualIRI the individual IRI to be removed
	 * @return <code>true</code> if the individual is still inside the slot
	 */
	@SuppressWarnings({ "rawtypes" })
	// @SuppressWarnings({ "unchecked" })
	private boolean isDataPresent(Slot<?> slot, String removeIndividualIRI) throws KPIModelException {
		// find the object to be removed
		if (removeIndividualIRI == null) {
			throw new KPIModelException("Cannot remove something that seems to be a null individual");
		}

		try {
			if (slot == null) {
				Logger.error("Cannot remove the obsolete individual " + removeIndividualIRI);
				return false;
			}

			if (!slot.isObjectProperty()) {
				Logger.error("The slot " + slot.getName() + " is not an object property, so that " + removeIndividualIRI
						+ " cannot be checked to be removed");
				return false;
			}

			/** OBJECT PROPERTY */
			if (slot.isFunctionalProperty()) { /** FUNCTIONAL OBJECT PROPERTY */
				// assign to the current aoc
				FunctionalObjectSlot foslot = (FunctionalObjectSlot) slot;
				if (foslot.getValue() != null) {
					if (foslot.getValue()._getIndividualIRI().equals(removeIndividualIRI)) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else { /** NON-FUNCTIONAL OBJECT PROPERTY */
				NonFunctionalObjectSlot nfoslot = (NonFunctionalObjectSlot) slot;

				for (Object value : nfoslot.listValues()) {
					AbstractOntConcept aoc = (AbstractOntConcept) value;
					if (aoc._getIndividualIRI().equals(removeIndividualIRI)) {
						return true;
					}
				}
			} // END OF OBJECT PROPERTY

			return false;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Unable to check the presence of individual " + removeIndividualIRI + " in slot " + slot.getName());
		}

	}

	/**
	 * Finds a slot
	 * 
	 * @param startSubscriptionId initial subscription identifier
	 * @param startConceptIRI     initial individual iri
	 * @param endSubscriptionId   the leaf subscription
	 * @param routeToProperty     the route
	 * @return the slot
	 * @throws KPIModelException
	 */
	private Slot<?> findSlot(Long startSubscriptionId, String startConceptIRI, Long endSubscriptionId,
			HashMap<Long, Long> routeToProperty) throws KPIModelException {
		Logger.debug("findSlot(" + startSubscriptionId + "," + startConceptIRI + "," + endSubscriptionId + ","
				+ routeToProperty + ")");
		for (IndividualRelationship relationship : individualRelationships) {
			if (relationship.getSubjectIRI().equals(startConceptIRI)
					&& (relationship.getSubjectSubscriptionId().equals(startSubscriptionId))) {
				if (routeToProperty.get(startSubscriptionId).equals(relationship.getObjectSubscriptionId())) {
					Logger.debug("Found something that has relation: " + relationship.toString());
					if (relationship.getObjectSubscriptionId().equals(endSubscriptionId)) {
						AbstractOntConcept aoc = derivedSubscriptions.get(relationship.getSubjectSubscriptionId());
						aoc = getLatestIndividual(aoc);
						// this is the property that we should change
						Slot<?> slotProp = aoc._getProperty(relationship.getPropertyName());
						return slotProp;
					} else {
						return findSlot(relationship.getObjectSubscriptionId(), relationship.getObjectIRI(),
								endSubscriptionId, routeToProperty);
					}
				}
			}
		}
		return null;
	}

	/**
	 * Updates a slot with the obtained subscription data
	 * 
	 * @param changeSlot         the slot to be modified
	 * @param data               the subscription data obtained
	 * @param dataSubscriptionId the parent data subscription id
	 * @throws KPIModelException
	 */
	private void updateProperty(Slot<?> changeSlot, ResultsData data, Long dataSubscriptionId)
			throws KPIModelException {
		// find the object to be updated
		if (data.getIndividualIRI() == null) {
			throw new KPIModelException("Cannot update something that seems to be a null individual");
		}

		try {
			// Gets the individual to be updated
			AbstractOntConcept individualToUpdate = getIndividualFromSlot(changeSlot, data.getIndividualIRI());

			if (individualToUpdate == null) { // check null value
				throw new KPIModelException("Cannot find individual to update " + data.getIndividualIRI());
			}

			this.determineTypeOfProperties(data.listProperties());

			// update properties with result property values
			for (ResultsProperty property : data.listProperties()) {
				updateIndividual(individualToUpdate, property, dataSubscriptionId);
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot update individual " + data.getIndividualIRI()
					+ " due to an unexpected error:" + ex.getMessage(), ex);
		}
	}

	/**
	 * Removes a slot value with the obtained subscription data
	 * 
	 * @param removeSlot         the slot to be set null
	 * @param removeData         the subscription data obtained
	 * @param dataSubscriptionId the parent data subscription id
	 * @throws KPIModelException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void removeProperty(Slot<?> removeSlot, ResultsData removeData, Long dataSubscriptionId)
			throws KPIModelException {
		// find the object to be removed
		if (removeData.getIndividualIRI() == null) {
			throw new KPIModelException("Cannot remove something that seems to be a null individual");
		}

		try {
			if (removeSlot == null) {
				Logger.error("Cannot remove the obsolete individual " + removeData.getIndividualIRI());
				return;
			}

			if (!removeSlot.isObjectProperty()) {
				Logger.error(
						"The slot " + removeSlot.getName() + " to remove does not seems to be an object property.  ");
				return;
			}

			/** OBJECT PROPERTY */
			if (removeSlot.isFunctionalProperty()) {
				FunctionalObjectSlot foslot = (FunctionalObjectSlot) removeSlot;
				Logger.debug("Setting to null property valued with individual " + removeData.getIndividualIRI());
				foslot.setValue(null);
			} else { /** NON-FUNCTIONAL OBJECT PROPERTY */
				NonFunctionalObjectSlot nfoslot = (NonFunctionalObjectSlot) removeSlot;

				for (Object value : nfoslot.listValues()) {
					AbstractOntConcept aoc = (AbstractOntConcept) value;
					if (aoc._getIndividualIRI().equals(removeData.getIndividualIRI())) {
						Logger.debug("Removing individual " + removeData.getIndividualIRI() + " from property "
								+ removeSlot.getName() + " values");
						nfoslot.removeValue(aoc);
						return;
					}
				}
			} // END OF OBJECT PROPERTY
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Unable to remove individual " + removeData.getIndividualIRI());
		}
	}

	/**
	 * Gets an individual from a slot
	 * 
	 * @param slot          the slot
	 * @param individualIRI the individual IRI to find inside the slot
	 * @return an AbstractOntConcept located inside the slot that corresponds to the
	 *         individual IRI. if no data is found <code>null</code> value is
	 *         returned
	 * @throws KPIModelException when some error appeared during the searching of
	 *                           the individual
	 */
	@SuppressWarnings({ "rawtypes" })
	// @SuppressWarnings({ "unchecked" })
	private AbstractOntConcept getIndividualFromSlot(Slot<?> slot, String individualIRI) throws KPIModelException {
		/** PRE: SLOT IS AN OBJECT PROPERTY */
		try {
			AbstractOntConcept individual = null;

			if (slot.isFunctionalProperty()) { /** FUNCTIONAL OBJECT PROPERTY */
				FunctionalObjectSlot foslot = (FunctionalObjectSlot) slot;
				individual = foslot.getValue();

				// test if the individual IRI
				if ((individual != null) && individual._getIndividualIRI().equals(individualIRI)) {
					// then the value is already created. it is an update of datatype values
					return individual;
				}
			} else { /** NON-FUNCTIONAL OBJECT PROPERTY */
				NonFunctionalObjectSlot nfoslot = (NonFunctionalObjectSlot) slot;
				for (Object obj : nfoslot.listValues()) {
					if (obj != null) {
						individual = (AbstractOntConcept) obj;
						if (individual._getIndividualIRI().equals(individualIRI)) { // this is the individual that we
																					// are looking for
							return individual;

						}
					}
				}
			}

			return null;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Some error found trying to get the individual");
		}
	}

	/**
	 * Updates an individual with the provided data
	 * 
	 * @param individual the individual to be udpated
	 * @param data       the data containing the data to be updated
	 * @throws KPIModelException when cannot update the data
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void updateIndividual(AbstractOntConcept individual, ResultsProperty data, Long dataSubscriptionId)
			throws KPIModelException {
		if ((data == null) || !data.hasContent()) {
			throw new KPIModelException("Cannot update individual " + individual._getIndividualID()
					+ " because there is no data to update");
		}

		try {
			// gets the property
			Slot<?> slot = individual._getProperty(data.getName());

			if (slot == null) {
				Logger.error("Cannot find the property " + data.getName() + " in individual "
						+ individual._getIndividualID());
				return;
			}

			data.setFunctional(slot.isFunctionalProperty());
			data.setDatatypeProperty(slot.isDatatypeProperty());
			data.setObjectProperty(slot.isObjectProperty());
			data.setInitialized(true);
			propertiesCharacteristics.add(new PropertyCharacteristic(data));

			if (slot.isDatatypeProperty()) {
				if (slot.isFunctionalProperty()) { /** FUNCTIONAL DATATYPE PROPERTY */
					FunctionalDatatypeSlot fdslot = (FunctionalDatatypeSlot) slot;
					String newValue = data.getNewValue();

					if (newValue != null) { // assign the new value
						Logger.debug("Setting " + newValue + " in property " + data.getName() + " of "
								+ individual._getIndividualID());
						fdslot.setValue(fdslot.fromEncodedValue(newValue)); // this sets the new value
					} else { // remove the value
						Logger.debug("Clearing property " + data.getName() + " values from "
								+ individual._getIndividualID());
						fdslot.setValue(null);
					}
					individual._addProperty(fdslot);
				} else { /** NON-FUNCTIONAL DATATYPE PROPERTY */
					NonFunctionalDatatypeSlot nfdslot = (NonFunctionalDatatypeSlot) slot;

					// remove old values
					for (String oldValue : data.listObsoleteValues()) { // remove values that no longer belong to the
																		// individual
						Logger.debug("Removing " + oldValue + " in property " + data.getName() + " of "
								+ individual._getIndividualID());

						nfdslot.removeValue(nfdslot.fromEncodedValue(oldValue));
					}

					// add new values
					for (String newValue : data.listNewValues()) { // add the new values
						Logger.debug("Removing " + newValue + " in property " + data.getName() + " of "
								+ individual._getIndividualID());
						nfdslot.addValue(nfdslot.fromEncodedValue(newValue));
					}
					individual._addProperty(nfdslot);
				}
			} else {
				/** OBJECT PROPERTY */
				if (slot.isFunctionalProperty()) { /** FUNCTIONAL OBJECT PROPERTY */
					// assign to the current aoc
					FunctionalObjectSlot foslot = (FunctionalObjectSlot) slot;
					if (data.getNewValue() != null) {
						Logger.debug("Setting functional object property " + slot.getName() + " value to "
								+ data.getNewValue());
						AbstractOntConcept propertyIndividual = this.getIndividualFromSIB(data.getNewValue(),
								dataSubscriptionId, data.getName(), individual._getIndividualIRI());

						if (propertyIndividual == null) {
							Logger.error("Cannot get individual " + data.getNewValue() + " from SIB");
						} else {
							Logger.debug("Setting [" + individual._getIndividualID() + "," + slot.getName() + ","
									+ propertyIndividual._getIndividualID() + "]");
							foslot.setValue(propertyIndividual);
						}
					} else {
						Logger.debug("Removing functional object property " + slot.getName() + " value to null");
						foslot.setValue(null);
					}

					// adds the property to the property individual
					individual._addProperty(foslot);
				} else {
					NonFunctionalObjectSlot nfoslot = (NonFunctionalObjectSlot) slot;
					for (String obsoleteValue : data.listObsoleteValues()) {
						if (obsoleteValue != null) {
							AbstractOntConcept propertyIndividual = this.getIndividualFromSIB(data.getOldValue(), null,
									data.getName(), individual._getIndividualIRI());

							if (propertyIndividual == null) {
								Logger.error("Cannot get individual " + data.getNewValue() + " from SIB");
							} else {
								// assign to the current aoc
								nfoslot.removeValue(propertyIndividual);
							}
						}
					}

					for (String value : data.listNewValues()) {
						if (value != null) {
							AbstractOntConcept propertyIndividual = this.getIndividualFromSIB(data.getNewValue(),
									dataSubscriptionId, data.getName(), individual._getIndividualIRI());

							if (propertyIndividual == null) {
								Logger.error("Cannot get individual " + data.getNewValue() + " from SIB");
							} else {
								// assign to the current aoc
								nfoslot.addValue(propertyIndividual);
							}
						}
					}

					// Include the property values in the property individual
					individual._addProperty(nfoslot);
				} // END OF OBJECT PROPERTY

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Unable to update individual " + individual._getIndividualID() + " with data " + data.toString());
		}
	}

	private AbstractOntConcept getIndividualFromSIB(String individualIRI, Long dataSubscriptionId,
			String parentPropertyName, String parentIndividualIRI) throws KPIModelException {
		try {
			Logger.debug("getIndividualFromSIB(" + individualIRI + "," + dataSubscriptionId + "," + parentPropertyName
					+ "," + parentIndividualIRI + ")");

			// Get the class of the property individual
			String conceptClass = getClassOf(individualIRI);

			if (conceptClass == null) {
				Logger.error("concept " + individualIRI + " is not available in the SIB");
				return null;
			}

			// Create the corresponding individual
			Object obj = Class.forName(conceptClass, true, ClasspathManager.getInstance().getClassLoader())
					.newInstance();
			AbstractOntConcept propertyIndividual = (AbstractOntConcept) obj;
			if (individualIRI != null) {
				propertyIndividual._setIndividualIRI(individualIRI);
			} else {
				propertyIndividual._setIndividualIRI(null);
			}

			// populate with properties (recursive)
			if (dataSubscriptionId != null) {
				Long propertySubscriptionId = this.subscribeToIndividual(propertyIndividual, dataSubscriptionId);
				if (propertySubscriptionId != null) {
					IndividualRelationship graph = new IndividualRelationship(parentIndividualIRI, parentPropertyName,
							propertyIndividual._getIndividualIRI());
					graph.setSubjectSubscriptionId(dataSubscriptionId);
					graph.setObjectSubscriptionId(propertySubscriptionId);
					individualRelationships.add(graph);
				} else { // the individual was already part of the subscriptions
					for (Long individualSubscriptionId : this.derivedSubscriptions.keySet()) {
						if (this.derivedSubscriptions.get(individualSubscriptionId).equals(propertyIndividual)) {
							propertySubscriptionId = individualSubscriptionId;
							propertyIndividual = this.derivedSubscriptions.get(individualSubscriptionId);
						}
					}

					IndividualRelationship graph = new IndividualRelationship(parentIndividualIRI, parentPropertyName,
							propertyIndividual._getIndividualIRI());
					graph.setSubjectSubscriptionId(dataSubscriptionId);
					graph.setObjectSubscriptionId(propertySubscriptionId);
					individualRelationships.add(graph);
				}
			} else {
				this.populateIndividual(propertyIndividual);
			}
			Logger.debug("Returning individual " + propertyIndividual);
			return propertyIndividual;
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot get the individual " + individualIRI + " due to " + ex.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot get the individual " + individualIRI + " due to " + ex.getMessage());
		}
	}

	/**
	 * Dumps the results obtained from an SUBSCRIPTION INDICATION into a ResultsData
	 * structure
	 * 
	 * @param newResults      the indication new results in RDF-M3 format
	 * @param obsoleteResults the indication obsolete results in RDF-M3 format
	 * @return a set of ResultData representing the data obtained from indications
	 */
	private Collection<ResultsData> importResults(String newResults, String obsoleteResults) throws KPIModelException {
		try {

			Logger.debug("Importing indication results...");
			/** Create an arraylist to have returned data */
			ArrayList<ResultsData> individualsData = new ArrayList<ResultsData>();

			if (hasContentRDFM3(newResults)) {
				importResults(individualsData, newResults);
			}

			if (hasContentRDFM3(obsoleteResults)) {
				importResults(individualsData, obsoleteResults, true);
			}

			return individualsData;
		} catch (RDFM3ParseException ex) {
			throw new KPIModelException("Cannot import data from provided results:" + ex.getMessage());
		}
	}

	/**
	 * Imports indication data in what data is new results
	 * 
	 * @param indicationData a collection of indication data.
	 * @param results        the results in rdf-m3 format
	 * @throws RDFM3ParseException when cannot parse data from incoming results
	 */
	private void importResults(Collection<ResultsData> indicationData, String results) throws RDFM3ParseException {
		try {
			importResults(indicationData, results, false);
		} catch (RDFM3ParseException ex) {
			ex.printStackTrace();
			throw new RDFM3ParseException("Cannot import data from provided results:" + ex.getMessage());
		}
	}

	/**
	 * Imports data from indication results
	 * 
	 * @param indicationData the indication data
	 * @param results        a string with the results in rdf-m3 format
	 * @param obsolete       if the data is treated as obsolete data
	 */
	private void importResults(Collection<ResultsData> resultsData, String results, boolean obsolete)
			throws RDFM3ParseException {
		try {
			// Parses the new results
			M3Parser parser = new M3Parser();
			RDFM3 m3 = parser.parse(results);

			for (RDFM3Triple triple : m3.listTriples()) {
				boolean found = false;
				String individualIRI = triple.getSubject();

				for (ResultsData data : resultsData) { // check if we have defined before this individual
					if (data.getIndividualIRI().equals(individualIRI)) {
						if (obsolete && isRDFType(triple)) {
							Logger.debug("Setting " + data.getIndividualIRI() + " to be removable");
							data.setRemovable(true);
						} else if (!obsolete) {
							data.addNewValue(triple);
						} else {
							data.addObsoleteValue(triple);
						}
						found = true;
					}
				}

				// if we do not find any individual with the given iri, we have to create a new
				// one
				// and add to the list of results data
				if (!found) {
					ResultsData data = new ResultsData(individualIRI);
					if (obsolete && isRDFType(triple)) {
						data.setRemovable(true);
					} else if (!obsolete) { // data can correspond either to new results or obsolete results
						data.addNewValue(triple);
					} else {
						data.addObsoleteValue(triple);
					}
					resultsData.add(data);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RDFM3ParseException(ex);
		}
	}

	/**
	 * Gets the RDF-M3 xml in what is queried about individuals of a given
	 * AbstractOntConcept
	 * 
	 * @param aoc the class to be consulted
	 * @return a RDF-M3 xml querying about a given class
	 */
	private RDFM3 getIndividualQuery(String aocId) {
		RDFM3 query = new RDFM3();
		RDFM3Triple queryTriple = new RDFM3Triple(RDFM3Triple.WILDCARD, "rdf:type", aocId);
		query.addTriple(queryTriple);
		return query;
	}

	/**
	 * Gets the RDF-M3 xml that asks about the subclasses of a given class
	 * 
	 * @param aoc the class to be consulted
	 * @return a RDF-M3 xml querying about a given class
	 */
	private RDFM3 getSubClassOfQuery(String aocId) {
		RDFM3 query = new RDFM3();
		RDFM3Triple queryTriple = new RDFM3Triple(RDFM3Triple.WILDCARD, "rdfs:subClassOf", aocId);
		query.addTriple(queryTriple);
		return query;
	}

	/**
	 * Gets the RDF-M3 xml that asks about the subclasses of a given class
	 * 
	 * @param individualId the individual class to be consulted
	 * @return a RDF-M3 xml querying about a given class
	 */
	private RDFM3 getPropertiesQuery(String individualId) {
		RDFM3 query = new RDFM3();
		RDFM3Triple queryTriple = new RDFM3Triple(individualId, RDFM3Triple.WILDCARD, RDFM3Triple.WILDCARD);
		query.addTriple(queryTriple);
		return query;
	}

	/**
	 * Gets the RDF-M3 xml that asks about the subclasses of a given class
	 * 
	 * @param propertyId the individual class to be consulted
	 * @return a RDF-M3 xml querying about a given class
	 */
	private RDFM3 getPropertyTypeQuery(String propertyId) {
		RDFM3 query = new RDFM3();
		RDFM3Triple queryTriple = new RDFM3Triple(propertyId, "rdf:type", RDFM3Triple.WILDCARD);
		query.addTriple(queryTriple);
		return query;
	}

	/**
	 * Gets the RDF-M3 xml that asks about the subclasses of a given class
	 * 
	 * @param individualId the individual class to be consulted
	 * @return a RDF-M3 xml querying about a given class
	 */
	private RDFM3 getIndividualTypeQuery(String individualId) {
		RDFM3 query = new RDFM3();
		RDFM3Triple queryTriple = new RDFM3Triple(individualId, "rdf:type", RDFM3Triple.WILDCARD);
		query.addTriple(queryTriple);
		return query;
	}

	/**
	 * Gets the set of individuals for a given set of classes
	 * 
	 * @param ontClassURLs a list of ontology class urls
	 * @return the set of AbstractOntConcept individuals corresponding to the given
	 *         classes
	 * @throws KPIModelException when some error arise
	 */
	@SuppressWarnings("unchecked")
	private <C extends AbstractOntConcept> List<C> getIndividuals(Collection<String> ontClassURLs)
			throws KPIModelException {

		/** Store here the individuals */
		ArrayList<C> individuals = new ArrayList<C>();

		try {
			// Create a ComposedPendingTask to throw all queries in this second step
			ComposedPendingTask cpt = new ComposedPendingTask();

			for (String ontClassURL : ontClassURLs) {
				// Create the associated query
				RDFM3 m3Query = this.getIndividualQuery(ontClassURL);

				// Create one task per query
				cpt.addPendingSubtask(model.getSIB().query(m3Query.toString(), TypeAttribute.RDFM3));
			}

			if (cpt.size() == 0) {
				return null;
			}

			// Throw the requests to the SIB
			model.getSIB().publish();
			cpt.waitUntilFinished(model.getTimeout());

			if (!cpt.isFinished()) { // the timeout has expired
				throw new KPIModelException("Cannot get the individual set due to the timeout has been exceded");
			}

			// Check the correctness of each of the tasks in the composite task
			for (PendingTask qpt : cpt.listPendingTasks()) {
				// Gets the status of the response
				SIBStatus status = (SIBStatus) qpt.getProperty("status");

				if (!status.equals(SIBStatus.STATUS_OK)) {
					throw new KPIModelException(
							"The individual obtaining query cannot be executed due to " + status.getValue());
				}

				// gets the set of results
				String results = (String) qpt.getProperty("results");

				if ((results != null) && !results.equals("")) { // if the results are not null or void
					individuals.addAll((Collection<? extends C>) createEmptyIndividuals(results));
				}
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Unable to execute the query due to some error in model layer", ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Unable to execute the query", ex);
		}

		// returns the list of individuals
		return individuals;
	}

	/**
	 * Gets the set of individuals for a given set of classes
	 * 
	 * @param ontClassURLs a list of ontology class urls
	 * @return the set of AbstractOntConcept individuals corresponding to the given
	 *         classes
	 * @throws KPIModelException when some error arise
	 */
	@SuppressWarnings("unchecked")
	private <C extends IAbstractOntConcept> void subscribeToClasses(Collection<String> ontClassURLs,
			ISubscription<C> as) throws KPIModelException {

		try {
			for (String ontClassURL : ontClassURLs) {
				/** Store here the individuals */
				ArrayList<AbstractOntConcept> individuals = new ArrayList<AbstractOntConcept>();

				// Create a query that queries about the properties associated to the individual
				String query = this.getIndividualQuery(ontClassURL).toString(); // [*, rdf:type, Class]
				PendingTask pt = model.getSIB().subscribe(query, TypeAttribute.RDFM3);

				model.getSIB().publish();
				pt.waitUntilFinished(model.getTimeout());

				if (!pt.isFinished()) { // the timeout has expired
					throw new KPIModelException("The query finished by timeout");
				}

				// gets the subscription identifier
				long subscriptionId = (Long) pt.getProperty("subscriptionId");

				String conceptName = model.getConceptName(ontClassURL);

				Object obj = Class.forName(conceptName, true, ClasspathManager.getInstance().getClassLoader())
						.newInstance(); // gets the instance
				C classConcept = (C) obj; // performs the casting

				subscribedConcepts.put(subscriptionId, (Class<? extends AbstractOntConcept>) classConcept.getClass());

				// Gets the status of the response
				SIBStatus status = (SIBStatus) pt.getProperty("status");

				if (!status.equals(SIBStatus.STATUS_OK)) {
					throw new KPIModelException("The query cannot be executed due to " + status.getValue());
				}

				// Gets the results parameter from the QUERY RESPONSE
				String results = (String) pt.getProperty("results");

				if ((results != null) && !results.equals("")) {
					individuals.addAll(createEmptyIndividuals(results));
				}

				// complete with data
				if (individuals.size() == 0) {
					Logger.debug("There is no individuals associated to subscription");
				}

				// Notify
				for (AbstractOntConcept individual : individuals) {
					// Fill with data
					Long individualSubcriptionId = subscribeToIndividual(individual, null);
					if (individualSubcriptionId != null) {
						Logger.debug("Main subscription [" + individualSubcriptionId + "]");
					}

					individual._setPublished();
//					individual._setModel(model);
//					individual._setPublished(true);
					model.addPublished(individual);
					as.conceptAdded((C) individual);
				}

				// Update subscription listeners
				this.model.getSIB().addSubscriptionsListener(subscriptionId, this);
				subscriptions.put(subscriptionId, (ISubscription<AbstractOntConcept>) as);

				// Save individuals
				subscriptionIndividuals.put(subscriptionId, individuals);
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Unable to execute the query due to some error in model layer", ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Unable to execute the query", ex);
		}
	}

	/**
	 * Populates an individual with its corresponding property values. To get
	 * property values it is necessary to query about it to the SIB.
	 * 
	 * @param individual the individual to be populated
	 * @throws KPIModelException if individual cannot be populated
	 */
	private void populateIndividual(AbstractOntConcept individual) throws KPIModelException {
		try {
			// Create a query that queries about the properties associated to the individual
			String query = this.getPropertiesQuery(individual._getIndividualIRI()).toString();
			PendingTask pt = model.getSIB().query(query, TypeAttribute.RDFM3);

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());

			if (!pt.isFinished()) { // the timeout has expired
				throw new KPIModelException("The query finished by timeout");
			}

			// Gets the status of the response
			SIBStatus status = (SIBStatus) pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The query cannot be executed due to " + status.getValue());
			}

			// Gets the results parameter from the QUERY RESPONSE
			String results = (String) pt.getProperty("results");

			if ((results != null) && !results.equals("")) {
				// Parses the results
				populateIndividual(individual, results);
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Cannot populate individual " + individual._getIndividualID() + " with properties", ex.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Some error ocurred during property population of individual " + individual._getIndividualID(),
					ex.getCause());
		}
	}

	/**
	 * Subscribes to a given individual
	 * 
	 * @param individual the individual to be subscribed
	 */
	private Long subscribeToIndividual(AbstractOntConcept individual) throws KPIModelException {
		try {
			return subscribeToIndividual(individual, null);
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot subscribe to individual", ex);
		}
	}

	/**
	 * Subscribes to all data of a given individual
	 * 
	 * @param individual the concept to be filled
	 * @return an AbstractOntConcept with the properties filled
	 */
	private synchronized Long  subscribeToIndividual(AbstractOntConcept individual, Long parentSubscriptionId)
			throws KPIModelException {
		// the concept
		// AbstractOntConcept aoc = individual;
		Long subscriptionId = null;

		try {
			if (derivedSubscriptions.containsValue(individual)) { // already subscribed
				return null;
			}

			// Create a query that queries about the properties associated to the individual
			String subscriptionQuery = this.getPropertiesQuery(individual._getIndividualIRI()).toString(); // [individual,
																											// *, *]
			PendingTask pt = model.getSIB().subscribe(subscriptionQuery, TypeAttribute.RDFM3);

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());

			if (!pt.isFinished()) { // the timeout has expired
				throw new KPIModelException("The query finished by timeout");
			}

			// Gets the status of the response
			SIBStatus status = (SIBStatus) pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The subscription cannot be executed due to " + status.getValue());
			}

			// gets the subscription identifier
			subscriptionId = (Long) pt.getProperty("subscriptionId");

			// Gets the results parameter from the QUERY RESPONSE
			String results = (String) pt.getProperty("results");

			// Now we are sure that we are subscribed to the individual
			// This is marked as a derived subscription, this is, not directly asked by the
			// user
			derivedSubscriptions.put(subscriptionId, individual);

			// link with the parent subscription (if exists)
			if (parentSubscriptionId != null) {
				subscriptionHierarchy.put(subscriptionId, parentSubscriptionId);
			}

			this.model.getSIB().addSubscriptionsListener(subscriptionId, this);

			// Process the results
			if ((results != null) && !results.equals("")) {
				// populates the individual
				populateIndividual(individual, results, subscriptionId);
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Cannot populate individual " + individual._getIndividualID() + " with properties", ex.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Some error ocurred during property population of individual " + individual._getIndividualID(),
					ex.getCause());
		}

		return subscriptionId;
	}

	/**
	 * Populates the individual with the properties obtained in the result. Take
	 * into account that this population is made recursively
	 * 
	 * @param individual the individual to be populated
	 * @param results    the results obtained from the query [individual, *, *]
	 * @throws KPIModelException when cannot populate a given individual
	 */
	private void populateIndividual(AbstractOntConcept individual, String results) throws KPIModelException {
		try {
			populateIndividual(individual, results, null);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Cannot populate individual " + individual._getIndividualID() + " with properties", ex.getCause());
		}
	}

	/**
	 * Populates the individual with the properties obtained in the result. Take
	 * into account that this population is made recursively
	 * 
	 * @param individual     the individual to be populated
	 * @param results        the results obtained from the query [individual, *, *]
	 * @param subscriptionId the subscription identifier is needed to associate to a
	 *                       subscription. <code>null</code> value is used for no
	 *                       subscription
	 * @throws KPIModelException when cannot populate a given individual
	 */
	private void populateIndividual(AbstractOntConcept individual, String results, Long subscriptionId)
			throws KPIModelException {
		try {
			// Parses the results
			M3Parser parser = new M3Parser();
			RDFM3 m3 = parser.parse(results);

			// A set of M3-based properties
			ArrayList<ResultsProperty> individualProperties = new ArrayList<ResultsProperty>();

			// In our case, it is the subject the one that has the subclasses uris
			for (RDFM3Triple triple : m3.listTriples()) {
				// Gets the individual properties (except those that declare a type)
				if (!triple.getPredicate().equals(RDF_TYPE)) {
					// Create a new property
					ResultsProperty property = new ResultsProperty(triple.getPredicate());
					if (!individualProperties.contains(property)) {
						// Add the corresponding value
						if (triple.isObjectLiteral()) {
							property.addNewValue(triple.getObjectDataContent());
						} else {
							property.addNewValue(triple.getObject());
						}

						// adds to the individual properties
						individualProperties.add(property);
					} else {
						// find the property and add the new value
						for (ResultsProperty subscriptionProp : individualProperties) {
							if (subscriptionProp.getPropertyIRI().equals(triple.getPredicate())) {
								if (triple.isObjectLiteral()) {
									subscriptionProp.addNewValue(triple.getObjectDataContent());
								} else {
									subscriptionProp.addNewValue(triple.getObject());
								}
							}
						}
					}
				}
			}

			// Determine the type of properties
			if (individualProperties.size() > 0) {
				determineTypeOfProperties(individualProperties);
			}

			// assign property values to the individual
			assignPropertyValuesToIndividual(individual, individualProperties, subscriptionId);

		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Cannot populate individual " + individual._getIndividualID() + " with properties", ex.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Some error ocurred during property population of individual " + individual._getIndividualID(),
					ex.getCause());
		}
	}

	/**
	 * Assigns values to a given individual
	 * 
	 * @param individual   the individual to populate
	 * @param properties   the set of M3Properties with the values
	 * @param subscription a boolean value that determines if assignment of object
	 *                     property values require subscription
	 * @throws KPIModelException when cannot populate a given individual
	 */
	private void assignPropertyValuesToIndividual(AbstractOntConcept individual, ArrayList<ResultsProperty> properties,
			Long subscriptionId) throws KPIModelException {
		try {
			for (ResultsProperty subsProp : properties) { // for each of the found properties
				updateIndividual(individual, subsProp, subscriptionId);
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Cannot populate individual " + individual._getIndividualID() + " with properties", ex.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException(
					"Some error ocurred during property population of individual " + individual._getIndividualID(),
					ex.getCause());
		}
	}

	/**
	 * Determines the type of the properties for input properties.
	 * 
	 * @param properties a set of M3Properties in what the type is not still defined
	 * @return the set of properties with the datatype, object and functional values
	 *         set
	 * @throws KPIModelException when something was wrong in determining the
	 *                           ontological characteristics of the properties
	 */
	private void determineTypeOfProperties(ArrayList<ResultsProperty> properties) throws KPIModelException {

		try {

			for (ResultsProperty invididualProperty : properties) {
				for (PropertyCharacteristic property : propertiesCharacteristics) {
					if (property.getPropertyIRI().equals(invididualProperty.getPropertyIRI())) {
						invididualProperty.setFunctional(property.isFunctional());
						invididualProperty.setDatatypeProperty(property.isDatatype());
						invididualProperty.setObjectProperty(property.isObject());
						invididualProperty.setInitialized(true);
					}
				}
			}

			// Create a ComposedPendingTask to send all queries in this step
			ComposedPendingTask cpt = new ComposedPendingTask();

			for (ResultsProperty property : properties) {
				if (!property.isInitialized()) { // we should get that data from the SIB
					// Create one task per query
					cpt.addPendingSubtask(model.getSIB().query(
							this.getPropertyTypeQuery(property.getPropertyIRI()).toString(), TypeAttribute.RDFM3));
				}
			}

			if (cpt.size() == 0) {
				return;
			}

			model.getSIB().publish();
			cpt.waitUntilFinished(model.getTimeout());

			if (!cpt.isFinished()) { // the timeout has expired
				throw new KPIModelException(
						"Cannot get the ontological characteristics of the properties due to the timeout has been exceded");
			}

			// Check the correctness of each of the tasks in the composite task
			for (PendingTask qpt : cpt.listPendingTasks()) {
				// Gets the status of the response
				SIBStatus status = (SIBStatus) qpt.getProperty("status");

				if (!status.equals(SIBStatus.STATUS_OK)) {
					throw new KPIModelException(
							"The ontological characteristics of properties cannot be executed due to "
									+ status.getValue());
				}

				// gets the set of results
				String results = (String) qpt.getProperty("results");

				if ((results != null) && !results.equals("")) { // if the results are not null or void
					// Parses the results
					M3Parser parser = new M3Parser();
					RDFM3 m3 = parser.parse(results);

					for (RDFM3Triple triple : m3.listTriples()) {
						// gets the triples associated, where the subject corresponds to the instance
						// reference
						// and the object corresponds to the class reference
						if (triple.getObject().equals(DATATYPE_PROPERTY)) {
							for (ResultsProperty subsProp : properties) {
								if (subsProp.getPropertyIRI().equals(triple.getSubject())) {
									subsProp.setDatatypeProperty(true);
								}
							}
						} else if (triple.getObject().equals(OBJECT_PROPERTY)) {
							for (ResultsProperty subsProp : properties) {
								if (subsProp.getPropertyIRI().equals(triple.getSubject())) {
									subsProp.setObjectProperty(true);
								}
							}
						} else if (triple.getObject().equals(FUNCTIONAL_PROPERTY)) {
							for (ResultsProperty subsProp : properties) {
								if (subsProp.getPropertyIRI().equals(triple.getSubject())) {
									subsProp.setFunctional(true);
								}
							}
						} else {
							Logger.debug("Incorrect property: " + triple.getObject());
						}
					}
				}

				// stores the types of properties in order not to get this data from sib once
				// and again
				for (ResultsProperty subsProp : properties) {
					if (!propertiesCharacteristics.contains(new PropertyCharacteristic(subsProp.getPropertyIRI()))) {
						PropertyCharacteristic resolvedPropertyCharacteristic = new PropertyCharacteristic(subsProp);
						propertiesCharacteristics.add(resolvedPropertyCharacteristic);

					}
				}
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot set property ontological characteristics", ex.getCause());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot set property ontological characteristics", ex.getCause());
		}
	}

	/**
	 * Gets the class of a given individual
	 * 
	 * @param ontIndividualURL an URL representing a individual
	 * @return the class to what belongs the individual
	 * @throws KPIModelException when some error arise
	 */
	private String getClassOf(String ontIndividualURL) throws KPIModelException {

		try {
			// Create a QUERY REQUEST asking about the individual type
			PendingTask pt = model.getSIB().query(this.getIndividualTypeQuery(ontIndividualURL).toString(),
					TypeAttribute.RDFM3); // [ind, type, *]

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());

			if (!pt.isFinished()) { // the timeout has expired
				throw new KPIModelException("The query finished by timeout");
			}

			// Gets the status of the response
			SIBStatus status = (SIBStatus) pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The query cannot be executed due to " + status.getValue());
			}

			// Gets the results parameter from the QUERY RESPONSE
			String results = (String) pt.getProperty("results");

			if ((results != null) && !results.equals("")) {
				// Parses the results
				M3Parser parser = new M3Parser();
				RDFM3 m3 = parser.parse(results);

				for (RDFM3Triple triple : m3.listTriples()) {
					// gets the triples associated, where the subject corresponds to the instance
					// reference
					// and the object corresponds to the class reference

					// TODO [FJR] One individual can belong to various classes. check this
					return model.getConceptName(triple.getObject());
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot get the class of " + ontIndividualURL, ex.getCause());
		}

		// if no triple is obtained, then return null value indicating the error
		return null;
	}

	/**
	 * Removes the derived subscriptions of a given individual
	 * 
	 * @param individual an AbstractOntConcept representing an individual
	 * @throws KPIModelException when cannot remove derived subscriptions
	 */
	private void removeDerivedSubscriptions(final AbstractOntConcept individual, final Long parentSubscriptionId) {
		try {
			// Subscriptions to be removed
			ArrayList<Long> subscriptionsToBeRemoved = new ArrayList<Long>();

			for (Long subscriptionId : derivedSubscriptions.keySet()) {
				AbstractOntConcept subscribedConcept = derivedSubscriptions.get(subscriptionId);
				if (subscribedConcept.equals(individual)) {
					// the subscription is no longer a valid subscription
					subscriptionsToBeRemoved.add(subscriptionId);
				}
			}

			// Remove also subscriptions associated to the individual
			ArrayList<IndividualRelationship> individualRelationshipsToRemove = new ArrayList<IndividualRelationship>();
			for (IndividualRelationship individualRelationship : individualRelationships) {
				if (individualRelationship.getSubjectIRI().equals(individual._getIndividualIRI())) {
					// check that there is no other relationship associtated to the destionation IRI
					individualRelationshipsToRemove.add(individualRelationship);
				}
			}

			// Here assure that the individual relationship to remove is not related with
			// any other relationship
			for (IndividualRelationship individualRelationshipToRemove : individualRelationshipsToRemove) {
				Long destionationSubscriptionId = individualRelationshipToRemove.getObjectSubscriptionId();

				individualRelationships.remove(individualRelationshipToRemove);

				boolean related = false;
				for (IndividualRelationship individualRelationship : individualRelationships) {
					if (individualRelationship.getObjectSubscriptionId().equals(destionationSubscriptionId)) {
						related = true;
					}
				}

				if (!related) { // just add for being removed
					subscriptionsToBeRemoved.add(destionationSubscriptionId);
				}
			}

			for (Long subscriptionId : subscriptionsToBeRemoved) {
				this.removeSubscription(subscriptionId, parentSubscriptionId);
			}
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			Logger.error("Cannot remove derived subscriptions: " + ex.getMessage());
		}
	}

	/**
	 * Removes a determinate subscription from the list of subscriptions
	 * 
	 * @param subscriptionId a long with the subscription identifier
	 * @return <code>true</code> if the subscription is successfully removed
	 */
	private boolean removeSubscription(Long subscriptionId, Long parentSubscriptionId) throws KPIModelException {
		try {
			// Create a QUERY REQUEST asking about the individual type
			PendingTask pt = model.getSIB().unsubscribe(subscriptionId);

			model.getSIB().publish();
			pt.waitUntilFinished(model.getTimeout());

			if (!pt.isFinished()) { // the timeout has expired
				throw new KPIModelException("The query finished by timeout");
			}

			// Gets the status of the response
			SIBStatus status = (SIBStatus) pt.getProperty("status");

			if (!status.equals(SIBStatus.STATUS_OK)) {
				throw new KPIModelException("The query cannot be executed due to " + status.getValue());
			}

			AbstractOntConcept removedIndividual = derivedSubscriptions.remove(subscriptionId);
			this.model.getSIB().removeSubscriptionsListener(subscriptionId);

			if (removedIndividual != null) {
				if (parentSubscriptionId != null) {
					this.subscriptionIndividuals.get(parentSubscriptionId).remove(removedIndividual);
				}
			}

			return true;
		} catch (KPISSAPException ex) {
			ex.printStackTrace();
			throw new KPIModelException("Cannot unsubscribe from subscription " + subscriptionId, ex.getCause());
		} catch (KPIModelException ex) {
			ex.printStackTrace();
			throw new KPIModelException("There was an error in unsubscribe from subscription " + subscriptionId,
					ex.getCause());
		}
	}

	/**
	 * Gets a subscription individual
	 * 
	 * @param subscriptionId the subscription identifier
	 * @param individualIRI  the individual iri
	 * @return an AbstractOntConcept with the subscribed individual
	 */
	private AbstractOntConcept getSubscriptionIndividual(long subscriptionId, String individualIRI) {

		if (subscriptionIndividuals.get(subscriptionId) != null) {
			for (AbstractOntConcept individual : subscriptionIndividuals.get(subscriptionId)) {
				if (individual._getIndividualIRI().equals(individualIRI)) {
					return individual;
				}
			}
		}
		return null;
	}

	/**
	 * Determines if the triple property is an rdf:type property
	 * 
	 * @param triple a triple in RDFM3Triple format
	 * @return <code>true</code> if the property is an rdf:type property
	 */
	private boolean isRDFType(RDFM3Triple triple) {
		return triple.getPredicate().equals(RDF_TYPE);
	}

	private boolean hasContentRDFM3(String results) {
		if ((results == null) || results.equals("")) {
			return false;
		}

		try {
			M3Parser parser = new M3Parser();
			RDFM3 m3 = parser.parse(results);
			return m3.hasTriples();
		} catch (RDFM3ParseException e) {
			e.printStackTrace();
		}

		return false;
	}
}
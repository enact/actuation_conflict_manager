package ENACTConsumer.logic;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Observer;
import java.util.UUID;

import org.smool.kpi.model.exception.KPIModelException;

import ENACTConsumer.api.Consumer;
import ENACTConsumer.api.HumiditySensorSubscription;
import ENACTConsumer.api.LightingSensorSubscription;
import ENACTConsumer.api.PresenceSensorSubscription;
import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.api.TemperatureSensorSubscription;
import ENACTConsumer.model.smoolcore.IContinuousInformation;
import ENACTConsumer.model.smoolcore.impl.BlindPositionActuator;
import ENACTConsumer.model.smoolcore.impl.ContinuousInformation;
import ENACTConsumer.model.smoolcore.impl.HumiditySensor;
import ENACTConsumer.model.smoolcore.impl.LightingSensor;
import ENACTConsumer.model.smoolcore.impl.PresenceSensor;
import ENACTConsumer.model.smoolcore.impl.SecurityAuthorization;
import ENACTConsumer.model.smoolcore.impl.SmokeSensor;
import ENACTConsumer.model.smoolcore.impl.TemperatureSensor;
import ENACTConsumer.model.smoolcore.IMessage;
import ENACTConsumer.model.smoolcore.impl.Message;
import ENACTConsumer.model.smoolcore.impl.MessageReceiveSensor;
import ENACTConsumer.api.MessageReceiveSensorSubscription;
import ENACTConsumer.api.Producer;

import org.eclipse.paho.client.mqttv3.*;

import ENACTConsumer.logic.ACM_GeneSIS_Demo_Common;

/**
 * Subscribe to data generated from KUBIK smart building.
 * <p>
 * This class should be the skeleton for the IoT application in the SB use case
 * </p>
 * <p>
 * The app is retrieving temperature and other data. When temp is higher or
 * lower than comfort, an actuation order is sent back to the Smart Building to
 * turn the temperature back to normal.
 * </p>
 *
 */
public class EnactConsumerMain implements MqttCallback {
	public static final String vendor = "CNRS";
	public static final String name = "ACM_GeneSIS_Demo";
	public static final String nameKP = name + "2";

	public static MessageReceiveSensor microphone_replay;
	public static MessageReceiveSensor botvacD3_command;
	public static MessageReceiveSensor sensor_cec_source;
	public static MessageReceiveSensor cec_power;
	public static MessageReceiveSensor microphone_record;
	public static MessageReceiveSensor x_sensor_luminance;
	public static MessageReceiveSensor camera_detection;
	public static MessageReceiveSensor botvacD3_state;
	public static MessageReceiveSensor botvacD3_action;
	public static MessageReceiveSensor botvacD3_is_docked;
	public static MessageReceiveSensor botvacD3_is_scheduled;
	public static MessageReceiveSensor botvacD3_is_charging;
	public static MessageReceiveSensor microphone_sound;
	public static MessageReceiveSensor microphone_zcr;
	public static MessageReceiveSensor microphone_mfcc;
	public static MessageReceiveSensor microphone_time;
	public static MessageReceiveSensor cec_status;
	public static MessageReceiveSensor actuator_cec_source;	

	public IMqttClient publisher; 
    
    private SecurityAuthorization sec;
    private int counter=0;	
    //hard code the JWT token for now
  	private final String token = "eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJzdWIiOiJFbmFjdENvbnN1bWVyIiwib2JqIjoiQmxpbmRQb3NpdGlvbkFjdHVhdG9yIiwiYWN0Ijoid3JpdGUiLCJpYXQiOjE1OTc3NTE1NzEsImV4cCI6MTU5Nzc1MjE3MX0.";
  	
	private Producer producer;

	CustomActuation botvacD3_command_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.botvacD3_command_id, nameKP, vendor);
	CustomActuation microphone_replay_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_replay_id, nameKP, vendor);
	CustomActuation actuators_cec_power_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.actuators_cec_power_id, nameKP, vendor);
	CustomActuation actuators_cec_source_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.actuators_cec_source_id, nameKP, vendor);
	CustomActuation microphone_record_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_record_id, nameKP, vendor);
	CustomActuation switch_dimmer_1_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1, nameKP, vendor);
	CustomActuation switch_dimmer_2_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2, nameKP, vendor);
		

	public EnactConsumerMain(String sib, String addr, int port) throws Exception {

		SmoolKP.setKPName(nameKP);
		System.out.println("*** " + nameKP + " ***");
		// ------------PREPARE ACTUATION OBJECT--------------------
//		actuation = new CustomActuation(name);

		// ---------------------------CONNECT TO SMOOL---------------------
		SmoolKP.connect(sib, addr, port);
		
		//Create an MQTT client here
		initMQTTclient();

		// ---------------------------DATA----------------------
		Consumer consumer = SmoolKP.getConsumer();
		microphone_replay = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_replay_id);
		botvacD3_command = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id);
		actuator_cec_source = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_source_id);
		cec_power = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_power_id);
		microphone_record = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_record_id);
		ACM_GeneSIS_Demo_Common.switch_dimmer_1 = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1);
		ACM_GeneSIS_Demo_Common.switch_dimmer_2 = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2);

		x_sensor_luminance= new MessageReceiveSensor(name + "_x_sensor_luminance");
		camera_detection= new MessageReceiveSensor(name + "_camera_detection");
		botvacD3_state= new MessageReceiveSensor(name + "_botvacD3_state");
		botvacD3_action= new MessageReceiveSensor(name + "_botvacD3_action");
		botvacD3_is_docked= new MessageReceiveSensor(name + "_botvacD3_is_docked");
		botvacD3_is_scheduled= new MessageReceiveSensor(name + "_botvacD3_is_scheduled");
		botvacD3_is_charging= new MessageReceiveSensor(name + "_botvacD3_is_charging");
		microphone_sound= new MessageReceiveSensor(name + "_microphone_sound");
		microphone_zcr= new MessageReceiveSensor(name + "_microphone_zcr");
		microphone_mfcc= new MessageReceiveSensor(name + "_microphone_mfcc");
		microphone_time= new MessageReceiveSensor(name + "_microphone_time");
		cec_status= new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_status_id);
		sensor_cec_source= new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_source_id);	


		String timestamp = Long.toString(System.currentTimeMillis());

		Message msg = new Message();
		msg.setBody("initialisation_SMOOL_KP");
		msg.setTimestamp(timestamp);

		this.producer = SmoolKP.getProducer();

		//Initialization produce data
		//ARF-this is only to send a message, comment it if you have mqtt enabled
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_replay_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_power_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_source_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_record_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2, nameKP, vendor, null, null, null, msg, null);
		

		
		//initialization receive data
		MessageReceiveSensorSubscription subscription = new MessageReceiveSensorSubscription(createObserver());
		//consumer.subscribeToMessageReceiveSensor(subscription, null);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_luminance_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.camera_detection_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_state_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_action_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_is_docked_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_is_charging_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_sound_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_zcr_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_mfcc_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_time_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensors_cec_status_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensors_cec_source_id);
		
		// -----------ATTACH WATCHDOG instead of SLEEP-------
		SmoolKP.watchdog(3000); // maximum interval that at least one message should arrive
	}
	
	/**
	 * Create an MQTT client here just for testing purpose: 
	 * Whenever there is a message from SMOOL send it via MQQT to apps
	 * Whenever there is an actuation order from app via MQTT send it to SMOOL. 
	 * @throws InterruptedException 
	 */
	private void initMQTTclient() throws InterruptedException {
		try{
        	// ---------------------------MQTT Client----------------------------------
			// Receive actuation orders and publish them on SMOOL
    		String publisherId = UUID.randomUUID().toString();
    		publisher = new MqttClient("tcp://192.168.0.21:1883", publisherId);
    		
            
            MqttConnectOptions options = new MqttConnectOptions();
            options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
			publisher.setCallback(this);
            publisher.connect(options);
			System.out.println("Connected to the Apps broker");

			// All subscriptions
			String myTopic = "enact/sensors/microphone/replay";
			MqttTopic topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/neato/botvacD3/command";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/cec/source";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/cec/power";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/microphone/record";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/wlights/1/switch_dimmer";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/wlights/2/switch_dimmer";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);
		
            
		}catch(MqttException e){
			System.out.println("WAITING for the CONNECTION to the broker to the Apps...");
			//initMQTTclient();
            //throw new RuntimeException("Exception occurred in creating MQTT Client");
        }catch(Exception e) {
        	//Unable to connect to server (32103) - java.net.ConnectException: Connection refused
        	e.printStackTrace();
			System.exit(1);
        }
	}
	
	/**
	FROM APP to SMOOL
	*/
	 public void connectionLost(Throwable t) {
        System.out.println("Connection lost!");
        // code to reconnect to the broker would go here if desired
        try {
        	System.out.println("RECONNECTING to the broker of the Apps");
			initMQTTclient();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public  void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
    	
		//sec.setData(token).setTimestamp(Long.toString(System.currentTimeMillis()));
		//blindPos.setSecurityData(sec).setTimestamp(Long.toString(System.currentTimeMillis()));
    	
    	//we should send it to Smool Server
		String value_blind=new String(mqttMessage.getPayload());        
		String timestamp = Long.toString(System.currentTimeMillis());
			
		String message_payload = new String(mqttMessage.getPayload());
		Message msg_with_timestamp = new Message();
		msg_with_timestamp.setBody(message_payload);
		msg_with_timestamp.setTimestamp(timestamp);

		System.out.println("-------------------------------------------------");
		System.out.println("Received actuator message from "+ s +" (APPs): " + message_payload);
		System.out.println("-------------------------------------------------");
		
		try{
			switch(s){
				case "enact/actuators/neato/botvacD3/command":
					new Thread(() -> botvacD3_command_ca.run(msg_with_timestamp)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(botvacD3_command._getIndividualID(), nameKP, vendor, null, null, null, msg_with_timestamp, null);
					break;
				case "enact/actuators/microphone/record":
					new Thread(() -> microphone_record_ca.run(msg_with_timestamp)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(microphone_record._getIndividualID(), nameKP, vendor, null, null, null, msg_with_timestamp, null);
					break;
				case "enact/sensors/microphone/replay":
					new Thread(() -> microphone_replay_ca.run(msg_with_timestamp)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(microphone_replay._getIndividualID(), nameKP, vendor, null, null, null, msg_with_timestamp, null);
					break;
				case "enact/actuators/cec/source":
					new Thread(() -> actuators_cec_source_ca.run(msg_with_timestamp)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(sensor_cec_source._getIndividualID(), nameKP, vendor, null, null, null, msg_with_timestamp, null);
					break;
				case "enact/actuators/cec/power":
					new Thread(() -> actuators_cec_power_ca.run(msg_with_timestamp)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(cec_power._getIndividualID(), nameKP, vendor, null, null, null, msg_with_timestamp, null);
					break;
				case "enact/actuators/wlights/1/switch_dimmer":
					new Thread(() -> switch_dimmer_1_ca.run(msg_with_timestamp)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_1._getIndividualID(), nameKP, vendor, null, null, null, msg_with_timestamp, null);
					break;
				case "enact/actuators/wlights/2/switch_dimmer":
					new Thread(() -> switch_dimmer_2_ca.run(msg_with_timestamp)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_2._getIndividualID(), nameKP, vendor, null, null, null, msg_with_timestamp, null);
					break;
				default:
					System.out.println("Unknown topic");
			}

		}catch (Exception e){
			e.printStackTrace();
		}
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        //System.out.println("Pub complete" + new String(token.getMessage().getPayload()));

    }

    private Observer createObserverDummy() {
		return (o, concept) -> {
			MessageReceiveSensor sensor = (MessageReceiveSensor) concept;
			IMessage msg = sensor.getMessage();
			if(msg==null)System.err.println("a  SENSOR is sending null messages" +sensor._getIndividualID());
			else System.out.println("Message arrived: "+msg.getBody());
		};
    }
    /**
	FROM SMOOL to APP
	 */

    private Observer createObserver() {
		return (o, concept) -> {
			MessageReceiveSensor sensor = (MessageReceiveSensor) concept;
			IMessage msg_sensor = sensor.getMessage();


			if(sensor.getDeviceID().equals(ACM_GeneSIS_Demo_Common.name)) {

				if(msg_sensor.getBody() != null){
					MqttMessage msg=null;
					
					msg = new MqttMessage(msg_sensor.getBody().getBytes());
					
					if(msg!=null){
						msg.setQos(0);
						msg.setRetained(true);
						try {
							if(publisher.isConnected()) {
								//whenever there is sensor data update sent from SMOOL server, send it to the Apps via MQTT broker 
								System.out.println("-------------------------------------------------");
								System.out.println("Is Forwarding sensor "+sensor._getIndividualID()+" message to APPs: " + new String(msg.getPayload()));
								System.out.println("-------------------------------------------------");

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_luminance_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensor_luminance_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.camera_detection_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.camera_detection_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_state_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_state_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_action_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_action_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_sound_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.microphone_sound_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_zcr_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.microphone_zcr_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_mfcc_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.microphone_mfcc_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_time_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.microphone_time_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensors_cec_status_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensors_cec_status_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensors_cec_source_id)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensors_cec_source_topic, msg);
								}

							}else {
								System.out.println("Cannot send to Apps: publisher.isConnected() = " + publisher.isConnected());
							}
						} catch (MqttPersistenceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (MqttException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}    
					}
				}

			}

		};
	}


	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		while (true) {
			try {
				new EnactConsumerMain(sib, addr, port);
			} catch (KPIModelException | IOException e) {
				e.printStackTrace();
				Thread.sleep(3000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}

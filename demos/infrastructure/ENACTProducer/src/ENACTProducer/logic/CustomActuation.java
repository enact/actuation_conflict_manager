package ENACTProducer.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ENACTProducer.model.smoolcore.IMessage;
import ENACTProducer.model.smoolcore.impl.Message;

import ENACTProducer.api.SmoolKP;

class CustomActuation {
    private boolean isFirstActuation = true;
    private String vendor;
    private String id;
    private String kpName;

    public CustomActuation(String id, String kpName, String vendor) {
        this.id=id;
        this.vendor=vendor;
        this.kpName=kpName;
    }

    public synchronized void run(Message msg) {
		try {
            Thread.sleep(100);
            System.out.println(id+ " "+this.isFirstActuation);
            if(msg != null){
                if (isFirstActuation) {
                    SmoolKP.getProducer().createMessageReceiveSensor(id, kpName, vendor, null, null, null, msg, null);
                    this.isFirstActuation=false;
                }else{
                    SmoolKP.getProducer().updateMessageReceiveSensor(id, kpName, vendor, null, null, null, msg, null);
                    System.out.println("Message updated");
                }
            }else{
                System.out.println(id+ " is sending null");
            }
        } catch (Exception e) {
			System.out.println("Error: the actuation order cannot be sent. " + e.getMessage());
            System.out.println("Error: the actuation order cannot be sent. id: "+id+" - "+ e.getMessage());
		}
	}

}
package ENACTProducer.logic;

import java.io.IOException;
import java.util.Observer;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.smool.kpi.model.exception.KPIModelException;

import ENACTProducer.api.Consumer;
import ENACTProducer.api.MessageReceiveSensorSubscription;
import ENACTProducer.api.Producer;
import ENACTProducer.api.SmoolKP;
import ENACTProducer.model.smoolcore.IMessage;
import ENACTProducer.model.smoolcore.impl.Message;
import ENACTProducer.model.smoolcore.impl.MessageReceiveSensor;

import ENACTProducer.logic.ACM_GeneSIS_Demo_Common;

/**
 * Producer of data for the Smart Building use case for Enact.
 * <p>
 * NOTE: although this app is mainly a producer of sensors data, it can receive
 * actuation orders.
 * </p>
 * <p>
 * This app is sending (via SMOOL) the temperature, gas, etc stataus in the
 * smart building. It is also receiving (via SMOOL) actuation orders, and checking if security data is valid and then it calls
 * the Smart Building SCADA to tune the values.
 * </p>
 *
 */
public class EnactProducerMain {
	
	private MqttClientEnact m;
	

	public EnactProducerMain(String sib, String addr, int port) throws Exception {
		SmoolKP.setKPName(ACM_GeneSIS_Demo_Common.name);
		System.out.println("KPName*** " + ACM_GeneSIS_Demo_Common.kpName + " *** Vendor:" + ACM_GeneSIS_Demo_Common.vendor);
		// ---------------------------CONNECT TO SMOOL---------------------
		// SmoolKP.connect();
		// SmoolKP.connect("sib1", "172.24.5.151", 23000);
		// SmoolKP.connect("sib1", "192.168.1.128", 23000);
		SmoolKP.connect(sib, addr, port);
		
		//Create an MQTT client here
		//initMQTTclient();
		this.m = new MqttClientEnact();
		

		// ---------------------------CREATE SENSOR-----------------------------
		/*ACM_GeneSIS_Demo_Common.microphone_replay = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_replay_id);
		ACM_GeneSIS_Demo_Common.botvacD3_command = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id);
		ACM_GeneSIS_Demo_Common.actuators_cec_source = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_source_id);
		ACM_GeneSIS_Demo_Common.actuators_cec_power = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_power_id);
		ACM_GeneSIS_Demo_Common.microphone_record = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_record_id);
		ACM_GeneSIS_Demo_Common.switch_dimmer_1 = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1);
		ACM_GeneSIS_Demo_Common.switch_dimmer_2 = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2);
		*/
		ACM_GeneSIS_Demo_Common.sensor_luminance = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_luminance_id);
		ACM_GeneSIS_Demo_Common.camera_detection = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.camera_detection_id);
		ACM_GeneSIS_Demo_Common.botvacD3_state = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_state_id);
		ACM_GeneSIS_Demo_Common.botvacD3_action = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_action_id);
		ACM_GeneSIS_Demo_Common.botvacD3_is_docked = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_id);
		ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id);
		ACM_GeneSIS_Demo_Common.botvacD3_is_charging = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_id);
		ACM_GeneSIS_Demo_Common.microphone_sound = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_sound_id);
		ACM_GeneSIS_Demo_Common.microphone_zcr = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_zcr_id);
		ACM_GeneSIS_Demo_Common.microphone_mfcc = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_mfcc_id);
		ACM_GeneSIS_Demo_Common.microphone_time = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_time_id);
		ACM_GeneSIS_Demo_Common.botvacD3_command = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id);
		ACM_GeneSIS_Demo_Common.sensors_cec_status = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_status_id);
		ACM_GeneSIS_Demo_Common.sensors_cec_source = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_source_id);
		
//		switch_binary = new MessageSendActuator(name + "_switch_binary");

		

		// ---------------------------CONSUME ACTION----------------------------------
		Thread.sleep(1000);
		Consumer consumer = SmoolKP.getConsumer();
		
//		MessageReceiveSensorSubscription sensor_luminance_subscription = new MessageReceiveSensorSubscription();
//		consumer.subscribeToMessageReceiveSensor(sensor_luminance_subscription, sensor_luminance._getIndividualID());
		
		MessageReceiveSensorSubscription subscription = new MessageReceiveSensorSubscription(createObserver());
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_command_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_record_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_replay_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuators_cec_source_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuators_cec_power_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.switch_dimmer_id_1);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.switch_dimmer_id_2);
//		LightSwitchActuatorSubscription subscription_switch_binary = new LightSwitchActuatorSubscription(createObserver_switch_binary());
//		consumer.subscribeToLightSwitchActuator(subscription_switch_binary, switch_binary._getIndividualID());		


		// ---------------------SEND DATA for a testing purpose--------------------------------------------
		/*while (true) {
			Thread.sleep(1000);
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>");
			timestamp = Long.toString(System.currentTimeMillis());
			Message message = new Message();
			message.setBody("test");
			producer.updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_luminance._getIndividualID(), ACM_GeneSIS_Demo_Common.name, null, null, null, null, message, null);
			System.out.println("Producing " + ACM_GeneSIS_Demo_Common.sensor_luminance._getIndividualID() + " (and more concepts)");
			producer.updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.camera_detection._getIndividualID(), ACM_GeneSIS_Demo_Common.name, null, null, null, null, message, null);
			System.out.println("Producing " + ACM_GeneSIS_Demo_Common.camera_detection._getIndividualID() + " (and more concepts)");
		}*/
		
		// -----------ATTACH WATCHDOG instead of SLEEP-------
		SmoolKP.watchdog(3000); // maximum interval that at least one message should arrive
	}
	
	

    /**
	 * Processs messages related to any Actuator of ACM_GeneSIS_Demo
	 */
	private Observer createObserver() {
		return (o, concept) -> {
			MessageReceiveSensor actuator = (MessageReceiveSensor) concept;
			IMessage msg_actuator = actuator.getMessage();
			

			System.out.println(actuator._getIndividualID() + " receiving Actuator Message order. Value: " + msg_actuator.getBody() + " "+ACM_GeneSIS_Demo_Common.name2 + " == "+actuator.getDeviceID());

			//ONLY If the actuator does belong to name="ACM_GeneSIS_Demo", we will process further
			if(actuator.getDeviceID().startsWith(ACM_GeneSIS_Demo_Common.name2)) {
				
				if(msg_actuator.getBody() != null){
					MqttMessage msg=null;
					
					msg = new MqttMessage(msg_actuator.getBody().getBytes());
					
					if(msg!=null){
						msg.setQos(0);
						//msg.setRetained(true);
						
						try {
							if(m.publisher.isConnected()) {
								//whenever there is sensor data update sent from SMOOL server, send it to the Apps via MQTT broker 
								System.out.println("-------------------------------------------------");
								System.out.println("Is Forwarding actuator Cmd to SMART BUILDING: " + new String(msg.getPayload()));
								System.out.println("-------------------------------------------------");
								
								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_record_id)) {
									m.publisher.publish(ACM_GeneSIS_Demo_Common.microphone_record_topic, msg);
								}
								
								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_command_id)) {
									m.publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_command_topic, msg);
								}
								
								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_replay_id)) {
									m.publisher.publish(ACM_GeneSIS_Demo_Common.microphone_replay_topic, msg);
								}
								
								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuators_cec_source_id)) {
									m.publisher.publish(ACM_GeneSIS_Demo_Common.actuators_cec_source_topic, msg);
								}
								
								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuators_cec_power_id)) {
									m.publisher.publish(ACM_GeneSIS_Demo_Common.actuators_cec_power_topic, msg);
								}

								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1)) {
									m.publisher.publish(ACM_GeneSIS_Demo_Common.switch_dimmer_topic_1, msg);
								}

								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2)) {
									m.publisher.publish(ACM_GeneSIS_Demo_Common.switch_dimmer_topic_2, msg);
								}
								
							}else {
								System.out.println("Cannot send to SMART BUILDING: publisher.isConnected() = " + m.publisher.isConnected());
							}
						} catch (MqttPersistenceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (MqttException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}    
					}
				}
			}
		};
	}


	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		// Logger.setDebugging(true);
		// Logger.setDebugLevel(4);
		while (true) {
			try {
				new EnactProducerMain(sib, addr, port);
			} catch (KPIModelException | IOException e) {
				e.printStackTrace();
				Thread.sleep(1000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}

package ENACTConsumer.logic;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Observer;
import java.util.UUID;

import org.smool.kpi.model.exception.KPIModelException;

import ENACTConsumer.api.Consumer;
import ENACTConsumer.api.HumiditySensorSubscription;
import ENACTConsumer.api.LightingSensorSubscription;
import ENACTConsumer.api.PresenceSensorSubscription;
import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.api.TemperatureSensorSubscription;
import ENACTConsumer.model.smoolcore.IContinuousInformation;
import ENACTConsumer.model.smoolcore.impl.BlindPositionActuator;
import ENACTConsumer.model.smoolcore.impl.ContinuousInformation;
import ENACTConsumer.model.smoolcore.impl.HumiditySensor;
import ENACTConsumer.model.smoolcore.impl.LightingSensor;
import ENACTConsumer.model.smoolcore.impl.PresenceSensor;
import ENACTConsumer.model.smoolcore.impl.SecurityAuthorization;
import ENACTConsumer.model.smoolcore.impl.SmokeSensor;
import ENACTConsumer.model.smoolcore.impl.TemperatureSensor;
import ENACTConsumer.model.smoolcore.IMessage;
import ENACTConsumer.model.smoolcore.impl.Message;
import ENACTConsumer.model.smoolcore.impl.MessageReceiveSensor;
import ENACTConsumer.api.MessageReceiveSensorSubscription;
import ENACTConsumer.api.Producer;

import org.eclipse.paho.client.mqttv3.*;

import ENACTConsumer.logic.ACM_GeneSIS_Demo_Common;

/**
 * Subscribe to data generated from KUBIK smart building.
 * <p>
 * This class should be the skeleton for the IoT application in the SB use case
 * </p>
 * <p>
 * The app is retrieving temperature and other data. When temp is higher or
 * lower than comfort, an actuation order is sent back to the Smart Building to
 * turn the temperature back to normal.
 * </p>
 *
 */
public class EnactConsumerMain {
	public static final String vendor = "CNRS";
	public static final String name = "ACM_GeneSIS_Demo";
	public static final String nameKP = "ACM_GeneSIS_Demo";

	//for brokering to NodeRED apps
	public IMqttClient publisher; 
	private MqttClientEnact m;
    
    private SecurityAuthorization sec;
    private int counter=0;	
    //hard code the JWT token for now
  	private final String token = "eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJzdWIiOiJFbmFjdENvbnN1bWVyIiwib2JqIjoiQmxpbmRQb3NpdGlvbkFjdHVhdG9yIiwiYWN0Ijoid3JpdGUiLCJpYXQiOjE1OTc3NTE1NzEsImV4cCI6MTU5Nzc1MjE3MX0.";
  	
	private Producer producer;

	public EnactConsumerMain(String sib, String addr, int port) throws Exception {

		SmoolKP.setKPName(nameKP);
		System.out.println("*** " + nameKP + " ***");
		// ------------PREPARE ACTUATION OBJECT--------------------
//		actuation = new CustomActuation(name);

		// ---------------------------CONNECT TO SMOOL---------------------
		SmoolKP.connect(sib, addr, port);
		
		//Create an MQTT client here
		//initMQTTclient();
		//this.m = new MqttClientEnact();

		ACM_GeneSIS_Demo_Common.microphone_replay = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_replay_id);
		ACM_GeneSIS_Demo_Common.botvacD3_command = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id);
		ACM_GeneSIS_Demo_Common.actuators_cec_source = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_source_id);
		ACM_GeneSIS_Demo_Common.actuators_cec_power = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_power_id);
		ACM_GeneSIS_Demo_Common.microphone_record = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_record_id);
		ACM_GeneSIS_Demo_Common.switch_dimmer_1 = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1);
		ACM_GeneSIS_Demo_Common.switch_dimmer_2 = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2);
		
		/*ACM_GeneSIS_Demo_Common.sensor_luminance = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_luminance_id);
		ACM_GeneSIS_Demo_Common.camera_detection = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.camera_detection_id);
		ACM_GeneSIS_Demo_Common.botvacD3_state = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_state_id);
		ACM_GeneSIS_Demo_Common.botvacD3_action = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_action_id);
		ACM_GeneSIS_Demo_Common.botvacD3_is_docked = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_id);
		ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id);
		ACM_GeneSIS_Demo_Common.botvacD3_is_charging = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_id);
		ACM_GeneSIS_Demo_Common.microphone_sound = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_sound_id);
		ACM_GeneSIS_Demo_Common.microphone_zcr = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_zcr_id);
		ACM_GeneSIS_Demo_Common.microphone_mfcc = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_mfcc_id);
		ACM_GeneSIS_Demo_Common.microphone_time = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_time_id);
		ACM_GeneSIS_Demo_Common.botvacD3_command = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id);
		ACM_GeneSIS_Demo_Common.sensors_cec_status = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_status_id);
		ACM_GeneSIS_Demo_Common.sensors_cec_source = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_source_id);
		*/
		
		//this.producer = SmoolKP.getProducer();

		//Initialization produce data
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_replay_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_power_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_source_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_record_id, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1, nameKP, vendor, null, null, null, msg, null);
		//producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2, nameKP, vendor, null, null, null, msg, null);
		
		Thread.sleep(1000);
		Consumer consumer = SmoolKP.getConsumer();

		//initialization receive data
		MessageReceiveSensorSubscription subscription = new MessageReceiveSensorSubscription(createObserver());
		//consumer.subscribeToMessageReceiveSensor(subscription, null);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_luminance_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.camera_detection_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_state_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_action_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_is_docked_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.botvacD3_is_charging_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_sound_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_zcr_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_mfcc_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.microphone_time_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensors_cec_status_id);
		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensors_cec_source_id);

		// -----------ATTACH WATCHDOG instead of SLEEP-------
		SmoolKP.watchdog(3000); // maximum interval that at least one message should arrive
	}
	
	
    /**
	FROM SMOOL to APP
	 */

    private Observer createObserver() {
		return (o, concept) -> {
			MessageReceiveSensor sensor = (MessageReceiveSensor) concept;
			IMessage msg_sensor = sensor.getMessage();

			System.out.println(sensor._getIndividualID() + " receiving Actuator Message order. Value: " + msg_sensor.getBody() + " "+ACM_GeneSIS_Demo_Common.name + " == "+sensor.getDeviceID());

			if(sensor.getDeviceID().startsWith(ACM_GeneSIS_Demo_Common.name)) {

				if(msg_sensor.getBody() != null){
					MqttMessage msg=null;
					
					msg = new MqttMessage(msg_sensor.getBody().getBytes());
					
					if(msg!=null){
						msg.setQos(0);
						msg.setRetained(true);
						
						try {
							if(this.m.publisher.isConnected()) {
								//whenever there is sensor data update sent from SMOOL server, send it to the Apps via MQTT broker 
								System.out.println("-------------------------------------------------");
								System.out.println("Is Forwarding sensor "+sensor._getIndividualID()+" message to APPs: " + new String(msg.getPayload()));
								System.out.println("-------------------------------------------------");

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_luminance_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.sensor_luminance_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.camera_detection_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.camera_detection_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_state_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_state_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_action_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_action_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_sound_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.microphone_sound_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_zcr_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.microphone_zcr_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_mfcc_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.microphone_mfcc_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.microphone_time_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.microphone_time_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensors_cec_status_id)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.sensors_cec_status_topic, msg);
								}

								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensors_cec_source)) {
									this.m.publisher.publish(ACM_GeneSIS_Demo_Common.sensors_cec_source_topic, msg);
								}

							}else {
								System.out.println("Cannot send to Apps: publisher.isConnected() = " + this.m.publisher.isConnected());
							}
						} catch (MqttPersistenceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (MqttException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}    
					}
				}

			}

		};
	}


	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		while (true) {
			try {
				new EnactConsumerMain(sib, addr, port);
			} catch (KPIModelException | IOException e) {
				e.printStackTrace();
				Thread.sleep(3000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}

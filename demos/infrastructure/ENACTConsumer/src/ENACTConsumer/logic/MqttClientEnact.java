package ENACTConsumer.logic;

import java.io.IOException;
import java.util.Observer;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.smool.kpi.model.exception.KPIModelException;

import ENACTConsumer.api.Consumer;
import ENACTConsumer.api.MessageReceiveSensorSubscription;
import ENACTConsumer.api.Producer;
import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.model.smoolcore.IMessage;
import ENACTConsumer.model.smoolcore.impl.Message;
import ENACTConsumer.model.smoolcore.impl.MessageReceiveSensor;

import ENACTConsumer.logic.ACM_GeneSIS_Demo_Common;

public class MqttClientEnact implements MqttCallback{

    public IMqttClient publisher; 
    CustomActuation botvacD3_command_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.botvacD3_command_id, EnactConsumerMain.nameKP, EnactConsumerMain.vendor);
	CustomActuation microphone_replay_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_replay_id, EnactConsumerMain.nameKP, EnactConsumerMain.vendor);
	CustomActuation actuators_cec_power_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.actuators_cec_power_id, EnactConsumerMain.nameKP, EnactConsumerMain.vendor);
	CustomActuation actuators_cec_source_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.actuators_cec_source_id, EnactConsumerMain.nameKP, EnactConsumerMain.vendor);
	CustomActuation microphone_record_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_record_id, EnactConsumerMain.nameKP, EnactConsumerMain.vendor);
	CustomActuation switch_dimmer_1_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1, EnactConsumerMain.nameKP, EnactConsumerMain.vendor);
	CustomActuation switch_dimmer_2_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2, EnactConsumerMain.nameKP, EnactConsumerMain.vendor);
	
	Message msg_with_timestamp_botvac = new Message();
	Message msg_with_timestamp_mic = new Message();
	Message msg_with_timestamp_repl = new Message();
	Message msg_with_timestamp_cec_source = new Message();
	Message msg_with_timestamp_cec_power = new Message();
	Message msg_with_timestamp_switch1 = new Message();
	Message msg_with_timestamp_switch2 = new Message();

    public MqttClientEnact() throws Exception{
        initMQTTclient();
    }

    /**
	 * Create an MQTT client here just for testing purpose: 
	 * Whenever there is a message from SMOOL send it via MQQT to apps
	 * Whenever there is an actuation order from app via MQTT send it to SMOOL. 
	 * @throws InterruptedException 
	 */
	private void initMQTTclient() throws InterruptedException {
		try{
        	// ---------------------------MQTT Client----------------------------------
			// Receive actuation orders and publish them on SMOOL
    		String publisherId = UUID.randomUUID().toString();
    		publisher = new MqttClient("tcp://192.168.0.21:1883", publisherId);
    		
            
            MqttConnectOptions options = new MqttConnectOptions();
            options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
			publisher.setCallback(this);
            publisher.connect(options);
			System.out.println("Connected to the Apps broker");

			// All subscriptions
			String myTopic = "enact/sensors/microphone/replay";
			MqttTopic topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/neato/botvacD3/command";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/cec/source";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/cec/power";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/microphone/record";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/wlights/1/switch_dimmer";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);

			myTopic = "enact/actuators/wlights/2/switch_dimmer";
			topic = publisher.getTopic(myTopic);
			publisher.subscribe(myTopic, 0);
		
            
		}catch(MqttException e){
			System.out.println("WAITING for the CONNECTION to the broker to the Apps...");
			//initMQTTclient();
            //throw new RuntimeException("Exception occurred in creating MQTT Client");
        }catch(Exception e) {
        	//Unable to connect to server (32103) - java.net.ConnectException: Connection refused
        	e.printStackTrace();
			System.exit(1);
        }
	}
	
	/**
	FROM APP to SMOOL
	*/
	 public void connectionLost(Throwable t) {
        System.out.println("Connection lost!");
        // code to reconnect to the broker would go here if desired
        try {
        	System.out.println("RECONNECTING to the broker of the Apps");
			initMQTTclient();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public  void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
    	
		//sec.setData(token).setTimestamp(Long.toString(System.currentTimeMillis()));
		//blindPos.setSecurityData(sec).setTimestamp(Long.toString(System.currentTimeMillis()));
    	
    	//we should send it to Smool Server
		String value_blind=new String(mqttMessage.getPayload());        
		String timestamp = Long.toString(System.currentTimeMillis());
			
		String message_payload = new String(mqttMessage.getPayload());
		Message msg_with_timestamp = new Message();
		msg_with_timestamp.setBody(message_payload);
		msg_with_timestamp.setTimestamp(timestamp);

		System.out.println("-------------------------------------------------");
		System.out.println("Received actuator message from "+ s +" (APPs): " + message_payload);
		System.out.println("-------------------------------------------------");
		
		try{
			switch(s){
				case "enact/actuators/neato/botvacD3/command":
					msg_with_timestamp_botvac.setBody(message_payload);
					msg_with_timestamp_botvac.setTimestamp(timestamp);
					new Thread(() -> botvacD3_command_ca.run(msg_with_timestamp_botvac)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_command_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg_with_timestamp_botvac, null);
					break;
				case "enact/actuators/microphone/record":
					msg_with_timestamp_mic.setBody(message_payload);
					msg_with_timestamp_mic.setTimestamp(timestamp);
					new Thread(() -> microphone_record_ca.run(msg_with_timestamp_mic)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_record_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg_with_timestamp_mic, null);
					break;
				case "enact/sensors/microphone/replay":
					msg_with_timestamp_repl.setBody(message_payload);
					msg_with_timestamp_repl.setTimestamp(timestamp);
					new Thread(() -> microphone_replay_ca.run(msg_with_timestamp_repl)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_replay_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg_with_timestamp_repl, null);
					break;
				case "enact/actuators/cec/source":
					msg_with_timestamp_cec_source.setBody(message_payload);
					msg_with_timestamp_cec_source.setTimestamp(timestamp);
					new Thread(() -> actuators_cec_source_ca.run(msg_with_timestamp_cec_source)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_source_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg_with_timestamp_cec_source, null);
					break;
				case "enact/actuators/cec/power":
					msg_with_timestamp_cec_power.setBody(message_payload);
					msg_with_timestamp_cec_power.setTimestamp(timestamp);
					new Thread(() -> actuators_cec_power_ca.run(msg_with_timestamp_cec_power)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuators_cec_power_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg_with_timestamp_cec_power, null);
					break;
				case "enact/actuators/wlights/1/switch_dimmer":
					msg_with_timestamp_switch1.setBody(message_payload);
					msg_with_timestamp_switch1.setTimestamp(timestamp);
					new Thread(() -> switch_dimmer_1_ca.run(msg_with_timestamp_switch1)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_1, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg_with_timestamp_switch1, null);
					break;
				case "enact/actuators/wlights/2/switch_dimmer":
					msg_with_timestamp_switch2.setBody(message_payload);
					msg_with_timestamp_switch2.setTimestamp(timestamp);
					new Thread(() -> switch_dimmer_2_ca.run(msg_with_timestamp_switch2)).start();
					//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.switch_dimmer_id_2, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg_with_timestamp_switch2, null);
					break;
				default:
					System.out.println("Unknown topic");
			}

		}catch (Exception e){
			e.printStackTrace();
		}
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        //System.out.println("Pub complete" + new String(token.getMessage().getPayload()));

    }



}
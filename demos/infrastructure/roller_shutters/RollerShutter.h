#ifndef _ROLLERSHUTTERS_H_
#define _ROLLER_SHUTTERS_H_
#include "RollerShutter_sm.h"

#ifdef __cplusplus
extern "C" {
#endif

struct RollerShutter {
  struct RollerShutterContext _fsm;
};

void RollerShutter_Init(struct RollerShutter *rl);

#ifdef __cplusplus
}
#endif
#endif

#include "RollerShutterSelect.h"
#include "KeyMapper.h"

void RollerShutterSelect_Init(struct RollerShutterSelect *rl) {
  RollerShutterSelectContext_Init(&rl->_fsm, rl);
  //RollerShutterSelectContext_EnterStartState(rl);
}

void RollerShutterSelect_select1() {
  printf("Select 1\n");
  keyPress(KEY_1);
}

void RollerShutterSelect_select2() {
  printf("Select 2\n");
  keyPress(KEY_2);
}

void RollerShutterSelect_select3() {
  printf("Select 3\n");
  keyPress(KEY_3);
}

void RollerShutterSelect_selectAll() {
  printf("Select all\n");
  keyPress(KEY_6);
}

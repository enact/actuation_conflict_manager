#!/bin/bash

#echo "Change the FSM internal mapping name to allow multiple FSM in the same program"
#sed -i 's/MainMap/MainMap2/' RollerShutterSelect.sm

echo "Generate C code corresponding to RollerShutter FSM..."
java -jar Smc.jar -c RollerShutter.sm
echo "Generate C code corresponding to RollerShutterSlect FSM..."
java -jar Smc.jar -c RollerShutterSelect.sm

echo
echo "Change statemap.h inclusion to local one..."
sed -i 's/<statemap.h>/"statemap.h"/g' RollerShutter_sm.c
sed -i 's/<statemap.h>/"statemap.h"/g' RollerShutterSelect_sm.c

echo
echo "Start Arduino IDE to compile the project..."
sleep 1
#include "RollerShutter.h"
#include "KeyMapper.h"

void RollerShutter_Init(struct RollerShutter *rl) {
  RollerShutterContext_Init(&rl->_fsm, rl);
  //RollerShutterContext_EnterStartState(rl);
}

void RollerShutter_doUp() {
  printf("doUp\n");
  keyPress(KEY_Up);
}

void RollerShutter_doStop() {
  printf("doStop\n");
  keyPress(KEY_Stop);
}

void RollerShutter_doDown() {
  printf("doDown\n");
  keyPress(KEY_Down);
}

#include <Arduino.h>
#include "KeyMapper.h"

extern "C" void keyPress(short pin) {
  digitalWrite(pin, HIGH);
  delay(200);
  digitalWrite(pin, LOW);
  delay(200);
  printf("Activate pin: %d\n", pin);
}

# ACM ThingML

You can have a look to this demo with the following [video](https://www.youtube.com/watch?v=RXHkpAyxmt0).

## Hardware setup

 - [Arduino UNO](https://store.arduino.cc/arduino-uno-rev3)
 - [Grove Base Shield](https://www.gotronic.fr/art-module-grove-base-shield-103030000-19068.htm)
 - [Grove Buzzer](https://www.seeedstudio.com/Grove-Buzzer.html)
 - [Serial MP3 Player](https://andrologiciels.wordpress.com/arduino/son-et-arduino/mp3/catalex-mp3-serie/)
 - [Mini Speaker Modile](https://monkeyecho.com/daily-coupons/alfawise-m06-straight-plug-design-mobile-phone-mini-speaker/)
 - SD Card

Connect Buzzer to D2 (Arduino Pin 2) and Connect Serail MP3 Player to D4 (Arduino Pin 4 (TX) and 5 (RX)

## Compile ThingML code

 - Install MD_YX5300 library in you Arduino IDE
 - ``java -jar ThingML2CLI.jar -c arduino -s "alarm.thingml" -o "out"``
 - In the ``out`` directory you can find ``Alarm_merged.thingml`` and the ``Alarm/Alarm.ino``
 - Copy ``out/Alarm_merged.thingml`` in ``./alarm_merged.thinml`` (the genesis.model file use this file).

## Commands to send over the Arduino serial com
 - m: play audio
 - i: stop audio (interrupt)
 - b: start buzzer
 - s: stop buzzer

# ACM ThingML + Node-RED

## Deploy
Use the ``genesis.model`` file to deploy the full demo (ThingML code and Node-RED).

## Solve ACM conflict

- Create a first environment model corresponding to the Arduino audio device (including two audio sources), to manage the ThnigML conflict
- Use the ``__acm_rulestart.json`` file to configure the ACM rule strategy.
- Complete the first environment model to include the OpenBVE audio environment in the conflict, to identify and solve the conflict at the Node-RED level
- USe the ``AlarmConflictManagement.eca`` file to specify the behaviour of the management

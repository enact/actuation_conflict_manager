#!/bin/sh

SMARTSPACE="pi@192.168.0.27"
APPSERVER="pi@192.168.0.21"

echo "Removing following containers:"
ssh $APPSERVER 'docker rm -f UserComfortContainer' 2> /dev/null
#ssh $APPSERVER 'docker rm -f InfluxDBContainer' 2> /dev/null
ssh $SMARTSPACE 'docker rm -f service_smartspace' 2> /dev/null
#ssh $SMARTSPACE 'docker rm -f service_arduino' 2> /dev/null
echo "done."

echo "Removing Smool processes..."
ssh $APPSERVER 'pkill -f ENACTConsumer-1.0.0-jar-with-dependencies.jar'
ssh $SMARTSPACE 'pkill -f ENACTProducer-1.0.0-jar-with-dependencies.jar'
echo "done."
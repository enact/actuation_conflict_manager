#---------------
# Install system
#---------------

# Flash Raspbian with balenaEtcher
# Create "ssh" file in the boot partition
# Create "wpa_supplicant.conf" in the boot partition with the following text
##
country=fr
update_config=1
ctrl_interface=/var/run/wpa_supplicant

network={
scan_ssid=1
ssid="MiniTweedy"
psk="xpad#3000nce"
}
##

# Boot Raspberry Pi 3B+ with the created SD Card
# ssh pi@raspberrypi.local

#---------------
# Sytem configure
#---------------
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install i2c-tools git
sudo raspi-config
# configure timezone

#---------------
# Docker
#---------------

# Install current docker programs to get dependencies
curl -fsSL https://get.docker.com | sh

# Docker specific version install to avoid issue/bug on lastest release (18.09.0)
sudo apt-get install docker-ce=18.06.2~ce~3-0~raspbian

sudo usermod -aG docker pi

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo sh -c 'echo "[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock
" > /etc/systemd/system/docker.service.d/startup_options.conf'

sudo sh -c 'echo "nameserver 8.8.8.8" > /etc/resolv.conf'

sudo systemctl daemon-reload
sudo systemctl enable docker.service

#---------------
# MQTT Broker
#---------------
sudo apt-get install mosquitto
sudo systemctl enable docker.service
